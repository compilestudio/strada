var gulp = require('gulp'),
    less = require('gulp-less'),
    watch = require('gulp-watch'),
    notify  = require('gulp-notify'),
    minifyCss = require('gulp-minify-css'),
    sourcemaps = require('gulp-sourcemaps'),
    browserSync = require('browser-sync').create(),
    plumber = require('gulp-plumber'),
    changed    = require('gulp-changed'),
    imagemin   = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    concat = require('gulp-concat'),
    stripDebug = require('gulp-strip-debug'),
    uglify = require('gulp-uglify'),
    autoprefixer = require('gulp-autoprefixer'),
    chmod = require('gulp-chmod'),
    sass = require('gulp-sass');

var onError = function(err) {
    console.log(err);
}

gulp.task('default', ['sass','less','browser-sync']);
//gulp.task('default', ['less']);

gulp.task('sass', function () {
  return gulp.src('./_css/front/sass/**/*.scss')
    .pipe(watch('./_css/front/sass/**/*.scss'))
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(minifyCss())
        .pipe(gulp.dest('./_css/front/'))
        .pipe(notify({ message: 'Scripts task complete' }));
});
 

gulp.task('less', function () {
    watch('./_css/front/*.less', function () {
        gulp.src('./_css/front/*.less')
            .pipe(watch('./_css/front/.less'))
            .pipe(less())
            .pipe(autoprefixer({
                browsers: ['last 2 versions'],
                cascade: false
            }))
            .pipe(minifyCss())
            .pipe(gulp.dest('./_css/front/'))
            .pipe(notify({ message: 'Scripts task complete' }));
    });
});

gulp.task('browser-sync', function() {
    browserSync.init({
        proxy: "localhost/strada"
    });
    gulp.watch("./application/**/*.*").on('change', browserSync.reload);
    gulp.watch("./_css/**/*.css").on('change', browserSync.reload);
    gulp.watch("./_js/**/*.*").on('change', browserSync.reload);
});

gulp.task('images', function() {
    var imgSrc = './temp_images/*.*',
        imgDst = './_img/';

    return gulp.src(imgSrc)
        .pipe(plumber({
            errorHandler: onError
        }))
        .pipe(changed(imgDst))
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: true}],
            use: [pngquant({quality: '50-80', speed: 4})]
        }))
        .pipe(chmod(755))
        .pipe(gulp.dest(imgDst))
        .pipe(notify({ message: 'Images task complete' }));
});

gulp.task('scripts', function() {
    return gulp.src('./_js/src/*.js')
        .pipe(plumber({
            errorHandler: onError
        }))
        .pipe(concat({ path: 'app.min.js', stat: { mode: 0666 }}))
        .pipe(stripDebug())
        .pipe(uglify())
        .pipe(gulp.dest('./_js/'))
        .pipe(notify({ message: 'Scripts task complete' }));
});
