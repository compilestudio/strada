<h1 class="title">
	<?=$parent;?> | <?=$title;?>
</h1>
<div class="left">
	<?php if(!empty($info['img1'])):?>
		<img src="<?=base_url();?>uploads/<?=$info['img1'];?>" alt="Philosophy" class="img-text">
	<?php endif;?>
	<?php if(!empty($info['img2'])):?>
		<img src="<?=base_url();?>uploads/<?=$info['img2'];?>" alt="Philosophy" class="img-text">
	<?php endif;?>
</div>
<div class="right">
	<?php if(!empty($info['pull'])):?>
		<div class="pull">
			<?=$info['pull'];?>
		</div>
	<?php endif;?>
	<div class="full-main">
		<?=$info['main'];?>
	</div>
</div>

<script type="text/javascript">
	$('.full-main').readmore({
		speed: 75,
		lessLink: '<a class="read" href="#">Read less</a>',
		moreLink: '<a class="read" href="#">Read more</a>'
	});
</script>