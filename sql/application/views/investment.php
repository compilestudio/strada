<h1 class="title">
	<?=$parent;?> | <?=$title;?>
</h1>
<div class="left">
	<?php if(!empty($info['img1'])):?>
		<img src="<?=base_url();?>uploads/<?=$info['img1'];?>" alt="Philosophy">
	<?php endif;?>
	<?php if(!empty($info['img2'])):?>
		<img src="<?=base_url();?>uploads/<?=$info['img2'];?>" alt="Philosophy">
	<?php endif;?>
</div>
<div class="right">
	<?php if(!empty($info['pull'])):?>
		<div class="pull">
			<?=$info['pull'];?>
		</div>
	<?php endif;?>
	<div class="full">
		<?=$info['main'];?>
	</div>
</div>