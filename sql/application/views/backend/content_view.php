<?php
function cmp_ql($a, $b){
	if(empty($a->_order))
		return 1;
	if (intval($a->_order) == intval($b->_order)) {
		return 0;
	}
	if(empty($b->_order))
		return -1;
	return (intval($a->_order) < intval($b->_order)) ? -1 : 1;
}
$a_="";
$b_="";
?>
<script language="javascript">
//The variable to store which section it's selected
var section = 0;
//Guardamos el selector del caption que se dio click
var caption_clicked = null;
$(document).ready(function(){

	$('#coords').parent().hide();

	<?php if($content_info->pending_approval==1 && $this->session->userdata('user_type')!=4):?>
		$('.right_column').block({message:  '<?=$content_locked;?>'});
	<?php endif;?>
	//Vemos el diff de los campos
	$('.diff').on('click',function(){
		$.colorbox({href:"<?=site_url('backend/content/view_diff');?>",data:{'section_id':$(this).attr("data-section_id"),'content_id':$(this).attr("data-content_id"),'row_id':$('.multiple',$(this).parent().parent()).val()}});
	});
	//Aqui ponemos el evento del botón de rechazar
	$('#reject_').on('click',function(){
		if(confirm('Are you sure that you want to reject this change?')){
			if($('#reject_reason').val()==''){
				alert("You have to add a comment about the rejection!");
			}else{
				$.post('<?=site_url("backend/content/reject_changes");?>',{id:<?=$content_id;?>,reason:$('#reject_reason').val()},function(){
					location.href='<?=base_url("backend/content/".$content_id);?>';
				});
			}
		}
	});
	//Aqui vemos el colapse y el expand segun en que esten
	if($('.active').parent().parent().hasClass("nav_top")){
		$('ul',$('.active').parent()).show();
	}else{
		$('.active').parent().parent().show();
	}
/******************Mostrar y ocultar secciones*********************************/
	//Aqui Mostramos los campos de la sección al darle edit
	$('.edit').on('click',function(){
		$('.section_body',$(this).parent().parent()).slideDown(function(){
			//Actualizamos el tamaño de la rallada de la izquierda
			if($('.right_column').height()>550)
				$('.left_column').height($('.right_column').height());
			});
		$(this).hide();
		$('.close_',$(this).parent()).fadeIn();
	});
	//Aqui ocultamos la sección al darle click en close
	$('.close_').on('click',function(){
		$('.section_body',$(this).parent().parent()).slideUp(function(){
			if($('.right_column').height()<550)
				$('.left_column').height(550);
			else
				$('.left_column').height($('.right_column').height());
		});
		$(this).hide();
		$('.edit',$(this).parent()).fadeIn();
	});
	//Armamos el uploadify de todas las imagenes
	$('.files').each(function(){
		var parent = $(this).parent().parent().parent();
		$(this).uploadify({
			'formData'      : {'content_id' : <?=$content_id;?>,'section_id':$(this).attr("data-sectionId"),'user_type':<?=$this->session->userdata('user_type');?>},
			'buttonImage' : '<?=base_url();?>_img/backend/select_files.png',
			'width'    : 97,
			'height'   : 30,
			'swf'      : '<?=base_url();?>swf/uploadify.swf',
			'uploader' : '<?=site_url("backend/content/upload_file");?>' ,
			'onUploadSuccess' : function(file, data, response) {
				//console.log(data);
				var resp = data.split(",");
				$('.picture_container ul',$(parent)).append("<li><img src='<?=base_url();?>uploads/"+resp[0]+"'><?php if($this->session->userdata('user_type')==3):?><img src='<?=base_url();?>_img/backend/draft.png' class='deleted_draft'><?php endif;?><div class='caption' data-id='"+resp[1]+"'><?=$placeholder;?></div><a href='javascript:void(0);' class='delete' data-id='"+resp[1]+"'><img src='<?=base_url();?>_img/backend/close.png'></a><a href='javascript:void(0);' class='drag' data-id='"+resp[1]+"'><img src='<?=base_url();?>_img/backend/drag.png'></a><div class='formatted_caption hide'></div></li>");
				//Evento de delete image
				image_events();
			}	
		});
	});
	//Armamos el uploadify de todas las imagenes del field
	$('.inner_files').each(function(){
		var parent = $(this).parent().parent().parent().parent().parent();
		//console.log($('.multiple',parent));
		$(this).uploadify({
			'formData'      : {'content_id' : <?=$content_id;?>,'section_id':$(this).attr("data-section_id"),'field_id':$(this).attr("data-field_id"),'user_type':<?=$this->session->userdata('user_type');?>,row_id:$('.multiple',parent).val()},
			'buttonImage' : '<?=base_url();?>_img/backend/select_files.png',
			'width'    : 97,
			'height'   : 30,
			'swf'      : '<?=base_url();?>swf/uploadify.swf',
			'uploader' : '<?=site_url("backend/content/upload_file_field");?>' ,
			'onUploadSuccess' : function(file, data, response) {
				var resp = data.split(",");
				//Actualizamos el padre de el archivo, es el hack porque el padre no lo toma bien en el formData
				$.post('<?=site_url("backend/content/update_file_father");?>',{id:resp[1],father:$('.multiple',parent).val()},function(){
					
				});
				//console.log(resp[0].split('.').pop());
				//console.log($('.files'+resp[2]+' ul'));
				$('.files'+resp[2]+' ul').append("<li><img src='<?=base_url();?>uploads/"+resp[0]+"'><?php if($this->session->userdata('user_type')==3):?><img src='<?=base_url();?>_img/backend/draft.png' class='deleted_draft'><?php endif;?><div class='caption' data-id='"+resp[1]+"'><?=$placeholder;?></div><a href='javascript:void(0);' class='delete' data-id='"+resp[1]+"'><img src='<?=base_url();?>_img/backend/close.png'></a><a href='javascript:void(0);' class='drag' data-id='"+resp[1]+"'><img src='<?=base_url();?>_img/backend/drag.png'></a><div class='formatted_caption hide'></div></li>");
				//Evento de delete image
				image_events_fields();
			}	
		});
	});
	//Evento de delete image
	image_events();
	//El botón para guardar los captions
	save_caption();
	//Agregamos el evento a los botones para guardar la seccion
	save_section();
	//Activamos el boton de guardar el contenido principal
	save_content();
	//Activamos el evento para borrar la sección
	delete_section();
	//Activamos los eventos para los date pickers
	date_events();
	
	//Cargamos los que tengan campos
	<?php if($content_sections) 
		foreach($content_sections as $sec):
			if($sec->multiple!=1):
	?>
			get_fields_values(<?=$content_id;?>,<?=$sec->section_id;?>,<?=$sec->row;?>);
	<?php 	endif; 
		endforeach;
	?>
		
	//Cuando cambien un sleect múltiple
	$('.multiple').on('change',function(){
		get_fields_values(<?=$content_id;?>,$(this).attr("data-sec-id"),$(this).val());
	});
	
	//Funcionalidad de expand y collapse all
	$('.collapse_').on('click',function(){
		$('.section_body').slideUp(function(){
			if($('.right_column').height()>550)
				$('.left_column').height($('.right_column').height());
			else
				$('.left_column').height(550);
		});
		$('.edit').show();
		$('.close_').hide();
	});
	$('.expand').on('click',function(){
		$('.section_body').slideDown(function(){
			if($('.right_column').height()>550)
				$('.left_column').height($('.right_column').height());
			else
				$('.left_column').height(550);
		});
		$('.edit').hide();
		$('.close_').show();
	});
	//POnemos la imagen en el lighbox
	$('.pop_img').on('click',function(){
		$('#big_image_').attr("src",'<?=base_url();?>uploads/'+$('.file_',$(this).parent().parent()).val());
	});
	//Autocomplete para las secciones
	var projects = [
	      {
		  value: "-1",
		  label: "Create New"
	      }
	      <?php if($content_sections) 
		foreach($content_sections as $sec):
			if($sec->multiple_info)
				foreach($sec->multiple_info as $info):?>
				,{
					value: "<?=$info->value;?>",
				  	label: "<?=htmlspecialchars($info->text);?>"
			      }
			<?php endforeach;
		endforeach;?>
    	];
    	<?php if($content_sections) 
		foreach($content_sections as $sec):
			if($sec->autocomplete==1):
	?>
		$("#multiple_text_<?=$sec->section_id;?>").autocomplete({
			source: function( request, response ) {
				$.ajax({
					url: "<?=site_url("backend/content/get_rows_json");?>",
					dataType: "jsonp",
					data: {
						maxRows: 6,
						query: request.term,
						content_id: <?=$content_id;?>,
						section_id: <?=$sec->section_id;?>
		          		},
		          		success: function( data ) {
		            		response($.map( data, function( item ) {
							return {
								label: item.label,
								value: item.value
							}
						}));
					}
				});
		      },
		      minLength: 0,
		      //source: projects,
		      focus: function( event, ui ) {
				$(this).val( ui.item.label );
	        		return false;
	      	},
	      	select: function( event, ui ) {
				$(this).val( ui.item.label );
	        		$("#multiple_<?=$sec->section_id;?>").val( ui.item.value );
	        		get_fields_values(<?=$content_id;?>,$(this).attr("data-sec-id"),$("#multiple_<?=$sec->section_id;?>").val());
	 			return false;
	 		}
		})
    	<?php endif;
		endforeach;
    	?>
});
//Cargamos los eventos de las imágenes
function image_events(){
	//Aquí va el delete de la imágen
	$('.picture_container .delete').on('click',function(){
		if(confirm('<?=$confirm_img_delete;?>')){
			var obj = $(this);
			$.post('<?=site_url("backend/content/delete_image");?>',{id:$(this).attr('data-id')},function(){
				$(obj).parent().slideUp();
			});
		}
	});
	//Aquí va el delete de la imágen
	$('.picture_container .restore').on('click',function(){
		if(confirm('<?=$confirm_img_restore;?>')){
			var obj = $(this);
			$.post('<?=site_url("backend/content/restore_image");?>',{id:$(this).attr('data-id')},function(){
				//$(obj).parent().slideUp();
				location.reload();
			});
		}
	});
	//Ponemos el click al caption
	$('.picture_container .caption').on('click',function(){
		$('#image_text').html($('.formatted_caption',$(this).parent()).html().trim());
		$('#image_modal').modal();
		caption_clicked = $(this);
	});
	
	//Ponemos el click en la foto como tal
	$('.picture_container li img.main_image').on('click',function(){
		$('#image_big').modal();
		$('#big_image').attr("src",$(this).attr("src"));
		$('.big_caption').html($('.formatted_caption',$(this).parent()).html());
	});
	//Armamos el uploadify para los campos pequeños
	$('.picture_container ul').each(function(){
		var obj = $(this);
		$(this).sortable({
			placeholder: "ui-state-highlight",
			handle: ".drag",
			stop: function( event, ui ) {
				//console.log($(ui.item).parent().sortable( "serialize" ));
				$.post('<?=site_url("backend/content/update_image_order/".$content_id);?>/'+$(obj).attr("data-sec_id"),$(ui.item).parent().sortable( "serialize" ),function(data){
					
				});
			}
		});
		$(this).disableSelection();
	});
	//vemos el delete del image
	$('.remove').on('click',function(){
		if(confirm('Do you want to delete this file?')){
			$('input:text',$(this).parent()).val('');
		}
	});
	//vemos el delete de la fecha
	$('.remove_date').on('click',function(){
		if(confirm('Do you want to remove this date?')){
			$('input:text',$(this).parent()).val('');
		}
	});
}
//Cargamos los eventos de las imágenes
function image_events_fields(){
	//Aquí va el delete de la imágen
	$('.section_file .delete').on('click',function(){
		//if(confirm('<?=$confirm_img_delete;?>')){
			var obj = $(this);
			$.post('<?=site_url("backend/content/delete_image_field");?>',{id:$(this).attr('data-id')},function(){
				$(obj).parent().slideUp();
			});
		//}
	});
	//Aquí va el delete de la imágen
	$('.section_file .restore').on('click',function(){
		if(confirm('<?=$confirm_img_restore;?>')){
			var obj = $(this);
			$.post('<?=site_url("backend/content/restore_image_field");?>',{id:$(this).attr('data-id')},function(){
				//$(obj).parent().slideUp();
				location.reload();
			});
		}
	});
	//Ponemos el click al caption
	$('.section_file .caption').on('click',function(){
		$('#image_text_field').html($('.formatted_caption',$(this).parent()).html().trim());
		$('#image_modal_field').modal();
		caption_clicked = $(this);
	});
	
	//Ponemos el click en la foto como tal
	$('.section_file li img.main_image').on('click',function(){
		$('#image_big').modal();
		$('#big_image').attr("src",$(this).attr("src"));
		$('.big_caption').html($('.formatted_caption',$(this).parent()).html());
	});
	//Armamos el uploadify para los campos pequeños
	$('.section_file ul').each(function(){
		var obj = $(this);
		$(this).sortable({
			placeholder: "ui-state-highlight",
			handle: ".drag",
			stop: function( event, ui ) {
				//console.log($(ui.item).parent().sortable( "serialize" ));
				$.post('<?=site_url("backend/content/update_image_order_field/".$content_id);?>/'+$(obj).attr("data-sec_id"),$(ui.item).parent().sortable( "serialize" ),function(data){
					
				});
			}
		});
		$(this).disableSelection();
	});
}
//Agregamos el click al boton para guardar el caption
function save_caption(){
	$('#save_image_text').on('click',function(){
		$.post('<?=site_url("backend/content/update_image_caption");?>',{id:$(caption_clicked).attr('data-id'),caption:$('#image_text').html()},function(){
			//Guardamos el HTML
			$('.formatted_caption',$(caption_clicked).parent()).html($('#image_text').html());
			//Desplegamos el text
			$(caption_clicked).html($('#image_text').text());
			$('#image_text').html('');
			$('#image_modal').modal('hide')
		});
	});
	$('#save_image_text_field').on('click',function(){
		$.post('<?=site_url("backend/content/update_image_caption_field");?>',{id:$(caption_clicked).attr('data-id'),caption:$('#image_text_field').html()},function(){
			//Guardamos el HTML
			$('.formatted_caption',$(caption_clicked).parent()).html($('#image_text_field').html());
			//Desplegamos el text
			$(caption_clicked).html($('#image_text_field').text());
			$('#image_text_field').html('');
			$('#image_modal_field').modal('hide')
		});
	});
}
//Agregamos la funcionalidad para guardar la seccion
function save_section(){
	$('.save').on('click',function(){
		var id = $(this).attr("data-section");
		$.post('<?=site_url("backend/content/save_section/".$content_id);?>/'+id+'/'+$('#multiple_'+id).val(),$(this).parent().parent().serializeArray(),function(data){
			$.pnotify({
				text: "<?=$success_notification;?>",
				delay: <?=$success_notification_delay;?>,
				type: 'success'
			});
			//We update the html for the select option
			if($('#multiple_'+id).prop("type")!=="hidden") {
				$('#multiple_' + id).html(data);
				$('#multiple_' + id).change();
				//We show the delte button
				$('#delete_' + id).fadeIn();
			}else{
				$('#multiple_' + id).val(1);
				$('#multiple_' + id).change();
			}
		});
	});
	
	//Agregamos la funcionalidad para guardar la seccion del meta
	$('.save_meta').on('click',function(){
		$.post('<?=site_url("backend/content/save_meta/".$content_id);?>',$('#meta_row').serializeArray(),function(data){
			$.pnotify({
				text: "<?=$success_notification;?>",
				delay: <?=$success_notification_delay;?>,
				type: 'success'
			});
		});
	});
}
//Eliminamos la seccion y su contenido
function delete_section(){
	$('.delete_section').on('click',function(){
		var id = $(this).attr("data-section");
		if(confirm('Are you sure that you want to delete this section?')){
			$.post('<?=site_url("backend/content/delete_section/".$content_id);?>/'+id+'/'+$('#multiple_'+id).val(),function(data){
				clear_form(id);
				$('#multiple_'+id).html(data);
				//We hide the delete button
				$('#delete_'+id).fadeOut();
				$.pnotify({
					text: "<?=$delete_notification;?>",
					delay: <?=$delete_notification_delay;?>,
					type: 'success'
				});
			});
		}
	});
}
//Obtenemos los campos de la seccion
function get_fields_values(content_id,section_id,row){
	clear_form(section_id);
	if(row!=-1){
		var container = $('#multiple_'+section_id).parent().parent();
		$(container).block({message:  '<?=$loading_content_notification;?>'});
		$.post('<?=site_url('backend/content/get_fields_json/');?>/'+content_id+'/'+section_id+'/'+row,function(data){
			for (var key in data) {		
				//Si es select
				if(data[key]['type_id']==15 || data[key]['type_id']==16 || data[key]['type_id']==17){
					$('#'+data[key]['name']+" option:selected").removeAttr("selected");
					$('#'+data[key]['name']+' option[value="'+data[key]['content_draft']+'"]').attr("selected","selected");
				}else if(data[key]['type_id']==22 || data[key]['type_id']==23){
					$('#'+data[key]['name']+" option:selected").removeAttr("selected");
					
					var keys = data[key]['content_draft'].split(",");
					$.each(keys,function(index,value){
						$('#'+data[key]['name']+' option[value="'+value+'"]').attr("selected","selected");
					});
				//We set the switches	
				}else if(data[key]['type_id']==13 || data[key]['type_id']==14){
					if(data[key]['content_draft']=='on'){
						$('#'+data[key]['name']).bootstrapSwitch('setState', true);
					}else{
						$('#'+data[key]['name']).bootstrapSwitch('setState', false);
					}
				//Si los datos son archivos
				}else if(data[key]['type_id']==28){
					var obj = data[key]['id'];
					$.post('<?=site_url("backend/content/get_files");?>',{content_id:content_id,section_id:section_id,row:row,field_id:data[key]['id']},function(data){
						data = data.split("##@@##");
						//console.log(data);
						$('.files'+data[0]).html(data[1]);
						image_events_fields();
					});
				}else{
					//We set the other fields
					$('#'+data[key]['name']).val(data[key]['content_draft']);
					//$('#'+data[key]['name']).html(data[key]['content_draft']);
				}
				

				//Agregamos la cantidad de caracteres
				if(data[key]['type_id']==4 || data[key]['type_id']==8 || data[key]['type_id']==12){
					//console.log($(data[key]['content_draft']).text().length);
					//console.log($('.char_count .chars_number',$('#'+data[key]['name']).parent()));
					var html = data[key]['content_draft'];
					var div = document.createElement("div");
					div.innerHTML = html;
					var text = div.textContent || div.innerText || "";
					$('.char_count .chars_number',$('#'+data[key]['name']).parent()).html(text.length);
				}
			}
			$(container).unblock();
		});
		$('#delete_'+section_id).fadeIn();
	}else{
		$('#delete_'+section_id).fadeOut();
	}
}
//Función para limpiar el form
function clear_form(section_id){
	$(':input','#section_'+section_id)
		.not(':button, :submit, :reset, :hidden')
		.val('')
		.removeAttr('checked')
		.removeAttr('selected');
	$('textarea','#section_'+section_id).html('');
	$('.switch').not('.main_switch').bootstrapSwitch('setState', false);
	$('.section_file',('#section_'+section_id)).html('');
}
//Guardamos los datos principales del contenido
function save_content(){
	$('.save_content').on('click',function(){
		front_display = $('#page_status').is(':checked');
		front_display = front_display ? 1 : 0;
		$.post('<?=site_url("backend/content/save_title");?>',{title:$('#page_header').val(),order:$('#page_order').val(),front_display:front_display,id:<?=$content_id;?>},function(){
			$.pnotify({
				text: "<?=$success_notification;?>",
				delay: <?=$success_notification_delay;?>,
				type: 'success'
			});
		});
	});
}
//Cargamos los eventos de los campos dates
function date_events(){
	$('.date').datepicker({autoclose:true});
}
</script>
<div class="row_">
	<div class="left_column">
		<?=$menu;?>
	</div>
	<div class="right_column">
<!--Titulo del contenido-->
		<div class="headline">
			<?=$content_info->title;?>
			<a href="javascript:void(0);" class="collapse_">
				<?=$btn_collapse;?>
			</a>
			<a href="javascript:void(0);" class="expand">
				<?=$btn_expand;?>
			</a>
		</div>
	<!------------------Preview option-->
		<?php if($user_type==3 || $user_type==4):?>
			<?php if($content_info->rejected==1):?>
				<div class="section">
					<div class="alert alert-danger">
						<strong>
							Rejected reason:
						</strong> 
						<?=$reason;?>
					</div>
				</div>
			<?php endif;?>
			<div class="section" style="padding-bottom:15px;">
				<a href="<?=site_url("backend/content/preview/".$content_id);?>" class="btn btn-success pull-right" target="_blank">
					Preview Page
				</a>
				<?php if($user_type==3):?>
					<a href="<?=site_url("backend/content/submit_changes/".$content_id);?>" class="submit_changes btn btn-warning pull-left">
						<?=$btn_submit_changes?>
					</a>
				<?php endif;?>
				<?php if($user_type==4 && $content_info->pending_approval==1):?>
					<a href="<?=site_url("backend/content/approve_changes/".$content_id);?>" class="submit_changes btn btn-warning pull-left">
						<?=$btn_approve_changes;?>
					</a>
					<a href="#reject_modal" role="button" data-toggle="modal" class="submit_changes btn btn-danger pull-left" style="margin-left:10px;">
						<?=$btn_reject_changes;?>
					</a>
				<?php endif;?>
			</div>
		<?php endif;?>
<!------------------Primera sección, donde va el titulo, orden y status-->
		<div class="section">
			<div class="section_header">
				<div class="information">
					<div class="image">
						<img src="<?=base_url();?>_img/backend/sections/header.png">
					</div>
					<div class="name">
						<?=$header_title;?>
					</div>
					<div class="description">
						<?=$header_description;?>
					</div>
				</div>
				<a href="javascript:void(0);" class="edit">
					<?=$section_edit;?>
				</a>
				<a href="javascript:void(0);" class="close_">
					<?=$section_close;?>
				</a>
			</div>
			<div class="section_body">
				<div class="small_divider">&nbsp;</div>
				<label for="page_header" class="page_header">
					<?=$page_header_title;?>
				</label>
				<label for="page_order" class="page_order">
					<?=$page_order_title;?>
				</label>
				<?php if($content_id!=1):?>
				<label for="page_status" class="page_status">
					<?=$page_status_title;?>
				</label>
				<?php endif;?>
				<div class="inner_row">
					<input type="text" id="page_header" name="page_header" value="<?=$content_info->title;?>" />
					<input type="text" id="page_order" name="page_order" value="<?=$content_info->content_order;?>" />
					<?php if($content_id!=1):?>
					<div class="switch switch-small main_switch" data-on-label="Live" data-off-label="Hidden" data-on="success" data-off="danger">
						<input type="checkbox" id="page_status" name="page_status" <?php if($content_info->front_display==1) echo 'checked="checked"';?>>
					</div>
					<?php endif;?>
					<?php 
					#Si es aprobador o publicador sin aprobacion
					if($user_type==1 || $user_type==4):?>
						<button type="button" class="submit save_content">
							<?=$btn_sumbit;?>
						</button>
					<?php endif;?>
				</div>
			</div>
		</div>
	<!------------------Recorremos las secciones aquí-->
		<?php if($content_sections) 
		foreach($content_sections as $sec):?>
			<div class="section">
				<!---------------Encabezado de las secciones------>
				<div class="section_header">
					<div class="information">
						<div class="image">
							<img src="<?=base_url();?>_img/backend/sections/<?=$sec->file;?>">
						</div>
						<div class="name">
							<?=$sec->name;?>
						</div>
						<div class="description">
							<?=$sec->display_description;?>
						</div>
					</div>
					<a href="javascript:void(0);" class="edit">
						<?=$section_edit;?>
					</a>
					<a href="javascript:void(0);" class="close_">
						<?=$section_close;?>
					</a>
					<?php if($content_info->pending_approval==1 && $this->session->userdata('user_type')==4):?>
						<button type="button" class="btn btn-small btn-info diff" data-section_id="<?=$sec->id;?>" data-content_id="<?=$content_id;?>">
							<i class="icon icon-random icon-white"></i>
						</button>
					<?php endif;?>
				</div>
				<!---------Cuerpo de las secciones aqui van los campos--->
				<div class="section_body">
					<div class="small_divider">&nbsp;</div>
					<?php if($sec->section_id==5 && $content_id==4):?>
						<div class="span9">
							<button type="button" class="save_" data-section="<?=$sec->section_id;?>" onclick="$('#section_5 .save').click();">
								Save
							</button>
						</div>
					<?php endif;?>
<!--------------------------------------------------------------Si la sección tendra multiples entradas---------------------------------------------->
					<?php if($sec->multiple==1):?>
						<div class="inner_row">
							<label for="multiple">
								<?php
									if($content_id==9 && $sec->section_id==24):
										echo "Select an Event";
									else:
										echo $multiple_label." an Entry";
									endif;
								?>
							</label>
							<?php if($sec->autocomplete==0):?>
								<select id="multiple_<?=$sec->section_id;?>" name="multiple_<?=$sec->section_id;?>" class="span4 multiple" data-sec-id="<?=$sec->section_id;?>">
									<?php if(($sec->section_id==7 && $content_id==4)):?>
										<option value="0">
											Select a type
										</option> 
									<?php elseif($sec->section_id==21 && $content_id==15):?>
 											<!--<option>-->
									<?php else:?>
										<option value="-1">
											<?=$multiple_new;?>
										</option>
									<?php endif;?>
									<?php if($sec->multiple_info):
											if($content_id==3 && ($sec->section_id==3)):
												$a_ = "p_order";
												$b_="p_name";
												//usort($sec->multiple_info,'cmp_');
											endif;
											//projects
											if($content_id==4 && ($sec->section_id==5)):
												$separator = false;
													foreach($sec->multiple_info as $info):
														if($info->text == "SEPARATOR HERE"):?>
															<optgroup label='ARCHIVED PROJECTS'>
														<?php else:?>
														<option value="<?=$info->value;?>" class="op<?=$info->status;?>">
															<?=$info->text;?>
														</option>

														<?php endif;													
												 endforeach;
												 if($separator):?></optgroup><?php endif;
											else:
												foreach($sec->multiple_info as $info):?>
													<option value="<?=$info->value;?>" class="op<?=$info->status;?>">
														<?=$info->text;?>
													</option>
											<?php endforeach;
										endif;
									endif;?>
								</select>
							<?php else:?>
								<input type="hidden" class="span4 multiple" id="multiple_<?=$sec->section_id;?>" name="multiple_<?=$sec->section_id;?>" data-sec-id="<?=$sec->section_id;?>" autocomplete="off">
								<input type="text" class="span4" id="multiple_text_<?=$sec->section_id;?>" name="multiple_text_<?=$sec->section_id;?>" data-sec-id="<?=$sec->section_id;?>" autocomplete="off">
							<?php endif;?>
							<?php if(!($sec->section_id==7 && $content_id==4)):?>
								<button type="button" id="delete_<?=$sec->section_id;?>" class="btn btn-danger pull-right hide delete_section" data-section="<?=$sec->section_id;?>">
									<?=$btn_delete;?>
								</button>
							<?php endif;?>
						</div>
					<?php else: ?>
						<!--Agregamos el row al que corresponde-->
						<input type="hidden" id="multiple_<?=$sec->section_id;?>" name="multiple_<?=$sec->section_id;?>" value="<?=$sec->row;?>" data-sec-id="<?=$sec->section_id;?>" class="multiple">
					<?php endif;?>
<!-------------------------------------------------------------------------Si la entrada es de imágenes---------------------------------------------->
					<?php if($sec->image==1):?>
						<div class="inner_row">
							<label>
								<?=$upload_label;?> <span class="small">(<?=$sec->width;?>w x <?=$sec->height;?>t pixels)</span>
							</label>
							<div>
								<input type="file" class="files" id="files<?=$sec->section_id;?>" name="files<?=$sec->id;?>" data-sectionId="<?=$sec->section_id;?>">
							</div>
						</div>
						<div class="inner_row picture_container">
							<ul data-sec_id="<?=$sec->section_id;?>">
								<!-- Desplegamos las imágenes si hubieran-->
								<?php if($sec->images)
								foreach($sec->images as $images):?>
									<li id="image_<?=$images->id;?>">
										<img src='<?=base_url();?>uploads/<?=$images->img;?>' class="main_image">
										<?php if($images->status==1):?>
											<img src='<?=base_url();?>_img/backend/deleted_draft.png' class="deleted_draft">
										<?php endif;?>
										<?php if($images->status==2):?>
											<img src='<?=base_url();?>_img/backend/draft.png' class="draft">
										<?php endif;?>
										<div class='caption' data-id='<?=$images->id;?>'>
											<?php if($images->status==1):
												echo "Pending delete approval"; 
											elseif($images->caption !=''):
												echo strip_tags($images->caption);
											else:
												echo $placeholder; 
											endif;?>
										</div>
										<div class="formatted_caption hide">
											<?php if($images->caption !=''):
												echo $images->caption;
											else:?>
												<?=$placeholder;?>
											<?php endif;?>
										</div>
										<?php 
										#Si no estamos pendientes de eliminación
										if($images->status!=1):?>
											<a href='javascript:void(0);' class='delete' data-id='<?=$images->id;?>'>
												<img src='<?=base_url();?>_img/backend/close.png'>
											</a>
											<a href='javascript:void(0);' class='drag' data-id='<?=$images->id;?>'>
												<img src='<?=base_url();?>_img/backend/drag.png'>
											</a>
										<?php else:?>
											<a href='javascript:void(0);' class='restore' data-id='<?=$images->id;?>'>
												<img src='<?=base_url();?>_img/backend/restore.png'>
											</a>
										<?php endif;?>
									</li>
								<?php endforeach;?>
							</ul>
						</div>
					<?php endif;?>
<!-------------------------------------------------------------------------Desplegamos los campos---------------------------------------------->
					<div class="inner_row fields_row">
						<form id="section_<?=$sec->section_id;?>" action="" method="post">
							<?php if($sec->image!=1 && $user_type!=4):?>
								<div class="span9">
									<button type="button" class="save" data-section="<?=$sec->section_id;?>">
										<?php
										switch($user_type):
											case 1:
												echo $btn_sumbit;
												break;
											case 3:
												echo $btn_save_draft;
												break;
										endswitch;
										?>
									</button>
								</div>
							<?php endif;?>
							<?php $i =0; 
							if($sec->fields)
							foreach($sec->fields as $field):?>
								<?php
								if($field['html']=="separator"):
									$i=-1;
									echo "<div class='separator'>&nbsp;</div>";
								else:?>
									<?=$field['html'];?>
									<?php $array_ = array(14,16,17,21,23,25,28);
									#Vemos si son big no agrega el campo
									if((($field['type']>=5 && $field['type']<=12) || $field['type']==26 || in_array($field['type'],$array_)))
										$i=-1; 
									#Si no son big agrega el separador enmedio de los campos
									if($i%2==0) 
										echo "<div class='span1'></div>";
									?>
								<?php endif;?>
							<?php $i++; 
							endforeach;?>
							<?php if($sec->image!=1 && $user_type!=4):?>
								<div class="span9">
									<button type="button" class="save" data-section="<?=$sec->section_id;?>">
										<?php
											switch($user_type):
												case 1:
													echo $btn_sumbit;
													break;
												case 3:
													echo $btn_save_draft;
													break;
											endswitch;
										?>
									</button>
								</div>
							<?php endif;?>
						</form>
					</div>
				</div>
			</div>	
		<?php endforeach;?>	
<!-------------------------------------------------------------------------Desplegamos el Meta data---------------------------------------------->
		<?php
		#Si tienen permisos para estos
		if($user_type!=3):?>
			<div class="section">
				<!---------------Encabezado de las secciones------>
				<div class="section_header">
					<div class="information">
						<div class="image">
							<img src="<?=base_url();?>_img/backend/sections/meta_data.png">
						</div>
						<div class="name">
							<?=$meta_tags_title;?>
						</div>
						<div class="description">
							<?=$meta_tags_description;?>
						</div>
					</div>
					<a href="javascript:void(0);" class="edit">
						<?=$section_edit;?>
					</a>
					<a href="javascript:void(0);" class="close_">
						<?=$section_close;?>
					</a>
				</div>
				<!---------Cuerpo de las secciones aqui van los campos--->
				<div class="section_body">
					<div class="small_divider">&nbsp;</div>
					<div class="inner_row fields_row">
						<form id="meta_row" action="" method="post">
							<div class="span9">
								<label for="meta_page_title">
									<?=$meta_page_title;?>
								</label>
								<input type="text" class="span9" id="meta_page_title" name="meta_page_title" value="<?php if($meta_data) echo $meta_data->meta_title;?>">
							</div>
							<div class="span9">
								<label for="meta_keywords">
									<?=$meta_keywords_title;?>
								</label>
								<textarea class="span9" id="meta_keywords" name="meta_keywords"><?php if($meta_data) echo $meta_data->meta_keywords;?></textarea>
							</div>
							<div class="span9">
								<label for="meta_description">
									<?=$meta_description_title;?>
								</label>
								<textarea class="span9" id="meta_description" name="meta_description"><?php if($meta_data) echo $meta_data->meta_description;?></textarea>
							</div>
						</form>
					</div>
					<div class="inner_row fields_row">
						<div class="span9">
							<button type="button" class="save_meta" data-section="meta">
								<?=$meta_tags_save;?>
							</button>
						</div>
					</div>
				</div>
			</div>
		<?php endif;?>

	</div>
</div>

<!--------------------------------------Modals------------------------------>
<div class="modal hide fade" id="image_modal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<?=$edit_image_title;?>
  	</div>
  	<div class="modal-body">
		<textarea class="tinymce" id="image_text" name="image_text"></textarea>
	</div>
	<div class="modal-footer">
		<button class="btn btn-primary pull-right" type="button" id="save_image_text" name="save_image_text">
			<?=$update_image_text;?>
		</button>
	</div>
</div>
<!--------------------------------------Modals------------------------------>
<div class="modal hide fade" id="image_big">
  	<div class="modal-body">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<img src="#" id="big_image">
		<div class="big_caption"></div>
	</div>
</div>
<!--------------------------------------MOdal for Image------------------------------>
<div class="modal hide fade" id="image_modal_2">
  	<div class="modal-body">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<img src="" id="big_image_">
	</div>
</div>
<!--------------------------------------Modal para el caption de los fields------------------------------>
<div class="modal hide fade" id="image_modal_field">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<?=$edit_image_title;?>
  	</div>
  	<div class="modal-body">
		<textarea class="tinymce" id="image_text_field" name="image_text_field"></textarea>
	</div>
	<div class="modal-footer">
		<button class="btn btn-primary pull-right" type="button" id="save_image_text_field" name="save_image_text_field">
			<?=$update_image_text;?>
		</button>
	</div>
</div>
<!--------------------------------------Modal para el rechazo de los cambios------------------------------>
<div class="modal hide fade" id="reject_modal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<?=$reject_title;?>
  	</div>
  	<div class="modal-body">
		<textarea class="tinymce" id="reject_reason" name="reject_reason"></textarea>
	</div>
	<div class="modal-footer">
		<button class="btn btn-primary btn-danger pull-right" type="button" id="reject_" name="reject_">
			<?=$reject_btn;?>
		</button>
	</div>
</div>
<style>
#related_projects{
	height:200px;
}
</style>
<?php
function cmp_($a, $b){
	global $a_;
	global $b_;
	if ($a[$a_] == $b[$a_]) {
		if ($a[$b_] == $b[$b_]) {
			return 0;
		}
		return ($a[$b_] < $b[$b_]) ? -1 : 1;
	}
	return ($a[$a_] < $b[$a_]) ? -1 : 1;
}
?>