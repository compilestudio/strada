			<div class="row">
				&nbsp;
			</div>
		</div>
<div class="modal hide fade" id="metaModal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			&times;
		</button>
		<h3>Meta Data</h3>
	</div>
	<div class="modal-body">
		<p>
			Metadata is data (information) about data.<br><br>
			The <meta> tag provides metadata about the HTML document. Metadata will not be displayed on the page, but will be machine parsable.
			<br><br>
			Meta elements are typically used to specify page description, keywords, author of the document, last modified, and other metadata.
			<br><br>
			The metadata can be used by browsers (how to display content or reload page), search engines (keywords), or other web services.
		</p>
	</div>
</div>
</body>
</html>