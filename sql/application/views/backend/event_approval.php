<div class="container content_container">
	<?=$menu;?>
	<div class="span9">
		<?php if($this->uri->segment(3)==1):?>
			<div class="alert alert-success">
				<strong>Notice!</strong> The event has been approved and added to the event list
			</div>
		<?php endif;?>
		<?php if($this->uri->segment(3)==2):?>
			<div class="alert alert-error">
				<strong>Notice!</strong> The event has been rejected and deleted from the list
			</div>
		<?php endif;?>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Event Date</th>
					<th>Event Name</th>
					<th>Event Venue</th>
					<th>
						Options
					</th>
				</tr>
			</thead>
			<tbody>
				<?php if($rows) 
				foreach($rows as $row):?>
					<tr>
						<td>
							<?=$row->event_date;?>
						</td>
						<td>
							<?=$row->name;?>
						</td>
						<td>
							<?=$row->location;?>
						</td>
						<td>
							<a href="<?=site_url("backend/event_approval/detail/".$row->id);?>" class="btn btn-mini">
								<i class="icon icon-eye-open"></i>
							</a>
						</td>
					</tr>
				<?php endforeach;?>
			</tbody>
		</table>
	</div>
</div>