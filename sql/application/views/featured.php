<h1 class="title_project">
	<?php
	$count = 0;
	foreach ($categories as $category) { 
		$class = "";
		if($category['category_']==$cat)
			$class = "active";
	?>
		<span>
			<?php
				if($count == 0){
					echo "";
				}else{
					echo " | ";
				}
			?>
		</span>
		<span class="separated-name <?=$class;?>" data-filter="<?=strtolower($category['category_'])?>">
			<?=$category['category_'];?>
		</span>
	<?php
		$count++;
		}
	?>
</h1>
<div class="boxes">
	<?php
		// for($i=0;$i<2;$i++)
		foreach ($projects as $project) { ?>
			<div class="box project-style <?=$project['classes'];?>">
				<div class="contain">
					<a href="<?=site_url('project/detail/'.$project['row'])?>">
						<img src="<?=base_url().'uploads/'.$project['image']?>">
						<span class="person-name"><?=$project['name']?></span>
					</a>
				</div>
			</div>
	<?php
		}
	?>
</div>
<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
<script type="text/javascript">
	/*
	var box = $('.box'),
	    boxContainer = $('.boxes'),
	    section = $('.sort-section'),
	    containerHeight = $('.adjustable-height'),
	    boxClassFilter,
	    showThese;

	$('span.separated-name').on('click', function(){  
		//We add the active class to the filter
		$('span.separated-name').removeClass('active');
		$(this).addClass("active");

		//We get the filter
	    boxClassFilter = $(this).data('filter');
	    showThese = boxContainer.find('.box' + '.' + boxClassFilter);
	    var sectionHeight = section.height();
	    
	    var tl = new TimelineLite()
	    .to(box, 0.5, {scale:0, opacity:0, ease: Power3.easeOut})
	    .set(box, {display:'none'})
	    .set(showThese, {display:'block'})
	    .to(showThese, 0.8, {scale:1, opacity:1, ease: Power3.easeOut}, '+=0.1')
	    .fromTo(section, 1, {height:'sectionHeight', ease: Power3.easeOut},  {height:'initial', ease: Power3.easeIn}, '-=1');

	    if (boxClassFilter == 'all') {    
	        var allTL = new TimelineLite()
	            .set(section, {height:sectionHeight})
	            .to(box, 0.5, {scale:0, opacity:0, ease: Power3.easeOut})
	            .set(box, {display:'none'})
	            .set(box, {display:'block'})
	            .to(box, 0.8, {scale:1, opacity:1, ease: Power3.easeOut}, '+=0.1')
	           .fromTo(section,1 , {height:'sectionHeight', ease: Power3.easeOut},  {height:'initial', ease: Power3.easeIn}, '-=1');
	    }       
	});
	*/
	var $grid;
	$(window).on("load",function(){
		resize();
		$grid = $('.boxes').isotope({
		  // options
		  itemSelector: '.box',
		  layoutMode: 'fitRows'
		});

		$('.separated-name').on('click', function(){  
			$(".separated-name").removeClass("active");
			$(this).addClass("active");
			var filter = $(this).data('filter');
			$grid.isotope({ filter: '.'+filter });
		});
		$(".separated-name.active").click();
	});
	$(window).resize(function(){
		resize();
	});
	function resize(){
		var height = 0;
		$('.box .contain').removeAttr("style");
		$.each($('.box'), function( index, value ) {
		  var h = parseInt($(value).outerHeight());
		  if(h>height){
		  	height = h;
		  }
		});
		//$('.box .contain').outerHeight(height);
		setTimeout(function(){
			$('.boxes').removeAttr("style").css('marginRight',$('.box:first').css("marginLeft"));
			//$('.navbar-brand').removeAttr("style").css('marginLeft',parseInt($('.box:first').css("marginLeft"))-15);
			$('h1.title_project').removeAttr("style").css('paddingLeft',$('.box:first').css("marginLeft")).css('paddingRight',$('.box:first').css("marginLeft"));
		},100);
	}
</script>

<script src='http://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/TweenMax.min.js'></script>




