<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends MY_Controller {

#**************************************************************************************************
	public function index($cat="all"){
		$this->load->model("backend/content_model","content");

		// $data['cat'] = $cat;
		$data = null;
		$this->load_header_front($data);

		// $data['categories'] = $this->content->get_section_multiple(5,4);
		$data['info'] = $this->content->get_section_info(10,3);

		$this->load->view('contact',$data);
		$this->load_footer_front(null);
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
