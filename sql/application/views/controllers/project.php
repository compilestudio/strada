<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Project extends MY_Controller {

#**************************************************************************************************
	public function index($cat="Featured"){
		$this->load->model("backend/content_model","content");

		$data['cat'] = $cat;
		$this->load_header_front($data);

		$categories = $this->content->get_section_multiple(5,4);
		$data['categories'] = $categories;
		$projects = $this->content->get_section_multiple(5,5);
		foreach($projects as &$p):
			$class = "";
			$array = explode(',' , $p['category']);
			foreach($categories as $c):
				if(in_array($c['row'], $array)):
					$class .= strtolower($c['category_'])." ";
				endif;
			endforeach;
			$p['classes'] = $class;
		endforeach;
		$data['projects'] = $projects;

		$this->load->view('featured',$data);
		$this->load_footer_front(null);
	}
	public function detail($id){
		$this->load->model("backend/content_model","content");

		$this->load_header_front(null);
		$data = null;

		$data['project'] = $this->content->get_section_info(5,5, $id);
		$data['slides'] = $this->content->get_files_result(5,5,46,$id);

		$this->load->view('detail-project',$data);
		$this->load_footer_front(null);
	}


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
