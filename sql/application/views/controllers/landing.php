<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Landing extends MY_Controller {

#**************************************************************************************************
	#Display the landing page
	public function index($error=0){
		$this->load->model("backend/content_model","content");

		$this->load_header_front(null);
		$data = null;

		$images = $this->content->get_section_multiple(1,1);
		if(!empty($images))
			usort($images,array($this,'sortImages'));
		$data['images'] = $images;
		
		$caption = $this->content->get_section_info(1,2);
		$data['caption'] = $this->content->get_section_info(1,2);

		$this->load->view('landing',$data);
		$this->load_footer_front(null);
	}
	private function sortImages($a,$b){
		if ($a['order'] == $b['order']) {
			return 0;
		}
		return ($a['order'] < $b['order']) ? -1 : 1;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
