<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load_header_super(":::Super admin:::");
		$this->load->model("super/Home_model","home");
		$data = $this->home->home_info();
		$this->load->view('super/home_view',$data);
		$this->load_footer_super();
	}
#Cambiamos la información del usuario
	public function update_user(){
		$this->load->library('encrypt');
		$user_name = $this->input->post("user_name");
		$password = $this->encrypt->encode($this->input->post("password"));
		$id = $this->session->userdata('super_user_id');
		#Cargamos el modelo
		$this->load->model('super/Home_model','home');
		$this->home->update_user($user_name,$password,$id);
		redirect('super/home');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */