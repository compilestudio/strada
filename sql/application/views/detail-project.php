<?php if(!empty($slides)):?>
	<div style="position: relative;">
		<div class="title-project-slider"><?=$project['name']?></div>
		<div id="maximage">
			<? foreach($slides as $s):?>
				<img src="<?=base_url().'uploads/'.$s->img;?>" alt="">
			<?php endforeach;?>
		</div>
		<?php if(!empty($project['slider_description'])):?>
			<div class="caption-project">
				<?=$project['slider_description'];?>
			</div>
		<?php endif;?>
		<div class="circles"></div>
	</div>
<?php endif;?>

<div class="details-project">
	<div class="description_left">
		<p class="title-description color-text"><?=$project['fact1']?></p>
		<h4 class="info-details color-text"><?=$project['neighborhood']?></h4>
		<p class="title-description color-text"><?=$project['fact2']?></p>
		<h4 class="info-details color-text"><?=$project['type']?></h4>
		<p class="title-description color-text"><?=$project['fact3']?></p>
		<h4 class="info-details color-text"><?=$project['size']?></h4>
	</div>
	<div class="description_right">
		<h3 class="name"><?=$project['name']?></h3>
		<p><?=$project['description']?></p>

		<?php if(!empty($project['phrase'])):?>
			<div class="phrase">
				<img class="comillas" src="<?=base_url().'img/comillas.jpg'?>" alt="">
				<div class="quote">
					<?=$project['phrase']?>
				</div>
				<div class="quoter">
					<?=$project['quoter']?>
				</div>
			</div>
		<?php endif;?>
	</div>
</div>

<script src="<?=base_url();?>_js/jquery.maximage.js"></script>
<script src="<?=base_url();?>_js/jquery.cycle.all.js"></script>
<?php
function addhttp($url) {
	if(!empty($url)):
	    if (!preg_match("~^(?:f|ht)tps?://~i", $url)):
	        $url = "http://" . $url;
	    endif;
	else:
		$url = "javascript:void(0);";
	endif;
    return $url;
}
?>
<script type="text/javascript" charset="utf-8">
	$(function(){
		// Trigger maximage
		jQuery('#maximage').maximage({
			cycleOptions: {
				pager: $('.circles')
			}
		});
	});
</script>