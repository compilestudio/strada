<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Fecha Creación: 21 de noviembre del 2011
 * Autor: Pedro Izaguirre
 * Fecha última modificación: 21 de noviembre del 2011
 * Último en modificarla: Pedro Izaguirre
 * 
 * Descripción: Son las funciones que tendrán todos los controladores
 * */


class MY_Controller extends CI_Controller {

	protected $idioma;
	protected $shown;

	public function __construct(){
		parent::__construct();
		
		#Redirect on backend
		if ($this->uri->segment(1)== "backend" && $this->session->userdata('user_id') === FALSE && ($this->uri->segment(2)!= "landing" && $this->uri->segment(2)!= "session" && $this->uri->segment(3)!= "upload_file" && $this->uri->segment(3)!= "upload_file_field" && $this->uri->segment(3)!= "upload_file_fields"))
			redirect("backend/landing");
		
		#Redirect on super admin
		if ($this->uri->segment(1)== "super" && $this->session->userdata('super_user_id') === FALSE && ($this->router->class=="home"))
			redirect("super/landing");
		
		//Establecemos el idioma espanol como el basico o el que se haya seleccionado
		$this->language = "english";
		
		if ($this->session->userdata('language') !== FALSE)
			$this->idioma = $this->session->userdata('language');

		$this->shown =0;
			
	}
#****************************************************************************************************************
	#Funcion para cargar el header
	protected function load_header($title){
		
		$this->lang->load('backend', $this->language);
		
		$user_id = $this->session->userdata('user_id');
		$dataHeader["page_title"]=$title;
		
		$dataHeader["main_title"]=$this->lang->line('main_title');
		if($user_id):
			#Obtenemos el texto del menu
			$dataHeader["home_menu"]=$this->lang->line('home_menu');
			$dataHeader["projects_menu"]=$this->lang->line('projects_menu');
			$dataHeader["users_menu"]=$this->lang->line('users_menu');
			$dataHeader["trabajando_en"]=$this->lang->line('trabajando_en');
			$dataHeader["dejar_de_trabajar"]=$this->lang->line('dejar_de_trabajar');
			$dataHeader["salir_leyenda"]=$this->lang->line('salir_leyenda');
		endif;
		$this->load->view('backend/_includes/header', $dataHeader);
	}
#****************************************************************************************************************
	#Funcion para cargar el footer
	protected function load_footer(){
		$this->load->view('backend/_includes/footer');
	}
	
#**********************FRONT*******************************************************
#****************************************************************************************************************
	#Funcion para cargar el header del front
	protected function load_header_front($data){
		
		$this->load->model("backend/Content_model","content");
		
		$this->lang->load('front', $this->language);

		$dataHeader=$data;

		$dataHeader['class'] = $this->router->fetch_class();
		$dataHeader['method'] = $this->router->fetch_method();
		$categories = $this->content->get_section_multiple(5,4);
		if(!empty($categories))
			usort($categories,array($this,'sortCats'));
		$dataHeader['categories'] = $categories;

		$dataHeader['ap'] = $this->content->get(2);
		$dataHeader['phi'] = $this->content->get(3);
		$dataHeader['stra'] = $this->content->get(6);
		$dataHeader['team'] = $this->content->get(4);
		$dataHeader['proj'] = $this->content->get(5);
		$dataHeader['plat'] = $this->content->get(7);
		$dataHeader['inv'] = $this->content->get(8);
		$dataHeader['dev'] = $this->content->get(9);
		$dataHeader['cont'] = $this->content->get(10);
		



		$this->load->view('_includes/header', $dataHeader);
	}
	private function sortCats($a,$b){
		if ($a['order_'] == $b['order_']) {
			return 0;
		}
		return ($a['order_'] < $b['order_']) ? -1 : 1;
	}
#****************************************************************************************************************
	#Funcion para cargar el footer
	protected function load_footer_front($data){
		$this->load->model("Menu_model","menu");
		$data['menu'] = $this->menu->show_menu_footer();

		$this->load->model("backend/Content_model","content");
		
		$data['social_links'] = $this->content->get_section_multiple(3,3);
		$data['info'] = $this->content->get_section_info(3,4);
		$this->load->view('_includes/footer',$data);
	}
#****************************************************************************************************************
	#Funcion para descargas
	function download($file){
				
		$file = urldecode($file);
		$this->load->helper('download');
		$real = realpath(APPPATH);
		
	    $data = file_get_contents($real."/../uploads/".$file);
	    $name = $file;
	    force_download($name, $data);
	}
#**********************SUPER USER*******************************************************
#****************************************************************************************************************
	#Funcion para cargar el header
	protected function load_header_super($title){
		
		$this->lang->load('super', $this->language);
		
		$user_id = $this->session->userdata('user_id');
		$dataHeader["page_title"]=$title;
		$dataHeader["main_title"]=$this->lang->line('main_title');
		$this->load->view('super/_includes/header', $dataHeader);
	}
#****************************************************************************************************************
	#Funcion para cargar el footer
	protected function load_footer_super(){
		$this->load->view('super/_includes/footer');
	}
#****************************************************************************************************************
	#Function to obtain the page name
	public function get_iframe($path,$width,$height, $id_){
		$info = parse_url($path);
		$host = @$info['host'];
		$host_names = explode(".", $host);
		$host_name = $host_names[count($host_names)-2];
		$return = "";
		if($host_name=="youtube"):
			$id = $this->getYouTubeIdFromURL($path);
			$return = '<iframe id="video'.$id_.'" style="width:100%; height:480px" width="'.$width.'" height="'.$height.'" src="http://www.youtube.com/embed/'.$id.'?enablejsapi=1" frameborder="0" allowfullscreen></iframe>';
		elseif($host_name=="youtu"):
			$id = $this->getYouTu_beIdFromURL($path);
			$return = '<iframe id="video'.$id_.'"  style="width:100%; height:480px" width="'.$width.'" height="'.$height.'" src="http://www.youtube.com/embed/'.$id.'?enablejsapi=1" frameborder="0" allowfullscreen></iframe>';
		elseif($host_name=="vimeo"):
			$id = $this->getVimeoIdFromURL($path);
			$return = '<iframe id="video'.$id_.'" style="width:100%; height:480px" src="http://player.vimeo.com/video/'.$id.'?api=1&amp;player_id=video'.$id_.'" width="'.$width.'" height="'.$height.'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
		endif;
		
		return $return;
	}
#****************************************************************************************************************
	#Function to obtain the page name
	public function get_iframe_100($path,$width,$height, $id_){
		$info = parse_url($path);
		$host = @$info['host'];
		$host_names = explode(".", $host);
		$host_name = $host_names[count($host_names)-2];
		$return = "";
		if($host_name=="youtube"):
			$id = $this->getYouTubeIdFromURL($path);
			$return = '<iframe id="video'.$id_.'" style="width:100%; height:100%" width="'.$width.'" height="'.$height.'" src="http://www.youtube.com/embed/'.$id.'?enablejsapi=1" frameborder="0" allowfullscreen></iframe>';
		elseif($host_name=="youtu"):
			$id = $this->getYouTu_beIdFromURL($path);
			$return = '<iframe id="video'.$id_.'"  style="width:100%; height:100%" width="'.$width.'" height="'.$height.'" src="http://www.youtube.com/embed/'.$id.'?enablejsapi=1" frameborder="0" allowfullscreen></iframe>';
		elseif($host_name=="vimeo"):
			$id = $this->getVimeoIdFromURL($path);
			$return = '<iframe id="video'.$id_.'" style="width:100%; height:100%" src="http://player.vimeo.com/video/'.$id.'?api=1&amp;player_id=video'.$id_.'" width="'.$width.'" height="'.$height.'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
		endif;
		
		return $return;
	}	
	#****************************************************************************************************************
	#Function to obtain the video
	public function get_iframe_260($path,$width,$height, $id_){
		$info = parse_url($path);
		$host = @$info['host'];
		$host_names = explode(".", $host);
		$host_name = $host_names[count($host_names)-2];
		$return = "";
		if($host_name=="youtube"):
			$id = $this->getYouTubeIdFromURL($path);
			$return = '<iframe id="video'.$id_.'" style="width:100%; height:260px" width="'.$width.'" height="'.$height.'" src="http://www.youtube.com/embed/'.$id.'?enablejsapi=1" frameborder="0" allowfullscreen></iframe>';
		elseif($host_name=="youtu"):
			$id = $this->getYouTu_beIdFromURL($path);
			$return = '<iframe id="video'.$id_.'"  style="width:100%; height:260px" width="'.$width.'" height="'.$height.'" src="http://www.youtube.com/embed/'.$id.'?enablejsapi=1" frameborder="0" allowfullscreen></iframe>';
		elseif($host_name=="vimeo"):
			$id = $this->getVimeoIdFromURL($path);
			$return = '<iframe id="video'.$id_.'" style="width:100%; height:260px" src="http://player.vimeo.com/video/'.$id.'?api=1&amp;player_id=video'.$id_.'" width="'.$width.'" height="'.$height.'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
		endif;
		
		return $return;
	}	
#****************************************************************************************************************
	#Function to obtain the thumbnail
	public function get_video_thumbnail($path){
		$info = parse_url($path);
		$host = @$info['host'];
		$host_names = explode(".", $host);
		$host_name = $host_names[count($host_names)-2];
		$return = "";
		if($host_name=="youtube"):
			$id = $this->getYouTubeIdFromURL($path);
			$return = "http://img.youtube.com/vi/".$id."/hqdefault.jpg"; 
		elseif($host_name=="youtu"):
			$id = $this->getYouTu_beIdFromURL($path);
			$return = "http://img.youtube.com/vi/".$id."/hqdefault.jpg"; 
		elseif($host_name=="vimeo"):

			$id = $this->getVimeoIdFromURL($path);

			$hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$id.php"));

			$return = $hash[0]['thumbnail_medium'];  
		endif;
		
		return $return;
	}	


	
#****************************************************************************************************************	
	#Función para obtener el id de youtube
	public function getYouTubeIdFromURL($url)
	{
		$url_string = parse_url($url, PHP_URL_QUERY);
		parse_str($url_string, $args);
		return isset($args['v']) ? $args['v'] : false;
	}
#****************************************************************************************************************	
	#Función para obtener el id de youtu.be
	public function getYouTu_beIdFromURL($url)
	{
		$vars = split("/",$url);
		return isset($vars[3]) ? $vars[3] : false;
	}
#****************************************************************************************************************	
	#Función para obtener el id de youtube
	public function getVimeoIdFromURL($url)
	{
		if (0 === preg_match('/^http:\/\/(www\.)?vimeo\.com\/(clip\:)?(\d+).*$/', $url, $match))
			$error = false;
		else
			return $match[3];
	}
#**************************************************************************************************
	#Cortamos el html
	/**
	* Truncates text.
	*
	* Cuts a string to the length of $length and replaces the last characters
	* with the ending if the text is longer than length.
	*
	* @param string $text	String to truncate.
	* @param integer $length Length of returned string, including ellipsis.
	* @param string $ending Ending to be appended to the trimmed string.
	* @param boolean $exact If false, $text will not be cut mid-word
	* @param boolean $considerHtml If true, HTML tags would be handled correctly
	* @return string Trimmed string.
	*/
	function truncate($text, $length = 100, $ending = '...', $exact = true, $considerHtml = true) {
		if ($considerHtml) {
			// if the plain text is shorter than the maximum length, return the whole text
			if (strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
				return $text;
			}
			// splits all html-tags to scanable lines
			preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);

			$total_length = strlen($ending);
			$open_tags = array();
			$truncate = '';

			foreach ($lines as $line_matchings) {
				// if there is any html-tag in this line, handle it and add it (uncounted) to the output
				if (!empty($line_matchings[1])) {
					// if it's an “empty element'' with or without xhtml-conform closing slash (f.e.
					if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
						// do nothing
						// if tag is a closing tag (f.e. )
					} else if (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
						// delete tag from $open_tags list
						$pos = array_search($tag_matchings[1], $open_tags);
						if ($pos !== false) {
							unset($open_tags[$pos]);
						}
					// if tag is an opening tag (f.e. )
					} else if (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
						// add tag to the beginning of $open_tags list
						array_unshift($open_tags, strtolower($tag_matchings[1]));
					}
					// add html-tag to $truncate'd text
					$truncate .= $line_matchings[1];
				}

				// calculate the length of the plain text part of the line; handle entities as one character
				$content_length = strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));
				if ($total_length+$content_length > $length) {
					// the number of characters which are left
					$left = $length - $total_length;
					$entities_length = 0;
					// search for html entities
					if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
					// calculate the real length of all entities in the legal range
						foreach ($entities[0] as $entity) {
							if ($entity[1]+1-$entities_length <= $left) {
								$left--;
								$entities_length += strlen($entity[0]);
							} else {
								// no more characters left
								break;
							}
						}
					}
					$truncate .= substr($line_matchings[2], 0, $left+$entities_length);
					// maximum lenght is reached, so get off the loop
					break;
				} else {
					$truncate .= $line_matchings[2];
					$total_length += $content_length;
				}
				// if the maximum length is reached, get off the loop
				if($total_length >= $length) {
					break;
				}
			}
		} else {
			if (strlen($text) <= $length) {
				return $text;
			} else {
				$truncate = substr($text, 0, $length - strlen($ending));
			}
		}
		// if the words shouldn't be cut in the middle...
		if (!$exact) {
			// ...search the last occurance of a space...
			$spacepos = strrpos($truncate, ' ');
			if (isset($spacepos)) {
				// ...and cut the text in this position
				$truncate = substr($truncate, 0, $spacepos);
			}
		}

		// add the defined ending to the text
		$truncate .= $ending;

		if($considerHtml) {
			// close all unclosed html-tags
			foreach ($open_tags as $tag) {
				$truncate .= '';
			}
		}
		return $truncate;
	}
/************************************************************************/
	public function squares($entries,$type=0,$id=0,$themes){
		$i = 0;
		$path = realpath("uploads");
		$projects = array();
		$col = null;

		$p_small = array();
		$p_big = array();
		$p_large = array();
		foreach($entries as $p):
			$ty_ = 0;
			$types = explode(",",$p['type_']);
			if(!empty($types))
				$ty_ = in_array($id, $types);
			if(($type==0 && $id==0 && !empty($p['featured']) && $p['featured']=="on")
					#|| ($type==="type" && !empty($p['type_']) && $p['type_']==$id)
					|| ($type==="type" && !empty($p['type_']) && $ty_)
					|| ($type==="theme" && !empty($p['theme']) && $p['theme']==$id)
					|| ($type==="all")
			):
				$info = getimagesize($path."/".$p["thumbnail"]);
				if($info[0]<=570):
					if($info[1]<=372):
						#Si es un small
						#if($i==0):
							#Si no hay ninguno ingresado
							#$col[] = $p;
							#$i++;
						#else:
						#	$col[] = $p;
						#	$i=0;
							#Hacemos push a los projectos con el tipo 1 de small
						#	$projects[] = array('info' => $col,'type' => 1);
						#	$col = null;
						#endif;
						$p_small[] =$p;
					else:
						#Cuando solo viene 1 proyecto small
						#if($col != null):
						#	$projects[] = array('info' => $col,'type' => 1);
						#	$col = null;
						#endif;
						#$col[] = $p;
						#$i=0;
						#Hacemos push a los projectos con el tipo 2 de tall
						#$projects[] = array('info' => $col,'type' => 1);
						#$col = null;
						$p_large[] = $p;
					endif;
				elseif($info[0]>570):
					#Cuando solo viene 1 proyecto small
					#if($col != null):
					#	$projects[] = array('info' => $col,'type' => 1);
					#	$col = null;
					#endif;
					#$col[] = $p;
					#$i=0;
					#Hacemos push a los projectos con el tipo 3 de full
					#$projects[] = array('info' => $col,'type' => 2);
					#$col = null;
					$p_big[]=$p;
				endif;
			endif;
		endforeach;
		#Cuando solo viene 1 proyecto
		#if($col != null):
		#	$projects[] = array('info' => $col,'type' => 1);
		#endif;
		#echo "<pre>";
		#var_dump($projects);
		#echo "</pre>";

		#We shuffle the arrays
		if(!empty($p_small))
			shuffle($p_small);
		if(!empty($p_large))
			shuffle($p_large);
		if(!empty($p_big))
			shuffle($p_big);

		$theme = 0;
		if($type=="all"):
			$theme = 1;
		endif;
		while(!empty($p_small) || !empty($p_large) || !empty($p_big)):
			#Hacemos el random de los tipos de grid
			$grid = mt_rand(1,2);
			if($grid==1 && empty($p_large)):
				$grid=2;
			endif;
			if($grid==2 && empty($p_big)):
				$grid=1;
			endif;
			#echo $grid.">";
			if($grid==1):
				$t = mt_rand(1,3);
				#echo $t."<br>";
				#************************************************************************************
				if($t==1):
					$this->pop_($p_large,$projects,1,1);
					$this->pop_($p_small,$projects,1,2,$theme,1,$themes);
					$this->pop_($p_small,$projects,1,2,$theme,1,$themes);
				endif;
				#************************************************************************************
				#************************************************************************************
				if($t==2):
					$this->pop_($p_small,$projects,1,2,$theme,1,$themes);
					$this->pop_($p_large,$projects,1,1);
					$this->pop_($p_small,$projects,1,2,$theme,1,$themes);
				endif;
				#************************************************************************************
				if($t==3):
					$this->pop_($p_small,$projects,1,2,$theme,1,$themes);
					$this->pop_($p_small,$projects,1,2,$theme,1,$themes);
					$this->pop_($p_large,$projects,1,1);
				endif;
			#************************************************************************************
			else:
				$t = mt_rand(1,2);
				#echo $t."<br>";
				#************************************************************************************
				if($t==1):
					$this->pop_($p_small,$projects,1,2,$theme,1,$themes);
					$this->pop_($p_big,$projects,2,1);
				endif;
				#************************************************************************************
				if($t==2):
					$this->pop_($p_big,$projects,2,1);
					$this->pop_($p_small,$projects,1,2,$theme,1,$themes);
				endif;
			endif;
		endwhile;

		return $projects;
	}
	################################################################################################
	private function pop_(&$origin,&$dest,$type,$q=1,$theme=0,$small=0,&$themes=null){
		#metemos el small
		$themB = 0;
		$col = null;
		for($i=0;$i<$q;$i++):
			$t = mt_rand(1,2);
			if(!empty($themes) && $this->shown==0):
				if(!empty($themes)):
					$th = array_pop($themes);
					$col[] = array('theme_title'=>strtoupper($th));
					$this->shown = 1;
					$themB = 1;
				endif;
			else:
				$a = array_pop ($origin);
				if(!empty($a)):
					$a['small'] = $small;
					$col[] = $a;
				else:
					$a = array(
						'name' => '',
						'thumbnail' => 't_small.jpg',
						'location' => '',
						'slider' => 0,
						'type_' => -1,
						'theme' => -1,
						'description' => '',
						'stats' => '',
						'awards' => '',
						'featured' => 'on',
						'row' => 0
					);
					$a['small'] = $small;
					$col[] = $a;
				endif;
			endif;
		endfor;
		if(!empty($col)):
			$dest[] = array('info' => $col,'type' => $type);
		endif;

		if($this->shown==1 && $themB==0):
			$this->shown = 0;
		endif;
	}
/************************************************************************/
	public function squares_team($entries,$type=0){
		$i = 0;
		$path = realpath("uploads");
		$projects = array();
		$col = null;
		//shuffle($entries);
		foreach($entries as &$p):
			if($type=="all"
				|| (!empty($p['cat']) && $p['cat']==$type)
			):
				/*
				if($i==0):
					$t = mt_rand(1,2);
					if($t==2):
						$p['tall']='on';
					else:
						$p['tall']='0';
					endif;
				else:
					$p['tall']='0';
				endif;
				*/
				$p['tall'] = 0;
				if($p['tall']!=='on'):
				#if($t==1):
					#Si es un small
					if($i==0):
						#Si no hay ninguno ingresado
						$col[] = $p;
						$i++;
					else:
						$col[] = $p;
						$i=0;
						#Hacemos push a los projectos con el tipo 1 de small
						$projects[] = array('info' => $col,'type' => 1);
						$col = null;
					endif;
				else:
					#Cuando solo viene 1 proyecto small
					if($col != null):
						$projects[] = array('info' => $col,'type' => 1);
						$col = null;
					endif;
					$col[] = $p;
					$i=0;
					#Hacemos push a los projectos con el tipo 2 de tall
					$projects[] = array('info' => $col,'type' => 1);
					$col = null;
					$i=0;
				endif;
			endif;
		endforeach;
		#Cuando solo viene 1 proyecto
		if($col != null):
			$projects[] = array('info' => $col,'type' => 1);
		endif;
		#echo "<pre>";
		#var_dump($projects);
		#echo "</pre>";
		return $projects;
	}
	public function sortSocial($a,$b){
		if ($a['order'] == $b['order']) {
			return 0;
		}
		return ($a['order'] < $b['order']) ? -1 : 1;
	}
	public function addhttp($url) {
		if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
			$url = "http://" . $url;
		}
		return $url;
	}
}