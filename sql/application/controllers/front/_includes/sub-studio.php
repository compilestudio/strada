<?php
if(!empty($filter)):
	$two = $filter;
else:
	$two = $this->uri->segment(2);
endif;
?>
<div class="submenu">
	<ul class="nav nav-tabs">
		<li role="presentation" <?php if($two=='all') echo 'class="active"';?>>
			<a class="first" href="<?=site_url("studio/people");?>">
				ALL
			</a>
		</li>
		<?php if(!empty($categories))
		foreach($categories as $cat):?>
			<li role="presentation" <?php if($two==strtolower($cat['category'])) echo 'class="active"';?>>
				<a href="<?=site_url("studio/people/".strtolower($cat['category']));?>">
					<?=$cat['category'];?>
				</a>
			</li>
		<?php endforeach;?>
		<li role="presentation" <?php if($two=="life") echo 'class="active"';?>>
			<a href="<?=site_url("studio/life");?>">
				STUDIO LIFE
			</a>
		</li>
	</ul>
</div>
