<?php

	/*
	 * Fecha de Creación: 16-mar-2012
	 * Autor: Pedro Izaguirre
	 * Fecha Última Modificación: 
	 * Modificado por: 
	 * Descripción: Modelo con funciones para el Home
	 * 
	 */ 

class Content_model extends MY_Model{
	protected $draft;
	function __construct(){
		parent::__construct();
		$this->set_table("content");
		$this->draft = $this->session->userdata('draft');
	}
#********************************************************************************************************
	public function load_content_language(){
		$this->lang->load('backend', $this->idioma);
		
		$array_end = NULL;
		#Obtenemos los títulos para el content del language
		$array_end['header_title'] = $this->lang->line('header_title');
		$array_end['header_description'] = $this->lang->line('header_description');
		$array_end['section_edit'] = $this->lang->line('section_edit');
		$array_end['section_close'] = $this->lang->line('section_close');
		
		$array_end['page_header_title'] = $this->lang->line('page_header_title');
		$array_end['page_order_title'] = $this->lang->line('page_order_title');
		$array_end['page_status_title'] = $this->lang->line('page_status_title');
		
		#Obtenemos los labels del lang para las secciones
		$array_end['multiple_label'] = $this->lang->line('multiple_label');
		$array_end['multiple_new'] = $this->lang->line('multiple_new');
		
		
		#Obtenemos los labels lang de las fotos
		$array_end['upload_label'] = $this->lang->line('upload_label');
		$array_end['placeholder'] = $this->lang->line('placeholder');
		$array_end['confirm_img_delete'] = $this->lang->line('confirm_img_delete');
		$array_end['confirm_img_restore'] = $this->lang->line('confirm_img_restore');
		$array_end['edit_image_title'] = $this->lang->line('edit_image_title');
		$array_end['update_image_text'] = $this->lang->line('update_image_text');
		$array_end['reject_title'] = $this->lang->line('reject_title');
		$array_end['reject_btn'] = $this->lang->line('reject_btn');
		
		#button labels
		$array_end['btn_sumbit'] = $this->lang->line('btn_sumbit');
		$array_end['btn_save_draft'] = $this->lang->line('btn_save_draft');
		$array_end['btn_submit_changes'] = $this->lang->line('btn_submit_changes');
		$array_end['btn_approve_changes'] = $this->lang->line('btn_approve_changes');
		$array_end['btn_reject_changes'] = $this->lang->line('btn_reject_changes');
		$array_end['btn_approve'] = $this->lang->line('btn_approve');
		$array_end['btn_delete'] = $this->lang->line('btn_delete');
		$array_end['btn_expand'] = $this->lang->line('btn_expand');
		$array_end['btn_collapse'] = $this->lang->line('btn_collapse');
		
		#NOtifications
		$array_end['success_notification'] = $this->lang->line('success_notification');
		$array_end['success_notification_delay'] = $this->lang->line('success_notification_delay');
		$array_end['delete_notification'] = $this->lang->line('delete_notification');
		$array_end['delete_notification_delay'] = $this->lang->line('delete_notification_delay');
		$array_end['loading_content_notification'] = $this->lang->line('loading_content_notification');
		$array_end['content_locked'] = $this->lang->line('content_locked');
		
		#Meta Data
		$array_end['meta_tags_title'] = $this->lang->line('meta_tags_title');
		$array_end['meta_tags_description'] = $this->lang->line('meta_tags_description');
		$array_end['meta_tags_save'] = $this->lang->line('meta_tags_save');
		$array_end['meta_page_title'] = $this->lang->line('meta_page_title');
		$array_end['meta_keywords_title'] = $this->lang->line('meta_keywords_title');
		$array_end['meta_description_title'] = $this->lang->line('meta_description_title');
		
		return $array_end;
	}
#********************************************************************************************************
	public function content_info($id,$sub=0){
		
		#Cargamos las lineas del archivo de lenguaje
		$array_end = $this->load_content_language();
		
		#Mandamos el id del content
		$array_end['content_id']=$id;
		$array_end['user_type']=$this->session->userdata('user_type');

		#Aquí obtenemos el menú
		$this->load->model('backend/menu_model','menu');
		$array_end['menu']=$this->menu->show_menu();		
		
		#Obtenemos la información de las secciones
		$content_sections = $this->get_sections($id);
		if($content_sections)
		foreach($content_sections as &$cs):
			#Obtenemos las imágenes para las secciones que son imágenes
			if($cs->image==1):
				$cs->images = $this->get_images($id,$cs->section_id);
			endif;
			
			//Vemos si ya tiene rows
			$cs->row = $this->get_row($id,$cs->section_id);
			
			//Obtenemos el listado de los múltiples 
			$cs->multiple_info = $this->get_select_field_options($cs->section_id,$id);


			if($id==2 && $cs->section_id==8):
				foreach($cs->multiple_info as &$m):
					$campos = $this->content->get_section_info(2,8,$m->value);
					#var_dump($campos);
					$campos = $this->content->get_section_info(3,3,$campos['project']);
					$m->text = $campos['p_name'];
				endforeach;
			endif;

			#obtenemos los campos
			$cs->fields = $this->get_fields($id,$cs->section_id);

		endforeach;
		$array_end['content_sections'] = $content_sections;
		
		#Obtenemos el meta data
		$result = $this->db->get_where('meta_data', array('content_id' => $id));
		if ($result->num_rows() > 0):
			$result = $result->result();
			$array_end['meta_data']=$result[0];
		else:
			$array_end['meta_data'] =  NULL;
		endif;
		
		#Obtenemos la info del rechazo
		$this->set_table("log");
		$reason = $this->list_rows('reason','content_id='.$id,'date desc');
		if($reason)
			$array_end['reason'] = $reason[0]->reason;
		else
			$array_end['reason'] = NULL;
		
		#La información principal del contenido
		$this->set_table("content");
		$array_end['content_info']=$this->get($id);
			
		return $array_end;	
	}
#********************************************************************************************************
	#we get the list of sections for aval=0 content
	public function get_sections($content_id){
		$sql = 	"select	fs.*,cs.*,si.file,cs.description display_description
				 from 	content_section cs,field_section fs,section_icon si
				 where	fs.id=cs.section_id
				 and		si.id=fs.icon
				 and		cs.content_id=".$content_id."
				 and		cs.order>=0
				 order by cs.order asc";
		$query = $this->db->query($sql);
		
		if ($query->num_rows() > 0):
			return $query->result();
		else:
			return NULL;
		endif;
	}
#********************************************************************************************************
	#Obtenemos el número de sección
	public function get_row($content_id,$section_id){
		$this->set_table("data_row");
		$row = $this->list_rows('*',"content_id=".$content_id." and section_id=".$section_id);
		
		if($row):
			return 1;
		endif;

		return -1;
		
		$this->set_table("content");
	}

#********************************************************************************************************
	#Obtenemos los fields 
	public function content_fields($id,$sub=0){
		if($sub==0):
			$sql = "select 	f.id,f.name,ft.type,fcs.content content_small,fcl.content content_long,f.display_text,fs.name section_name,f.select_id,'options'
					from 	field_type ft,fields f
					left 	join field_content_small fcs on f.id=fcs.fields_id and f.content_id=fcs.content_id
					left 	join field_content_long fcl on f.id=fcl.fields_id and f.content_id=fcl.content_id
					left 	join field_section fs on f.field_section=fs.id
					where 	f.type_id=ft.id
					and 	f.content_id=".$id."
					order 	by fs.order asc, f.`order` asc";
		else:
			$sql = "select 	f.id,f.name,ft.type,fcs.content content_small,fcl.content content_long,f.display_text,fs.name section_name,f.select_id,'options'
					from 	field_type ft,fields f
					left 	join field_content_small fcs on f.id=fcs.fields_id and fcs.content_id=".$sub."
					left 	join field_content_long fcl on f.id=fcl.fields_id and fcl.content_id=".$sub."
					left 	join field_section fs on f.field_section=fs.id
					where 	f.type_id=ft.id
					and 	f.content_id=".$id."
					order 	by fs.order asc, f.`order` asc";
		endif;
		$query = $this->db->query($sql);
		
		if ($query->num_rows() > 0):
			#obtenemos la info
			$return = $query->result_array();
			#Recorremos para ver si hay algún select
			for($i=0;$i<count($return);$i++):
				if($return[$i]['select_id']!=0):
					$return[$i]['options'] = $this->list_rows('*',' father = '.$return[$i]['select_id']." and deleted=0");
				endif;
			endfor;
		else:
			$return = "";
		endif;
		return $return;	
	}

#********************************************************************************************************
	#we get the list of sections
	public function get_sections_($id,$val=0){
		if($val==0):
			$sql = "select	id,title
					from	content
					where	father=".$id."
					and		deleted=0
					order by content_order";
		else:
			$sql = "select	id,title
					from	content
					where	id=".$val."
					and		deleted=0";
		endif;
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0):
			#obtenemos la info
			$return = $query->result();
		else:
			$return = "";
		endif;
		return $return;
	}
#********************************************************************************************************
	#we get the information of the content with the value of a field
	public function get_info_on_field($val,$content_id){
		$sql = "select	fs.content_id id,f.select_id
				from	fields f,field_content_small fs 
				where	fs.fields_id=f.id
				and		fs.content='".$val."'
				and		f.content_id=".$content_id;
		$query = $this->db->query($sql);
		$category_name = $this->get_sections(0,$val);
		$return['category_name'] = $category_name[0]->title;
		if ($query->num_rows() > 0):
			foreach ($query->result() as $row):
				$content=$this->get($row->id);
				$data = NULL;
				$data['name'] = $content->title;
				$data['id'] = $content->id;
				$data['content']= $this->get_data($content->id);
				$return['content'][]=$data;
			endforeach;
		endif;
		return $return;
	}
#********************************************************************************************************
	#we get the id, name and content of the sections
	public function get_section_data($id){
		$sections = $this->get_sections($id);

		$data = "";
		if($sections!="")
			foreach($sections as $sec):
				$info = NULL;
				$info['id']=$sec->id;
				$info['name']=$sec->title;
				$parent_cat = $this->get($sec->id);
				$info['parent_category']=$parent_cat->parent_category;
				$info['order']=$parent_cat->content_order;
				$info['content']= $this->get_data($sec->id);
				$data[]=$info;
			endforeach;
		return $data;
	}
	
#********************************************************************************************************
	#obtenemos la infor del contenido
	public function get_data($id){
		$sql = "SELECT 	f.name,fs.content content_small,fl.content content_long,f.select_id
				FROM 	fields f
				LEFT 	JOIN field_content_long fl ON fl.fields_id = f.id AND 	fl.content_id =".$id."
				LEFT 	JOIN field_content_small fs ON fs.fields_id = f.id AND 	fs.content_id =".$id."
				WHERE 	fs.fields_id IS NOT NULL
				OR 		fl.fields_id IS NOT NULL";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0):
			$return = NULL;
			#obtenemos la info
			$result = $query->result();
			foreach($result as $res):
				$return[$res->name] = " ";
				if($res->select_id>0):
					$sql = "select	title
							from	content
							where	id=".$res->content_small;
					$query_select = $this->db->query($sql);
					if ($query_select->num_rows() > 0):
						$result_select = $query_select->result();
						$return[$res->name] = $result_select[0]->title;
					else:
						$return[$res->name] = "";
					endif;
				elseif($res->content_small!=NULL):
					$return[$res->name] = $res->content_small;
				elseif($res->content_long!=NULL):
					$return[$res->name] = $res->content_long;
				endif;
			endforeach;
		else:
			$return = "";
		endif;
		return $return;
	}
#********************************************************************************************************
	#Guardamos la información del contenido
	public function save_content($data,$id){
		if($id==-1):
			return $this->insert($data);
		else:
			$this->update($data, $id);
			return $id;
		endif;
	}
#********************************************************************************************************
	#Obtenemos los fields 
	public function calendar_dates($id,$limit=3,$month=0,$year=0,$category=0){
		if($month==0):
			$sql = "select 	fs.content_id,fs.content,mid( fs.content, 6,2) month,mid( fs.content, 1,4) year
					from 	content c,content c1,fields f,field_content_small fs
					where 	c1.father=c.id
					and 	c.id=f.content_id
					and 	c1.father=".$id."
					and 	f.name='event_date'
					and 	f.id=fs.fields_id
					and 	c1.id=fs.content_id
					and 	STR_TO_DATE( fs.content, '%Y-%m-%d' )>= CURDATE()
					and		c1.deleted=0
					group by fs.content_id
					order by fs.content asc,fs.content_id asc";
		else:
			$sql = "select 	fs.content_id,fs.content,mid( fs.content, 6,2) month,mid( fs.content, 1,4) year
					from 	content c,content c1,fields f,field_content_small fs
					where 	c1.father=c.id
					and 	c.id=f.content_id
					and 	c1.father=".$id."
					and 	f.name='event_date'
					and 	f.id=fs.fields_id
					and 	c1.id=fs.content_id
					and 	mid( fs.content, 6,2)= '".$month."'
					and 	mid( fs.content, 1,4)= '".$year."'
					and		c1.deleted=0
					group by fs.content_id
					order by fs.content asc,fs.content_id asc";
		endif;
		$dates_id = $this->db->query($sql);

		if ($dates_id->num_rows() > 0):
			$data = NULL;
			$i =0;
			foreach ($dates_id->result() as $row):
				$array = NULL;
				$array['title'] = $this->get($row->content_id);
				$array['content'] = $this->get_data($row->content_id);
				$array['month'] = date("F", mktime(0, 0, 0, $row->month, 10));
				$array['month_numeric'] = $row->month;
				$array['year'] = $row->year;
				
				$sql = "select	*
						from	category_content
						where	content_id=".$row->content_id."
						and		parent_category=".$category;
				$query = $this->db->query($sql);
				$valid = 0;
				if ($query->num_rows() > 0):
					$valid=1;
				endif;
				
				
				if($limit!=0 && $i>=$limit):
					break;
				else:
					$i++;
				endif;
				if($category==0 || ($category!=0 && $category==$array['title']->parent_category) || $valid==1)
					$data[] = $array;
			endforeach;
		else:
			return NULL;
		endif;
		return $data;
	}
/*****************************New functions*************************************/
#********************************************************************************************************
	#Insertamos o actualizamos una imágen
	#Si mandamos $id==-1 inserta si le mandamos el id actualiza esa imágen
	public function image_($id,$content_id,$section_id,$caption="",$file_name="",$user_type){
		#Si no trae nombre del archivo no lo actualiza
		if($file_name!=""):
			$data['img'] = $file_name;
		endif;
		#Si no trae caption no la actualiza
		if($caption!=""):
			$data['caption'] = $caption;
		endif;
		
		#cambiamos la tabla del modelo
		$this->set_table("image");
		
		#Si es insert
		if($id==-1):
			
			$array = array(
						'content_id' => $content_id,
						'section_id' => $section_id
					);			
			#Obtenemos cual es el siguiente orden
			$cant = $this->total_rows($array);
			$cant ++;
			
			$data['content_id'] = $content_id;
			$data['section_id'] = $section_id;
			$data['image_order'] = $cant;
			
			if($user_type==3):
				$data['status'] = 2;
			endif;
			
			
			$id = $this->insert($data);
		else:
			$this->update($data,$id);
		endif;
		
		return $id;
		
	}
#********************************************************************************************************
	#Insertamos o actualizamos una imágen de un campo de galeria
	#Si mandamos $id==-1 inserta si le mandamos el id actualiza esa imágen
	public function image_field($id,$content_id,$section_id,$caption="",$file_name="",$user_type,$field_id="",$row_id=""){
		#Si no trae nombre del archivo no lo actualiza
		if($file_name!=""):
			$data['img'] = $file_name;
		endif;
		#Si no trae caption no la actualiza
		if($caption!=""):
			if($user_type==3):
				$data['caption_d'] = $caption;
			else:
				$data['caption'] = $caption;
			endif;
		endif;
		
		#cambiamos la tabla del modelo
		$this->set_table("file");
		
		#Si es insert
		if($id==-1):
			
			$array = array(
						'content_id' => $content_id,
						'section_id' => $section_id,
						'field_id' => $field_id,
						'row_id' => $row_id
					);			
			#Obtenemos cual es el siguiente orden
			$cant = $this->total_rows($array);
			$cant ++;
			
			$data['content_id'] = $content_id;
			$data['section_id'] = $section_id;
			$data['field_id'] = $field_id;
			$data['row_id'] = $row_id;
			$data['image_order'] = $cant;
			
			if($user_type==3):
				$data['status'] = 2;
			endif;
			
			
			$id = $this->insert($data);
		else:
			if($user_type==3):
				$data['status'] = 2;
			endif;
			$this->update($data,$id);
		endif;
		
		return $id;
		
	}
#********************************************************************************************************
	#Obtenemos la información de las imágenes
	public function get_images($content_id,$section_id){
		$sql = 	"select	*
				 from		image
				 where	content_id=".$content_id."
				 and		section_id=".$section_id."
				 order by	image_order asc";
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0):
			return $query->result();
		else:
			return NULL;
		endif;
	}
#********************************************************************************************************
	#Eliminamos la imágen
	public function delete_image($id){
		#cambiamos la tabla del modelo
		$this->set_table("image");
		
		$user_type = $this->session->userdata('user_type');
		
		if($user_type==3):
			$array = array('status'=>1);
			$this->update($array,$id);
		else:
			$this->delete($id);
		endif;
	}
#********************************************************************************************************
	#Eliminamos la imágen del field
	public function delete_image_field($id){
		#cambiamos la tabla del modelo
		$this->set_table("file");
		
		$user_type = $this->session->userdata('user_type');
		
		if($user_type==3):
			$array = array('status'=>1);
			$this->update($array,$id);
		else:
			$this->delete($id);
		endif;
	}
#********************************************************************************************************
	#Restauramos la imágen
	public function restore_image($id){
		#cambiamos la tabla del modelo
		$this->set_table("image");
		$array = array('status'=>0);
		$this->update($array,$id);
	}
#********************************************************************************************************
	#Restauramos la imágen del field
	public function restore_image_field($id){
		#cambiamos la tabla del modelo
		$this->set_table("file");
		$array = array('status'=>0);
		$this->update($array,$id);
	}
#********************************************************************************************************
	#Obtenemos los campos
	public function get_field_list($content_id,$section_id){
		$sql = 	"SELECT 	*
				 FROM		fields f
				 where	f.content_id=".$content_id."
				 and		f.section_id=".$section_id."
				 order by 	f.order asc,f.id asc";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0):
			return $query->result();
		else:
			return NULL;
		endif;
	}
#********************************************************************************************************
	#Obtenemos los campos con su data y su html code
	public function get_fields($content_id,$section_id){
		$sql = 	"SELECT 	*
				 FROM		fields f
				 where	f.content_id=".$content_id."
				 and		f.section_id=".$section_id."
				 order by 	f.order asc,f.id asc";
		$query = $this->db->query($sql);
		
		#Creamos la variable para el arreglo a regresar
		$return = NULL;
		if ($query->num_rows() > 0):
			foreach($query->result() as $row):
				$data['type'] = $row->type_id;
				$data['html'] = $this->get_html_field($row);
				$return[] = $data;
			endforeach;
		endif;
		return $return;
	}
#********************************************************************************************************
	#Obtenemos los campos con su data
	public function get_fields_data($content_id,$section_id,$row_id=1){
		$user_type=$this->session->userdata('user_type');
		
		$field = "content_live";
		if($user_type==3):
			$field = "content_draft";
		endif;
		
		$sql = 	"SELECT 	f.*,fd.id data_id,fd.fields_id,fd.".$field." content_draft,fd.row_id,f.max_chars,f.type_id
				 FROM		fields f left join field_data fd
				 on 		f.id=fd.fields_id
				 where	f.content_id=".$content_id."
				 and		f.section_id=".$section_id."
				 and		fd.row_id=".$row_id."
				 order by 	f.order asc,f.id asc";
		#echo $sql;	 
		$query = $this->db->query($sql);
		
		$return = $query->result();

		return $return;
	}
#********************************************************************************************************
	#Obtenemos el html del campo
	public function get_html_field($row){
		$this->lang->load('backend', $this->idioma);
		#Variables de tamaño
		$span_small = "span4";
		$span_big = "span9";
		$span_file_small = "span3";
		$span_file_big = "span8";
		$value ='';
		
		#vemos si es select y obtenemos los campos
		if($row->select_id!=0):
			$values = explode(",",$row->select_id);
			$options = $this->get_select_field_options_($values[1],$values[0],$value,$this->lang->line('multiple_none'));
		endif;
		

		#variables del switch
		$on = $this->lang->line('switch_on');
		$off = $this->lang->line('switch_off');
		$date_format = $this->lang->line('date_format');
		
		
		switch($row->type_id):
			#Input small
			case 1:
					$field = "<div class='".$span_small."'>";
						$field .= "<label for='".$row->name."'>".$row->display_text."</label>";
						$field .= "<input type='text' id='".$row->name."' name='".$row->name."' value='".$value."' class='".$span_small."'>";
					break;
			#Textarea small
			case 2:
					$field = "<div class='".$span_small."'>";
						$field .= "<label for='".$row->name."'>".$row->display_text."</label>";
						$field .= "<textarea id='".$row->name."' name='".$row->name."' class='".$span_small."'>".$value."</textarea>";
					break;
			#File Small
			case 3:
					$field = "<div class='".$span_small."'>";
						$field .= "<label for='".$row->name."'><a href='#image_modal_2' data-toggle='modal' class='pop_img'><i class='icon-picture'></i></a>".$row->display_text."</label>";
						$field .= "<input type='text' id='".$row->name."' name='".$row->name."' value='".$value."' class='".$span_file_small." file_'>";
						$field .= "<input type='file' id='".$row->name."_file' value='".$value."' class='file_small'>";
						$field .= "<a href='javascript:void(0);' class='remove'><img src='".base_url()."_img/backend/cancel.png'></a>";
					break;
			#Texteditor Small
			case 4:
					$field = "<div class='".$span_small."'>";
						$max_chars = "";
						if($row->max_chars>0)
							$max_chars = " (".$row->max_chars." chars max)";
						$field .= "<label for='".$row->name."'>".$row->display_text."".$max_chars."</label>";
						$field .= "<textarea id='".$row->name."' name='".$row->name."' class='editor_small' max_chars='".$row->max_chars."'>".$value."</textarea>";
						$field .= "<div class='char_count ".$span_small."'><span class='chars_number'></span> characters</div>";
					break;
			#Switch Small
			case 13:
					$field = "<div class='".$span_small."'>";
						$field .= "<label for='".$row->name."'>".$row->display_text."</label>";
						$field .= "<div id='".$row->name."' class='switch' data-on-label='".$on."' data-off-label='".$off."' data-on='success' data-off='danger'>
									<input type='checkbox' id='".$row->name."' name='".$row->name."'>
								</div>";
					break;
			#Select Small
			case 15:
					$field = "<div class='".$span_small."'>";
						$field .= "<label for='".$row->name."'>".$row->display_text."</label>";
						$field .= "<select id='".$row->name."' name='".$row->name."'class='".$span_small."'>";
						$field .= $options;
						$field .= "</select>";
					break;
			#Date small
			case 20:
					$field = "<div class='".$span_small."'>";
						$field .= "<label for='".$row->name."'>".$row->display_text."</label>";
						$field .= "<div class='input-append date' data-date-format='".$date_format."'>
    									<input id='".$row->name."' name='".$row->name."' class='span3' size='16' type='text' value='".$value."' readonly>
    									<span class='add-on'>
    										<i class='icon-th'></i>
    									</span>
								</div>";
						$field .= "<a href='javascript:void(0);' class='remove_date'><img src='".base_url()."_img/backend/cancel.png'></a>";
					break;
					
			#Select Multiple Small
			case 22:
					$field = "<div class='".$span_small."'>";
						$field .= "<label for='".$row->name."'>".$row->display_text."</label>";
						$field .= "<select id='".$row->name."' name='".$row->name."[]' class='".$span_small." multiple_' multiple>";
							$field .= $options;
						$field .= "</select>";
					break;
			#Time small
			case 24:
					$field = "<div class='".$span_small."'>";
						$field .= "<label for='".$row->name."'>".$row->display_text."</label>";
						$field .= "<div class='input-append bootstrap-timepicker'>
									<input id='".$row->name."' name='".$row->name."' type='text' class='time'>
									<span class='add-on'><i class='icon-time''></i></span>
								</div>";
					break;
############################################################################################################################################################
			#Input Big
			case 5:
					$field = "<div class='".$span_big."'>";
						$field .= "<label for='".$row->name."'>".$row->display_text."</label>";
						$field .= "<input type='text' id='".$row->name."' name='".$row->name."' value='".$value."' class='".$span_big."'>";
					break;
			#Textarea Big
			case 6:
					$field = "<div class='".$span_big."'>";
						$field .= "<label for='".$row->name."'>".$row->display_text."</label>";
						$field .= "<textarea id='".$row->name."' name='".$row->name."' class='".$span_big."'>".$value."</textarea>";
					break;
			#File Big
			case 7:
					$field = "<div class='".$span_big."'>";
						$field .= "<label for='".$row->name."'><a href='#image_modal_2' data-toggle='modal' class='pop_img'><i class='icon-picture'></i></a>".$row->display_text."</label>";
						$field .= "<input type='text' id='".$row->name."' name='".$row->name."' value='".$value."' class='".$span_file_big."'>";
						$field .= "<input type='file' id='".$row->name."_file' class='file_big'>";
						$field .= "<a href='javascript:void(0);' class='remove'><img src='".base_url()."_img/backend/cancel.png'></a>";
					break;
			#Texteditor Big
			case 8:
					$field = "<div class='".$span_big."'>";
						$max_chars = "";
						if($row->max_chars>0)
							$max_chars = " (".$row->max_chars." chars max)";
						$field .= "<label for='".$row->name."'>".$row->display_text."".$max_chars."</label>";
						$field .= "<textarea id='".$row->name."' name='".$row->name."' class='editor_big' max_chars='".$row->max_chars."'>".$value."</textarea>";
						$field .= "<div class='char_count ".$span_big."'><span class='chars_number'></span> characters</div>";
					break;
			#Select Big
			case 16:
					$field = "<div class='".$span_big."'>";
						$field .= "<label for='".$row->name."'>".$row->display_text."</label>";
						$field .= "<select id='".$row->name."' name='".$row->name."'class='".$span_big."'>";
							$field .= $options;
						$field .= "</select>";
					break;
############################################################################################################################################################
			#Input Small Big
			case 9:
					$field = "<div class='".$span_big."'>";
						$field .= "<label for='".$row->name."'>".$row->display_text."</label>";
						$field .= "<input type='text' id='".$row->name."' name='".$row->name."' value='".$value."' class='".$span_small."'>";
					break;
			#Textarea Small Big
			case 10:
					$field = "<div class='".$span_big."'>";
						$field .= "<label for='".$row->name."'>".$row->display_text."</label>";
						$field .= "<textarea id='".$row->name."' name='".$row->name."' class='".$span_small."'>".$value."</textarea>";
					break;
			#File Small Big
			case 11:
					$field = "<div class='".$span_big."'>";
						$field .= "<div style='width:300px;'>
								<label for='".$row->name."'><a href='#image_modal_2' data-toggle='modal' class='pop_img'><i class='icon-picture'></i></a>".$row->display_text."</label>";
						$field .= "<input type='text' id='".$row->name."' name='".$row->name."' value='".$value."' class='".$span_file_small."'>";
						$field .= "<input type='file' id='".$row->name."_file' value='".$value."' class='file_small'>";
						$field .= "<a href='javascript:void(0);' class='remove'><img src='".base_url()."_img/backend/cancel.png'></a>
								</div>";
					break;
			#Texteditor Small Big
			case 12:
					$field = "<div class='".$span_big."'>";
						$max_chars = "";
						if($row->max_chars>0)
							$max_chars = " (".$row->max_chars." chars max)";
						$field .= "<label for='".$row->name."'> ".$row->display_text."".$max_chars."</label>";
						$field .= "<textarea id='".$row->name."' name='".$row->name."' class='editor_small' max_chars='".$row->max_chars."'>".$value."</textarea>";
						$field .= "<div class='char_count ".$span_small."'><span class='chars_number'></span> characters</div>";
					break;
			#Switch Small Big
			case 14:
					$field = "<div class='".$span_big."'>";
						$field .= "<label for='".$row->name."'>".$row->display_text."</label>";
						$field .= "<div id='".$row->name."' class='switch' data-on-label='".$on."' data-off-label='".$off."' data-on='success' data-off='danger'>
									<input type='checkbox' id='".$row->name."' name='".$row->name."'>
								</div>";
					break;
			#Select Small Big
			case 17:
					$field = "<div class='".$span_big."'>";
						$field .= "<label for='".$row->name."'>".$row->display_text."</label>";

						$field .= "<select id='".$row->name."' name='".$row->name."'class='".$span_small."'>";
							$field .= $options;
						$field .= "</select>";
					break;
			#Date small Big
			case 21:
					$field = "<div class='".$span_big."'>";
						$field .= "<label for='".$row->name."'>".$row->display_text."</label>";
						$field .= "<div class='input-append date' data-date-format='".$date_format."'>
    									<input id='".$row->name."' name='".$row->name."' class='span3' size='16' type='text' value='".$value."' readonly>
    									<span class='add-on'>
    										<i class='icon-th'></i>
    									</span>
								</div>";
						$field .= "<a href='javascript:void(0);' class='remove_date'><img src='".base_url()."_img/backend/cancel.png'></a>";
					break;
			#Select Multiple Small Big
			case 23:
					$field = "<div class='".$span_big."'>";
						$field .= "<label for='".$row->name."'>".$row->display_text."</label>";
						$field .= "<select id='".$row->name."' name='".$row->name."[]' class='".$span_small." multiple_' multiple>";
							$field .= $options;
						$field .= "</select>";
					break;
			#Time small big
			case 25:
					$field = "<div class='".$span_big."'>";
						$field .= "<label for='".$row->name."'>".$row->display_text."</label>";
						$field .= "<div class='input-append bootstrap-timepicker'>
									<input id='".$row->name."' name='".$row->name."' type='text' class='time'>
									<span class='add-on'><i class='icon-time''></i></span>
								</div>";
					break;
############################################################################################################################################################
			#Separator
			case 26:
					return "<div class='separator'>".$row->display_text."</div>";
					break;
			#Order
			case 27:
					$field = "<div class='".$span_small."'>";
						$field .= "<label for='".$row->name."'>".$row->display_text."</label>";
						$field .= "<input type='text' id='".$row->name."' name='".$row->name."' value='".$value."' class='span2'>";
					break;
############################################################################################################################################################
			#Files
			case 28:
					$field = "<div class='".$span_big."'>";
						$field .= "<div class='files_title'>".$row->display_text."</div>";
						$field .= "<div class='pull-left' style='margin:10px 0'>
									<input type='file' class='inner_files' id='files".$row->id."' name='files".$row->id."' data-section_id='".$row->section_id."' data-field_id='".$row->id."' data-content_id='".$row->content_id."'>
							     </div>";
						$field .= "<div class='section_file files".$row->id."'></div>";
						
						//$field .= "<input type='text' id='".$row->name."' name='".$row->name."' value='".$value."' class='span2'>";
					break;
		endswitch;
			#We add the Id of the field
			$field .= "<input type='hidden' id='".$row->name."_id' name='".$row->name."_id' value='".$row->id."'>";
		#We close the div
		$field .= "</div>";
		return $field;
	}
#********************************************************************************************************
	#Obtenemos los archivos del field
	function get_files($content_id,$section_id,$field_id,$row_id){
		$user_type = $this->session->userdata('user_type');
		
		$img_order = "image_order";
		if($user_type==3)
			$img_order = "image_order_d";
		
		$placeholder = $this->lang->line('placeholder');
		$sql = "select	*
			  from 	file
			  where	content_id=".$content_id."
			  and		section_id=".$section_id."
			  and		field_id=".$field_id."
			  and		row_id=".$row_id."
			  order by 	".$img_order." asc";
		$query = $this->db->query($sql);
		$return = "<ul data-sec_id='".$content_id."' data-content='".$content_id."' data-field_id='".$field_id."' data-row_id='".$row_id."'>";
		if ($query->num_rows() > 0):
			foreach ($query->result() as $images):
				$return .= "<li id='image_".$images->id."'>";
				$ext = pathinfo($images->img, PATHINFO_EXTENSION);
				$array_ = array('gif','jpg','png');
				if(in_array($ext,$array_)):
					$return .= "<img src='".base_url()."uploads/".$images->img."' class='main_image'>";
				else:
					$return .= "<img src='".base_url()."_img/backend/document.png' class='main_image'>";
				endif;
							if($images->status==1):
								$return .= "<img src='".base_url()."_img/backend/deleted_draft.png' class='deleted_draft'>";
							endif;
							if($images->status==2):
								$return .= "<img src='".base_url()."_img/backend/draft.png' class='draft'>";
							endif;
							$return .= "<div class='caption' data-id='".$images->id."'>";
							if($images->status==1):
								$return .=  "Pending delete approval"; 
							elseif($images->caption !=''):
								$return .=  strip_tags($images->caption);
							else:
								$return .= $placeholder; 
							endif;
							$return .= "</div>
									<div class='formatted_caption hide'>";
									$caption = $images->caption;
									if($user_type==3)
										$caption = $images->caption_d;
									 
									if($caption !=''):
										$return .=  $caption;
									else:
										$return .=  $placeholder;
									endif;
							$return .= "</div>";
							#Si no estamos pendientes de eliminación
							if($images->status!=1):
								$return .= "<a href='javascript:void(0);' class='delete' data-id='".$images->id."'>
												<img src='".base_url()."_img/backend/close.png'>
										</a>
										<a href='javascript:void(0);' class='drag' data-id='".$images->id."'>
											<img src='".base_url()."_img/backend/drag.png'>
										</a>";
							else:
								$return .= "<a href='javascript:void(0);' class='restore' data-id='".$images->id."'>
											<img src='".base_url()."_img/backend/restore.png'>
										</a>";
							endif;
							$return .= "</li>";
			endforeach;
		endif;
		$return .= "</ul>";
		return $return;
	}
	function get_select_field_options($section_id,$content_id){
		$user_type=$this->session->userdata('user_type');
	
		$field = "content_live";
		if($user_type==3):
			$field = "content_draft";
		endif;
		//home slides
		if(($content_id == 1 && $section_id == 23)): 
			$sql = "
					SELECT 	dr.id value, fd.content_live text,dr.status, 
						(SELECT 	fd.content_live text
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=".$content_id." 
					        and 	dr.section_id=".$section_id." 
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id  
					        and 	f.id='28'
			                        and dr.id = value
						 group 	by dr.id) as _order
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=".$content_id." 
					        and 	dr.section_id=".$section_id." 
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id  
						 	group 	by dr.id
					         order by cast(_order as decimal(38,10))  ASC";
		//social media icons
		elseif($content_id==1 && $section_id==2):
			$sql = "
					SELECT 	dr.id value, fd.content_live text,dr.status, 
						(SELECT 	fd.content_live text
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=".$content_id." 
					        and 	dr.section_id=".$section_id." 
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id  
					        and 	f.id='20'
			                        and dr.id = value
						 group 	by dr.id) as _order
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=".$content_id." 
					        and 	dr.section_id=".$section_id." 
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id  
						 	group 	by dr.id
					         order by cast(_order as decimal(38,10))  ASC";	
		//awards					         
		elseif($content_id==16 && $section_id==1):
			$sql = "
					SELECT value, COALESCE(CONCAT(text, ' - ', project), text) as text, status FROM (SELECT 	dr.id value, fd.content_live text,dr.status, 
						(SELECT 	fd.content_live text
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=16
					        and 	dr.section_id=1
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id
					        and 	f.id='211'
			                        and dr.id = value
						 group 	by dr.id) as project_id, 
						(SELECT 	fd.content_live text
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=4
					        and 	dr.section_id=5
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id  
					        and 	f.id='35'
			                        and dr.id = project_id
						 group 	by dr.id) as project
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=16
					        and 	dr.section_id=1
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id  
						 	group 	by dr.id ) A ";	

  


        //projects
		elseif($content_id==4 && $section_id==5):
			$sql = "
					SELECT * FROM (SELECT 	dr.id value, fd.content_live text,dr.status, 
						(SELECT 	fd.content_live text
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=4
					        and 	dr.section_id=5
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id  
					        and 	f.id='36'
			                        and dr.id = value
						 group 	by dr.id) as _order, 
						(SELECT 	fd.content_live text
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=4
					        and 	dr.section_id=5
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id  
					        and 	f.id='80'
			                        and dr.id = value
						 group 	by dr.id) as archived
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=4
					        and 	dr.section_id=5
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id  
						 	group 	by dr.id
					         order by cast(_order as decimal(38,10))  ASC ) A WHERE A.archived = '0'
					UNION ALL 
					SELECT 0, 'SEPARATOR HERE',0,0, 0
					UNION ALL 
					SELECT * FROM (SELECT 	dr.id value, fd.content_live text,dr.status, 
						(SELECT 	fd.content_live text
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=4
					        and 	dr.section_id=5
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id  
					        and 	f.id='36'
			                        and dr.id = value
						 group 	by dr.id) as _order, 
						(SELECT 	fd.content_live text
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=4
					        and 	dr.section_id=5
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id  
					        and 	f.id='80'
			                        and dr.id = value
						 group 	by dr.id) as archived
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=4
					        and 	dr.section_id=5
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id  
						 	group 	by dr.id
					         order by text ASC ) B WHERE B.archived = 'on'
				         ";		  
        //region
		elseif($content_id==4 && $section_id==8):
			$sql = "
					SELECT 	dr.id value, fd.content_live text,dr.status, 
						(SELECT 	fd.content_live text
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=".$content_id." 
					        and 	dr.section_id=".$section_id." 
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id  
					        and 	f.id='43'
			                        and dr.id = value
						 group 	by dr.id) as _order
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=".$content_id." 
					        and 	dr.section_id=".$section_id." 
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id  
						 	group 	by dr.id
					         order by cast(_order as decimal(38,10))  ASC";			 
        //project type
		elseif($content_id==4 && $section_id==7):
			$sql = "
					SELECT 	dr.id value, fd.content_live text,dr.status, 
						(SELECT 	fd.content_live text
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=".$content_id." 
					        and 	dr.section_id=".$section_id." 
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id  
					        and 	f.id='41'
			                        and dr.id = value
						 group 	by dr.id) as _order
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=".$content_id." 
					        and 	dr.section_id=".$section_id." 
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id  
						 	group 	by dr.id
					         order by cast(_order as decimal(38,10))  ASC";							           					              
		// team members		         
		elseif($content_id==3 && $section_id==10):
			$sql = "
					SELECT 	dr.id value, fd.content_live text,dr.status, 
						(SELECT 	fd.content_live text
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=".$content_id." 
					        and 	dr.section_id=".$section_id." 
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id  
					        and 	f.id='86'
			                        and dr.id = value
						 group 	by dr.id) as _order
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=".$content_id." 
					        and 	dr.section_id=".$section_id." 
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id  
						 	group 	by dr.id
					         order by cast(_order as decimal(38,10))  ASC";
		// employee type categories					         
		elseif($content_id==3 && $section_id==4):
			$sql = "
					SELECT 	dr.id value, fd.content_live text,dr.status, 
						(SELECT 	fd.content_live text
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=".$content_id." 
					        and 	dr.section_id=".$section_id." 
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id  
					        and 	f.id='82'
			                        and dr.id = value
						 group 	by dr.id) as _order
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=".$content_id." 
					        and 	dr.section_id=".$section_id." 
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id  
						 	group 	by dr.id
					         order by cast(_order as decimal(38,10))  ASC";
		 // advocacy
		elseif($content_id==10 && $section_id==1):
			$sql = "
					SELECT 	dr.id value, fd.content_live text,dr.status, 
						(SELECT 	fd.content_live text
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=".$content_id." 
					        and 	dr.section_id=".$section_id." 
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id  
					        and 	f.id='154'
			                        and dr.id = value
						 group 	by dr.id) as _order
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=".$content_id." 
					        and 	dr.section_id=".$section_id." 
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id  
						 	group 	by dr.id
					         order by _order asc";
		// research					         
		elseif($content_id==11 && $section_id==1):
			$sql = "
					SELECT 	dr.id value, fd.content_live text,dr.status, 
						(SELECT 	fd.content_live text
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=".$content_id." 
					        and 	dr.section_id=".$section_id." 
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id  
					        and 	f.id='161'
			                        and dr.id = value
						 group 	by dr.id) as _order
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=".$content_id." 
					        and 	dr.section_id=".$section_id." 
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id  
						 	group 	by dr.id
					         order by _order asc"; 
		// articles					          
		elseif($content_id==12 && $section_id==17):
			$sql = "
					SELECT 	dr.id value, fd.content_live text,dr.status, 
						(SELECT 	fd.content_live text
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=".$content_id." 
					        and 	dr.section_id=".$section_id." 
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id  
					        and 	f.id='180'
			                        and dr.id = value
						 group 	by dr.id) as _order
					        FROM 	data_row dr,fields f,field_data fd 
					        WHERE 	dr.content_id=f.content_id 
					        and 	dr.section_id=f.section_id 
					        and 	dr.content_id=".$content_id." 
					        and 	dr.section_id=".$section_id." 
					        and 	f.id=fd.fields_id 
					        and 	dr.id=fd.row_id  
						 	group 	by dr.id
					         order by _order asc";  					         					         
		else:								
			$sql = "SELECT 	dr.id value, fd.".$field." text,dr.status 
			        FROM 	data_row dr,fields f,field_data fd 
			        WHERE 	dr.content_id=f.content_id 
			        and 	dr.section_id=f.section_id 
			        and 	dr.content_id=".$content_id." 
			        and 	dr.section_id=".$section_id." 
			        and 	f.id=fd.fields_id 
			        and 	dr.id=fd.row_id 
			        and 	order_by=1
				  	group 	by dr.id
			      	order by fd.content_live asc ";	
		endif;
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0):
			$array = NULL;
			if(  ($content_id==9 && $section_id==24) || ($content_id==12 && $section_id==17)   || ($content_id==7 && $section_id==1)  || ($content_id==10 && $section_id==1) || ($content_id==11 && $section_id==1) ):

				if($content_id==9 && $section_id==24): $field_id = 168; endif;
				if($content_id==12 && $section_id==17): $field_id = 180; endif;
				if($content_id==7 && $section_id==1): $field_id = 147; endif;
				if($content_id==10 && $section_id==1): $field_id = 154; endif;
				if($content_id==11 && $section_id==1): $field_id = 161; endif;

				$sql2 = "
				SELECT 	dr.id value, IF(fd.content_live IS NULL or fd.content_live= '', '00-00-000', fd.content_live) as text ,dr.status 
		        FROM 	data_row dr,fields f,field_data fd 
		        WHERE 	dr.content_id=f.content_id 
		        and 	dr.section_id=f.section_id 
		        and 	dr.content_id=".$content_id."
		        and 	dr.section_id=".$section_id."
		        and 	f.id=fd.fields_id 
		        and 	dr.id=fd.row_id  
			 	and 	f.id=".$field_id."
			 	group 	by dr.id
		        order by dr.id asc";
		        //echo '<script>console.log("sql2 - '.$sql.' ");</script>';
		        $query2 = $this->db->query($sql2);
		        $result = $query->result();
		        $result2 = $query2->result();
		        foreach ($result as &$key ) {
					foreach ($result2 as $key2 ) {
						$var = "";

						if(($content_id==9 && $section_id==24) ||
						($content_id==12 && $section_id==17) ||
						($content_id==7 && $section_id==1) ||
						($content_id==10 && $section_id==1) ||
						($content_id==11 && $section_id==1)):
							$flag = true;
							
								if(($this->get_past_days(date("m-d-Y"),$key2->text)<=0)):
									$flag__ = true;
								else:
									$flag__ = false;
								endif;
							
							$flag = false;
						endif;
						if($key->value==$key2->value && (($flag && $flag__) || !$flag) ){
							$var = $key->text." / ".$key2->text;
							$key->text=$var;
							$array[] = $key;
						}
					}
				}
				usort($array,array($this,"order_calendar"));
				return $array;
			else:
				return $query->result();
			endif;
		else:
			return NULL;
		endif;
	}
	function order_calendar($a,$b){
		$dateA = substr($a->text,strpos($a->text,"/")+1,strlen($a->text));
		$dateB = substr($b->text,strpos($b->text,"/")+1,strlen($b->text));
		$dateA = trim($dateA);
		$dateB = trim($dateB);
		list($ma,$da,$ya)=explode("-",$dateA);
		list($mb,$db,$yb)=explode("-",$dateB);

		$compA = (int)($ya.''.$ma.''.$da);
		$compB = (int)($yb.''.$mb.''.$db);
		if($compA==$compB):
			return 0;
		else:
			return ($compA>$compB)? -1 : 1 ;
		endif;



	}
#********************************************************************************************************
	#intervalo de dias 
	function get_past_days($date1,$date2){
		list($m1,$d1,$y1) = explode("-",$date1);
		list($m2,$d2,$y2) = explode("-",$date2);
		$val1 = (int)($y1.''.$m1.''.$d1);
		$val2 = (int)($y2.''.$m2.''.$d2);
		return (int)$val1-(int)$val2;
	}



#********************************************************************************************************
	#Obtenemos las opciones para ingresarlas al select con javascript
	function get_select_field_options_($section_id,$content_id,$value=0,$text = ""){
		$this->lang->load('backend', $this->idioma);
		$sections = $this->get_select_field_options($section_id,$content_id);
		
		if($text=="")
			$text = $this->lang->line('multiple_new');
		
		if(($section_id == 8 && $content_id == 4) || ($section_id == 7 && $content_id == 4) || ($section_id == 21 && $content_id == 15)){
			$return ="";
		}else{
			$return = "<option value='-1'>".$text."</option>";
		}

		if($content_id==2 && $section_id==8) {
			foreach ($sections as &$m):
				$campos = $this->content->get_section_info(2, 8, $m->value);
				$campos = $this->content->get_section_info(3, 3, $campos['project']);
				$m->text = $campos['p_name'];
			endforeach;
		}
		//projects - ajax  
		if($section_id == 5 && $content_id == 4){


			if($sections):				
				$separator = false;
				foreach($sections as $section):		

					if($section->text == "SEPARATOR HERE"):
						$separator = true;
						$return.=" <optgroup label='ARCHIVED PROJECTS'>";
					else:
						$return .= "<option value='".$section->value."' class='op".$section->status."'";
						if($section->value==$value)	$return .= " selected ";
						$return .= ">".strip_tags($section->text)."</option>";  
					endif;
				endforeach;
				if($separator) $return.="</optgroup>";
			endif;
			return $return;


		}else{
			if($sections)
				foreach($sections as $section):
					$return .= "<option value='".$section->value."' class='op".$section->status."'";
					if($section->value==$value)
						$return .= " selected ";
					$return .= ">".strip_tags($section->text)."</option>";
				endforeach;
			return $return;
		}

	}
#********************************************************************************************************
	#Actualizamos el orden de las imagenes
	function update_image_order($images){
		$this->set_table("image");
		for($i=1;$i<=count($images);$i++):
			$array = array(
					'image_order'=>$i
				);
			$this->update($array,$images[$i-1]);
		endfor;
	}
#********************************************************************************************************
	#Actualizamos el orden de las imagenes del field
	function update_image_order_field($images){
		$user_type = $this->session->userdata('user_type');
		
		$this->set_table("file");
		for($i=1;$i<=count($images);$i++):
			if($user_type==3):
				$array = array(
						'image_order_d'=>$i
					);
			else:
				$array = array(
					'image_order'=>$i
				);
			endif;
			$this->update($array,$images[$i-1]);
		endfor;
	}	
	
	
#********************************************************************************************************
#********************************************************************************************************
#*****************************CONTENIDO FRONT*******************************************************
#********************************************************************************************************
#********************************************************************************************************
	#Obtenemos los campos con su data par front
	public function get_fields_data_front($content_id,$section_id,$row_id=1){
		$sql = 	"SELECT 	f.*,fd.id data_id,fd.fields_id,fd.content_live,fd.content_draft,fd.row_id,f.max_chars,f.type_id
				 FROM		fields f left join field_data fd
				 on 		f.id=fd.fields_id
				 where	f.content_id=".$content_id."
				 and		f.section_id=".$section_id."
				 and		fd.row_id=".$row_id."
				 order by 	f.order asc,f.id asc";
				 
		$query = $this->db->query($sql);
		
		$return = $query->result();
		
		#html_cut
		foreach($return as &$item):
			if(($item->type_id==4 || $item->type_id==8 || $item->type_id==12) && $item->max_chars>0):
				if($this->draft)
					$item->content_draft = $this->html_cut($item->content_draft, $item->max_chars);
				else
					$item->content_draft = $this->html_cut($item->content_live, $item->max_chars);
			endif;
		endforeach;
			
		return $return;
	}
#********************************************************************************************************
	#Obtenemos la información de la sección y contenido
	function get_section_info($content_id,$section_id,$row=1){
		$data = $this->get_fields_data_front($content_id,$section_id,$row);
		
		$result = NULL;
		foreach($data as $dat):
		if($this->draft)
			$result[$dat->name] = $dat->content_draft;
		else
			$result[$dat->name] = $dat->content_live;
		endforeach;
		
		return $result;
	}
#********************************************************************************************************
	#Obtenemos la información de las secciones multiples
	function get_section_multiple($content_id,$section_id){
		
		$field = "content_live";
		if($this->draft)
			$field = "content_draft";

		$sql = "SELECT 	dr.id value, fd.".$field." text,dr.id 
		        FROM 	data_row dr,fields f,field_data fd 
		        WHERE 	dr.content_id=f.content_id 
		        and 	dr.section_id=f.section_id 
		        and 	dr.content_id=".$content_id." 
		        and 	dr.section_id=".$section_id." 
		        and 	f.id=fd.fields_id 
		        and 	dr.id=fd.row_id 
		        and 	order_by=1
			  group 	by dr.id
		        order by fd.content_draft asc,dr.id asc";
		$query = $this->db->query($sql);
		$return = NULL;
		if ($query->num_rows() > 0):
			foreach ($query->result() as $row):
				$data = $this->get_fields_data_front($content_id,$section_id,$row->id);
				
				$result = NULL;
				foreach($data as $dat):
					if($this->draft):
						$result[$dat->name] = $dat->content_draft;
					else:
						$result[$dat->name] = $dat->content_live;
					endif;
				endforeach;
				$result["row"] = $row->id;
				$return[] =  $result;		
			endforeach;
		else:
			return NULL;
		endif;
		
		return $return;
	}	
#********************************************************************************************************
	#Obtenemos la información de las imágenes
	function get_images_data($content_id,$section_id){
			
		$draft_condition = " and status = 0 ";
		
		if($this->draft)	
			$draft_condition = " and status !=1 "; 
		
		$sql = "select	*
			  from	image
			  where	content_id=".$content_id."
			  and		section_id=".$section_id." ".$draft_condition."
			  order by 	image_order asc";
		$query = $this->db->query($sql);
		
		if ($query->num_rows() > 0):
			return $query->result();
		else:
			return NULL;
		endif;
	}
#********************************************************************************************************
	#Obtenemos la información en base a el valor del select
	function get_info_on_select($field_id,$value){
		$sql =     "SELECT 	content_id, section_id, row_id
				FROM 		`field_data` fd,fields f
				WHERE 	f.id = fd.fields_id
				AND 		fd.fields_id =".$field_id."
				and		fd.content_live like '%".$value."%'
				and		fd.content_live !=''";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0):
			$return = $query->result();
			foreach ($return as &$row):
				$row->fields = $this->get_section_info($row->content_id,$row->section_id,$row->row_id);
			endforeach;
			return $return;
		else:
			return NULL;
		endif;
	}	
#********************************************************************************************************
	#Obtenemos la información en base a el valor del selects
	function html_cut($text, $max_length){
		$tags   = array();
		    $result = "";
		
		    $is_open   = false;
		    $grab_open = false;
		    $is_close  = false;
		    $in_double_quotes = false;
		    $in_single_quotes = false;
		    $tag = "";
		
		    $i = 0;
		    $stripped = 0;
		
		    $stripped_text = strip_tags($text);
		
		    while ($i < strlen($text) && $stripped < strlen($stripped_text) && $stripped < $max_length)
		    {
		        $symbol  = $text{$i};
		        $result .= $symbol;
		
		        switch ($symbol)
		        {
		           case '<':
		                $is_open   = true;
		                $grab_open = true;
		                break;
		
		           case '"':
		               if ($in_double_quotes)
		                   $in_double_quotes = false;
		               else
		                   $in_double_quotes = true;
		
		            break;
		
		            case "'":
		              if ($in_single_quotes)
		                  $in_single_quotes = false;
		              else
		                  $in_single_quotes = true;
		
		            break;
		
		            case '/':
		                if ($is_open && !$in_double_quotes && !$in_single_quotes)
		                {
		                    $is_close  = true;
		                    $is_open   = false;
		                    $grab_open = false;
		                }
		
		                break;
		
		            case ' ':
		                if ($is_open)
		                    $grab_open = false;
		                else
		                    $stripped++;
		
		                break;
		
		            case '>':
		                if ($is_open)
		                {
		                    $is_open   = false;
		                    $grab_open = false;
		                    array_push($tags, $tag);
		                    $tag = "";
		                }
		                else if ($is_close)
		                {
		                    $is_close = false;
		                    array_pop($tags);
		                    $tag = "";
		                }
		
		                break;
		
		            default:
		                if ($grab_open || $is_close)
		                    $tag .= $symbol;
		
		                if (!$is_open && !$is_close)
		                    $stripped++;
		        }
		
		        $i++;
		    }
		
		    while ($tags)
		        $result .= "</".array_pop($tags).">";
		
		    return $result;
	}
#********************************************************************************************************
	#Obtenemos el listado de los files
	function get_files_result($content_id,$section_id,$field_id,$row_id,$op=1){
		$user_type = $this->session->userdata('user_type');
		
		$img_order = "image_order";
		$and = "";
		if($op==2):
			$img_order = "image_order_d";
			$and = " and status!=1 ";
		elseif($op==1):
			$and = " and status!=2 ";
		endif;
		
		$sql = "select	*
			  from 	file
			  where	content_id=".$content_id."
			  and		section_id=".$section_id."
			  and		field_id=".$field_id."
			  and		row_id=".$row_id.$and."
			  order by 	".$img_order." asc";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0):
			return $query->result();
		else:
			return NULL;
		endif;	
	}	
#********************************************************************************************************
	#Agregamos info al log
	function save_log($content_id,$operation,$reason=''){
		$array = array(
			'date' =>date('Y-m-d G:i:s' ),
			'user' =>$this->session->userdata('user_id'),
			'content_id' =>$content_id,
			'operation' => $operation,
			'reason' => $reason
		);
		$this->set_table("log");
		$this->insert($array);
		$this->set_table("content");
	}
}

?>