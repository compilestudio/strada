<?php

	/*
	 * Fecha de Creación: 22-dic-2011
	 * Autor: Victor Lopez
	 * Fecha Última Modificación: 
	 * Modificado por: 
	 * Descripción: 
	 * 
	 */

class Session_model extends MY_Model {
	
	function __construct() {
		
		parent::__construct();
		$this->set_table('user');
		
	}
	
	public function authorized($username, $password) {
			
		$authorized = false;

		#destruimos la sesión actual
		$this->session->unset_userdata('user_id');
		
		#buscamos un registro que coincida con el usuaruio
		$this->db->select('id, password,type');
		$this->db->from('user');
		$this->db->where('user', $username);
		
		$query = $this->db->get();
		
		#si encontramos un usuario
		if ($query->num_rows() == 1):
			
			$user = $query->row();
			
			#desencriptamos la contraseña de la base de datos
			$this->load->library('encrypt');
			$plain_password = $this->encrypt->decode($user->password);	
			$authorized = ($password === $plain_password);
			
			if ($authorized):
				
				#cargamos los datos del usuario a la sesión
				$row = $query->row();
				$this->session->set_userdata('user_id', $user->id);
				$this->session->set_userdata('user_type', $user->type);
				$this->session->unset_userdata('new_expiration');
				
			endif;
			
		endif;
			
		return ($authorized);
		
	}
	
	public function remember() {
		$this->session->set_userdata('new_expiration', 2419200); //4 weeks
	}
	
}	