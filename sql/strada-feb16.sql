-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 16, 2017 at 10:36 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `strada`
--

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Description of what you can change on this part',
  `father` bigint(20) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `content_order` int(11) NOT NULL,
  `front_display` int(1) NOT NULL DEFAULT '1',
  `pending_approval` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `title`, `description`, `father`, `deleted`, `content_order`, `front_display`, `pending_approval`) VALUES
(1, 'Home', '', -1, 0, 0, 1, 0),
(2, 'Approach', '', -1, 0, 0, 1, 0),
(3, 'Philosophy', '', 2, 0, 0, 1, 0),
(4, 'Team', '', -1, 0, 0, 1, 0),
(5, 'Projects', '', -1, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `content_section`
--

CREATE TABLE `content_section` (
  `content_id` bigint(20) NOT NULL,
  `section_id` bigint(20) NOT NULL,
  `multiple` tinyint(1) NOT NULL,
  `image` tinyint(1) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `autocomplete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `content_section`
--

INSERT INTO `content_section` (`content_id`, `section_id`, `multiple`, `image`, `width`, `height`, `order`, `description`, `autocomplete`) VALUES
(1, 1, 1, 0, 0, 0, 0, 'Edit the slides information here.', 0),
(1, 2, 0, 0, 0, 0, 0, 'Edit the Caption Information here.', 0),
(3, 3, 0, 0, 0, 0, 0, 'Edit the main information here.', 0),
(4, 3, 1, 0, 0, 0, 0, '', 0),
(5, 4, 1, 0, 0, 0, 0, '', 0),
(5, 5, 1, 0, 0, 0, 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `data_row`
--

CREATE TABLE `data_row` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `content_id` bigint(20) NOT NULL,
  `section_id` bigint(20) NOT NULL,
  `order` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `data_row`
--

INSERT INTO `data_row` (`id`, `content_id`, `section_id`, `order`, `status`) VALUES
(1, 1, 1, 0, 0),
(1, 1, 2, 0, 0),
(1, 3, 3, 0, 0),
(1, 4, 3, 0, 0),
(1, 5, 1, 0, 0),
(1, 5, 4, 0, 0),
(1, 5, 5, 0, 0),
(2, 1, 1, 0, 0),
(2, 4, 3, 0, 0),
(2, 5, 1, 0, 0),
(2, 5, 4, 0, 0),
(2, 5, 5, 0, 0),
(3, 4, 3, 0, 0),
(3, 5, 1, 0, 0),
(3, 5, 4, 0, 0),
(4, 4, 3, 0, 0),
(4, 5, 1, 0, 0),
(4, 5, 4, 0, 0),
(5, 5, 1, 0, 0),
(6, 5, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fields`
--

CREATE TABLE `fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(75) COLLATE utf8_unicode_ci DEFAULT NULL,
  `display_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_id` bigint(20) UNSIGNED NOT NULL,
  `type_id` int(10) UNSIGNED NOT NULL,
  `order` int(11) NOT NULL,
  `select_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `section_id` bigint(20) NOT NULL,
  `order_by` int(1) NOT NULL,
  `max_chars` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fields`
--

INSERT INTO `fields` (`id`, `name`, `display_text`, `content_id`, `type_id`, `order`, `select_id`, `section_id`, `order_by`, `max_chars`) VALUES
(1, 'img', 'Image (2000w x 1250t pixels)', 1, 7, 0, '0', 1, 0, 0),
(2, 'order', 'Order', 1, 27, 0, '0', 1, 1, 0),
(3, 'link', 'Link', 1, 9, 0, '0', 1, 0, 0),
(4, 'caption', 'Caption', 1, 4, 0, '0', 2, 0, 0),
(5, 'show', 'Show Caption?', 1, 13, 0, '0', 2, 0, 0),
(6, 'img1', 'Image 1 (650w x 650h pixels)', 3, 3, 0, '0', 3, 0, 0),
(7, 'img2', 'Image 2 (650w x 650h pixels)', 3, 3, 0, '0', 3, 0, 0),
(8, 'pull', 'Pull Text', 3, 4, 0, '0', 3, 0, 0),
(9, 'main', 'Main Text', 3, 8, 0, '0', 3, 0, 0),
(10, 'image', 'Image 1 (1w x 1h pixels)', 4, 7, 1, '0', 3, 0, 0),
(11, 'name', 'Nombre', 4, 1, 2, '0', 3, 1, 0),
(12, 'order', 'Orden', 4, 27, 6, '0', 3, 0, 0),
(13, 'position', 'Puesto', 4, 1, 4, '0', 3, 0, 0),
(14, 'mail', 'Correo Electrónico', 4, 1, 5, '0', 3, 0, 0),
(15, 'description', 'Descripción', 4, 8, 3, '0', 3, 0, 0),
(16, 'highlights', 'Highlights', 4, 8, 7, '0', 3, 0, 0),
(17, 'projects', 'Proyectos', 4, 8, 8, '0', 3, 0, 0),
(18, 'category', 'Category', 5, 1, 0, '0', 4, 1, 0),
(19, 'order_', 'Order', 5, 27, 0, '0', 4, 0, 0),
(20, 'name', 'Name', 5, 1, 0, '0', 5, 1, 0),
(21, 'category', 'Category', 5, 22, 0, '5,4', 5, 0, 0),
(22, 'order', 'Order', 5, 27, 0, '0', 5, 0, 0),
(23, 'image', 'Imagen ( 1w x 1h px)', 5, 7, 0, '0', 5, 0, 0),
(28, 'description', 'Descripción', 5, 6, 0, '0', 5, 0, 0),
(29, 'neighborhood', 'Neighborhood', 5, 5, 0, '0', 5, 0, 0),
(30, 'type', 'Type', 5, 1, 0, '0', 5, 0, 0),
(31, 'size', 'Size', 5, 1, 0, '0', 5, 0, 0),
(32, 'slider_description', 'Slider Description', 5, 6, 0, '0', 5, 0, 0),
(33, 'phrase', 'Phrase', 5, 8, 0, '0', 5, 0, 0),
(34, 'slider', 'Slider', 5, 28, 0, '0', 5, 0, 0),
(35, 'vcard', 'V-Card', 4, 7, 9, '0', 3, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `field_data`
--

CREATE TABLE `field_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fields_id` int(10) UNSIGNED NOT NULL,
  `content_live` text COLLATE utf8_unicode_ci,
  `content_draft` text COLLATE utf8_unicode_ci NOT NULL,
  `row_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `field_data`
--

INSERT INTO `field_data` (`id`, `fields_id`, `content_live`, `content_draft`, `row_id`) VALUES
(7, 1, '1.jpg', '', 1),
(8, 2, '1', '', 1),
(9, 3, 'google.com', '', 1),
(10, 1, '2.jpg', '', 2),
(11, 2, '2', '', 2),
(12, 3, '', '', 2),
(13, 4, '<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus a justo ullamcorper, commodo ipsum quis, suscipit libero. Donec sodales, odio eget lobortis semper, tellus lacus fermentum lorem, vel convallis magna dolor suscipit dui.</span>', '', 1),
(14, 5, 'on', '', 1),
(19, 6, 'p1.jpg', '', 1),
(20, 7, 'p2.jpg', '', 1),
(21, 8, 'Strada seeks to invest in U.S. markets that are positioned to grow faster than the rest of the country.', '', 1),
(22, 9, '<p>Though a recovery in the national US real estate market has been underway since 2011, the pace of recovery has varied between markets. The strongest improvement has occurred in markets where the underlying economy is leveraged to industries that are growing at rates faster than national GDP &ndash; particularly the technology and energy sectors. These top-performing markets tend to have a highly educated workforce, world-class educational institutions with strong research departments, diverse access to investment capital and strong ties to global markets.</p>\n<p>knowledge-based economies<br />Strada believes that these types of knowledge-based economies generally, and the San Francisco Bay Area in particular, will continue to outperform other regional economies in the short and long term for the following reasons:</p>\n<ul>\n<li>\n<p>Knowledge-based economies employ highly qualified workers who are not easily outsourced and are thus themselves valuable assets. These workers often prefer to live in vibrant urban centers such as the San Francisco Bay Area.</p>\n</li>\n<li>\n<p>Regions anchored by knowledge-based economies benefit from outsized jobs multipliers. For example, research shows that for every technology sector job created, four additional non-technology jobs are created in the region.</p>\n</li>\n<li>\n<p>Companies and sectors that were newly emerging in the last cycle are now maturing rapidly and experiencing less volatility in performance.</p>\n</li>\n</ul>\n<br />Strada has deep experience in the San Francisco Bay Area, arguably the strongest knowledge-<br /><br />\n<p>Though a recovery in the national US real estate market has been underway since 2011, the pace of recovery has varied between markets. The strongest improvement has occurred in markets where the underlying economy is leveraged to industries that are growing at rates faster than national GDP &ndash; particularly the technology and energy sectors. These top-performing markets tend to have a highly educated workforce, world-class educational institutions with strong research departments, diverse access to investment capital and strong ties to global markets.</p>\n<p>knowledge-based economies<br />Strada believes that these types of knowledge-based economies generally, and the San Francisco Bay Area in particular, will continue to outperform other regional economies in the short and long term for the following reasons:</p>\n<ul>\n<li>\n<p>Knowledge-based economies employ highly qualified workers who are not easily outsourced and are thus themselves valuable assets. These workers often prefer to live in vibrant urban centers such as the San Francisco Bay Area.</p>\n</li>\n<li>\n<p>Regions anchored by knowledge-based economies benefit from outsized jobs multipliers. For example, research shows that for every technology sector job created, four additional non-technology jobs are created in the region.</p>\n</li>\n<li>\n<p>Companies and sectors that were newly emerging in the last cycle are now maturing rapidly and experiencing less volatility in performance.</p>\n</li>\n</ul>\n<br />Strada has deep experience in the San Francisco Bay Area, arguably the strongest knowledge-', '', 1),
(162, 18, 'Featured Projects', '', 1),
(163, 19, '1', '', 1),
(164, 18, 'Office', '', 2),
(165, 19, '2', '', 2),
(166, 18, 'Residential', '', 3),
(167, 19, '3', '', 3),
(168, 18, 'Other', '', 4),
(169, 19, '4', '', 4),
(272, 20, 'Oakland City Center', '', 2),
(273, 21, '1', '', 2),
(274, 22, '', '', 2),
(275, 23, 'oakland.jpg', '', 2),
(276, 28, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras et viverra erat. Morbi condimentum purus id tellus lacinia aliquam. Pellentesque bibendum, metus at consequat tristique, leo augue dignissim velit, a semper sem justo sit amet arcu. In hac habitasse platea dictumst. Mauris vestibulum elit nec erat lacinia bibendum. Pellentesque id nisi commodo, accumsan orci a, imperdiet nulla. Maecenas dictum, elit quis lacinia aliquet, ipsum ipsum tempor nunc, sit amet accumsan sapien sapien at arcu. ', '', 2),
(277, 29, 'Mission Bay District, San Francisco', '', 2),
(278, 30, 'Residential', '', 2),
(279, 31, '52,955 sq. ft.', '', 2),
(280, 32, '“The Warriors are making an unprecedented, $1 billion-plus investment in\nSan Francisco. We’re the only sports team in America doing this all with\nprivate funds, on private land, with no public subsidy.”', '', 2),
(281, 33, '<p class=\"p1\"><strong>The Warriors are making an unprecedented, $1 billion-plus</strong></p>\n<p class=\"p1\"><strong>investment in San Francisco. We&rsquo;re the only sports team in</strong></p>\n<p class=\"p1\"><strong>America doing this all with private funds, on private land,</strong></p>\n<p class=\"p1\"><strong>with no public subsidy.</strong></p>\n<p class=\"p2\">&ndash; Rick Welts, President and Chief Operating Officer of the Golden State Warriors</p>', '', 2),
(282, 34, '0', '', 2),
(338, 20, 'Arena Warriors', '', 1),
(339, 21, '2', '', 1),
(340, 22, '', '', 1),
(341, 23, 'warriors_arena.jpg', '', 1),
(342, 28, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur a nunc scelerisque elit varius commodo id non nisi. Duis sodales porttitor magna a dictum. Donec cursus magna metus, in semper nunc elementum eu. Etiam lacinia tempus lacus et convallis. Sed venenatis erat id condimentum bibendum. Integer nec volutpat dui.', '', 1),
(343, 29, 'Mission Bay District, San Francisco', '', 1),
(344, 30, 'Residential', '', 1),
(345, 31, '52,955 sq. ft.', '', 1),
(346, 32, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur a nunc scelerisque elit varius commodo id non nisi. Duis sodales porttitor magna a dictum. Donec cursus magna metus, in semper nunc elementum eu. Etiam lacinia tempus lacus et convallis. Sed venenatis erat id condimentum bibendum. Integer nec volutpat dui. \n\nInteger sed purus efficitur, condimentum enim nec, scelerisque lorem. Nunc consectetur semper finibus. Aenean mattis mattis mi, eu tristique ipsum accumsan eu. Pellentesque id felis sed eros rhoncus laoreet ac condimentum magna.', '', 1),
(347, 33, '<p class=\"p1\"><em><strong>\"The Warriors are making an unprecedented, $1 billion-plus&nbsp;</strong></em><em><strong>investment in San Francisco. We&rsquo;re the only sports team in&nbsp;</strong></em><em><strong>America doing this all with private funds, on private land,&nbsp;</strong></em><em><strong>with no public subsidy.\"</strong></em></p>\n<p class=\"p2\"><em>&ndash; Rick Welts, President and Chief Operating Officer of the Golden State Warriors</em></p>', '', 1),
(348, 34, '0', '', 1),
(385, 10, 't1.png', '', 1),
(386, 11, 'Javi', '', 1),
(387, 15, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur a nunc scelerisque elit varius commodo id non nisi. Duis sodales porttitor magna a dictum. Donec cursus magna metus, in semper nunc elementum eu. Etiam lacinia tempus lacus et convallis. Sed venenatis erat id condimentum bibendum. Integer nec volutpat dui. Integer sed purus efficitur, condimentum enim nec, scelerisque lorem. Nunc consectetur semper finibus. Aenean mattis mattis mi, eu tristique ipsum accumsan eu. Pellentesque id felis sed eros rhoncus laoreet ac condimentum magna. <br /><br />Ut quis felis nec massa porta elementum vitae quis erat. Donec ut odio hendrerit, rutrum erat quis, commodo sapien. Nullam vel sem laoreet, accumsan nunc aliquet, pharetra ligula. Etiam facilisis molestie purus, ac tempor arcu bibendum eu. Maecenas sit amet maximus eros. Etiam fermentum posuere leo. Mauris ut diam sollicitudin orci mattis laoreet non ac est. Etiam vitae odio nec leo sollicitudin dictum a at felis. Aliquam ut lacus vel tellus imperdiet eleifend. Suspendisse ut purus eget nibh maximus aliquet. Curabitur tristique, metus nec consectetur feugiat, orci velit pellentesque risus, quis porta nisi nunc at leo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam eu dictum felis.', '', 1),
(388, 13, 'Co-Founder', '', 1),
(389, 14, 'j@mail.com', '', 1),
(390, 12, '1', '', 1),
(391, 16, '<ul>\n<li><span>Highlights 1</span></li>\n<li><span><span>Highlights 2</span></span></li>\n<li><span><span><span>Highlights 3</span></span></span></li>\n</ul>', '', 1),
(392, 17, '<ul>\n<li>Project 1</li>\n<li>Project 2</li>\n<li>Project 3</li>\n</ul>', '', 1),
(393, 35, 'Acuerdo_47-2008_Firmas_Electrónicas.pdf', '', 1),
(394, 10, 't1.png', '', 3),
(395, 11, 'Manuel', '', 3),
(396, 15, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur a nunc scelerisque elit varius commodo id non nisi. Duis sodales porttitor magna a dictum. Donec cursus magna metus, in semper nunc elementum eu. Etiam lacinia tempus lacus et convallis. Sed venenatis erat id condimentum bibendum. Integer nec volutpat dui. Integer sed purus efficitur, condimentum enim nec, scelerisque lorem. Nunc consectetur semper finibus. Aenean mattis mattis mi, eu tristique ipsum accumsan eu. Pellentesque id felis sed eros rhoncus laoreet ac condimentum magna. <br /><br />Ut quis felis nec massa porta elementum vitae quis erat. Donec ut odio hendrerit, rutrum erat quis, commodo sapien. Nullam vel sem laoreet, accumsan nunc aliquet, pharetra ligula. Etiam facilisis molestie purus, ac tempor arcu bibendum eu. Maecenas sit amet maximus eros. Etiam fermentum posuere leo. Mauris ut diam sollicitudin orci mattis laoreet non ac est. Etiam vitae odio nec leo sollicitudin dictum a at felis. Aliquam ut lacus vel tellus imperdiet eleifend. Suspendisse ut purus eget nibh maximus aliquet. Curabitur tristique, metus nec consectetur feugiat, orci velit pellentesque risus, quis porta nisi nunc at leo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam eu dictum felis.', '', 3),
(397, 13, 'Designer', '', 3),
(398, 14, 'm@mail.com', '', 3),
(399, 12, '3', '', 3),
(400, 16, '<ul>\n<li>Highlights 1</li>\n<li>Highlights 2</li>\n<li>Highlights 3</li>\n</ul>', '', 3),
(401, 17, '<ul>\n<li>Project 1</li>\n<li>Project 2</li>\n<li>Project 3</li>\n</ul>', '', 3),
(402, 35, 'Acuerdo_47-2008_Firmas_Electrónicas.pdf', '', 3),
(403, 10, 't2.png', '', 4),
(404, 11, 'Mia', '', 4),
(405, 15, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur a nunc scelerisque elit varius commodo id non nisi. Duis sodales porttitor magna a dictum. Donec cursus magna metus, in semper nunc elementum eu. Etiam lacinia tempus lacus et convallis. Sed venenatis erat id condimentum bibendum. Integer nec volutpat dui. Integer sed purus efficitur, condimentum enim nec, scelerisque lorem. Nunc consectetur semper finibus. Aenean mattis mattis mi, eu tristique ipsum accumsan eu. Pellentesque id felis sed eros rhoncus laoreet ac condimentum magna. <br /><br />Ut quis felis nec massa porta elementum vitae quis erat. Donec ut odio hendrerit, rutrum erat quis, commodo sapien. Nullam vel sem laoreet, accumsan nunc aliquet, pharetra ligula. Etiam facilisis molestie purus, ac tempor arcu bibendum eu. Maecenas sit amet maximus eros. Etiam fermentum posuere leo. Mauris ut diam sollicitudin orci mattis laoreet non ac est. Etiam vitae odio nec leo sollicitudin dictum a at felis. Aliquam ut lacus vel tellus imperdiet eleifend. Suspendisse ut purus eget nibh maximus aliquet. Curabitur tristique, metus nec consectetur feugiat, orci velit pellentesque risus, quis porta nisi nunc at leo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam eu dictum felis.', '', 4),
(406, 13, 'Developer', '', 4),
(407, 14, 'mi@mail.com', '', 4),
(408, 12, '4', '', 4),
(409, 16, '<ul>\n<li>Highlights 1</li>\n<li>Highlights 2</li>\n<li>Highlights 3</li>\n</ul>', '', 4),
(410, 17, '<ul>\n<li>Project 1</li>\n<li>Project 2</li>\n<li>Project 3</li>\n</ul>', '', 4),
(411, 35, 'Acuerdo_47-2008_Firmas_Electrónicas.pdf', '', 4),
(412, 10, 't2.png', '', 2),
(413, 11, 'Monica', '', 2),
(414, 15, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur a nunc scelerisque elit varius commodo id non nisi. Duis sodales porttitor magna a dictum. Donec cursus magna metus, in semper nunc elementum eu. Etiam lacinia tempus lacus et convallis. Sed venenatis erat id condimentum bibendum. Integer nec volutpat dui. Integer sed purus efficitur, condimentum enim nec, scelerisque lorem. Nunc consectetur semper finibus. Aenean mattis mattis mi, eu tristique ipsum accumsan eu. Pellentesque id felis sed eros rhoncus laoreet ac condimentum magna. <br /><br />Ut quis felis nec massa porta elementum vitae quis erat. Donec ut odio hendrerit, rutrum erat quis, commodo sapien. Nullam vel sem laoreet, accumsan nunc aliquet, pharetra ligula. Etiam facilisis molestie purus, ac tempor arcu bibendum eu. Maecenas sit amet maximus eros. Etiam fermentum posuere leo. Mauris ut diam sollicitudin orci mattis laoreet non ac est. Etiam vitae odio nec leo sollicitudin dictum a at felis. Aliquam ut lacus vel tellus imperdiet eleifend. Suspendisse ut purus eget nibh maximus aliquet. Curabitur tristique, metus nec consectetur feugiat, orci velit pellentesque risus, quis porta nisi nunc at leo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam eu dictum felis.', '', 2),
(415, 13, 'Designer', '', 2),
(416, 14, 'mo@mail.com', '', 2),
(417, 12, '2', '', 2),
(418, 16, '<ul>\n<li>Highlights 1</li>\n<li>Highlights 2</li>\n<li>Highlights 3</li>\n</ul>', '', 2),
(419, 17, '<ul>\n<li>Project 1</li>\n<li>Project 2</li>\n<li>Project 3</li>\n</ul>', '', 2),
(420, 35, 'Acuerdo_47-2008_Firmas_Electrónicas.pdf', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `field_section`
--

CREATE TABLE `field_section` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `icon` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='How we divide the inputs visualy on the cms';

--
-- Dumping data for table `field_section`
--

INSERT INTO `field_section` (`id`, `name`, `description`, `order`, `icon`) VALUES
(1, 'Slider', '', 0, 2),
(2, 'Caption', '', 0, 4),
(3, 'Main Content', '', 0, 4),
(4, 'Categories', '', 0, 15),
(5, 'Projects', '', 0, 13);

-- --------------------------------------------------------

--
-- Table structure for table `field_type`
--

CREATE TABLE `field_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `field_type`
--

INSERT INTO `field_type` (`id`, `type`) VALUES
(1, 'input_small'),
(2, 'textarea_small'),
(3, 'file_small'),
(4, 'text_editor_small'),
(5, 'input_big'),
(6, 'textarea_big'),
(7, 'file_big'),
(8, 'text_editor_big'),
(9, 'input_small_big'),
(10, 'textarea_small_big'),
(11, 'file_small_big'),
(12, 'text_editor_small_big'),
(13, 'switch_small'),
(14, 'switch_small_big'),
(15, 'select_small'),
(16, 'select_big'),
(17, 'select_small_big'),
(20, 'date_small'),
(21, 'date_small_big'),
(22, 'select_multiple_small'),
(23, 'select_multiple_small_big'),
(24, 'time_small'),
(25, 'time_small_big'),
(26, 'separator'),
(27, 'order_'),
(28, 'files');

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `content_id` bigint(20) NOT NULL,
  `section_id` bigint(20) NOT NULL,
  `field_id` bigint(20) NOT NULL,
  `row_id` bigint(20) NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caption` text COLLATE utf8_unicode_ci NOT NULL,
  `caption_d` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_order` int(11) NOT NULL,
  `image_order_d` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0=live,1=delete draft,2=new draft'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`id`, `content_id`, `section_id`, `field_id`, `row_id`, `img`, `caption`, `caption_d`, `image_order`, `image_order_d`, `status`) VALUES
(1, 5, 5, 34, 2, '58a483a399239.jpg', '', '', 1, 0, 0),
(2, 5, 5, 34, 2, '58a483a3bbcc2.jpg', '', '', 1, 0, 0),
(3, 5, 5, 34, 1, '58a49fcde8406.jpg', '', '', 1, 0, 0),
(4, 5, 5, 34, 1, '58a49fce1c96b.png', '', '', 1, 0, 0),
(5, 4, 3, 35, 1, '58a60f94436d4.pdf', '', '', 1, 0, 0),
(6, 4, 3, 35, 3, '58a60fad38eaa.pdf', '', '', 1, 0, 0),
(7, 4, 3, 35, 4, '58a60fbb8b101.pdf', '', '', 1, 0, 0),
(8, 4, 3, 35, 2, '58a60fc708d15.pdf', '', '', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `content_id` bigint(20) NOT NULL,
  `section_id` bigint(20) NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caption` text COLLATE utf8_unicode_ci NOT NULL,
  `image_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `date` datetime NOT NULL,
  `user` bigint(20) NOT NULL,
  `operation` int(11) NOT NULL,
  `reason` text COLLATE utf8_unicode_ci NOT NULL,
  `content_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `meta_data`
--

CREATE TABLE `meta_data` (
  `content_id` bigint(20) NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `section_icon`
--

CREATE TABLE `section_icon` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `section_icon`
--

INSERT INTO `section_icon` (`id`, `name`, `file`) VALUES
(2, 'Images', 'images.png'),
(3, 'Quicklinks', 'quicklinks.png'),
(4, 'Typewriter', 'typewriter.png'),
(5, 'Side Bar', 'sidebar.png'),
(6, 'News', 'news.png'),
(7, 'Calendar', 'calendar.png'),
(8, 'Map', 'map.png'),
(9, 'Phone', 'phone.png'),
(10, 'Side Bar Right', 'sidebar_right.png'),
(11, 'mail', 'mail.png'),
(12, 'Years', 'year.png'),
(13, 'Projects', 'projects.png'),
(14, 'blogs', 'blog.png'),
(15, 'categories', 'categories.png'),
(16, 'client list', 'client_list.png'),
(17, 'awards', 'award.png'),
(18, 'bios and background', 'bios_and_background.png'),
(19, 'socila media', 'social_media.png'),
(20, 'meta data', 'meta_data.png'),
(21, 'contact information', 'contact.png'),
(22, 'people', 'people.png'),
(23, 'drawings', 'drawings.png'),
(24, 'address', 'address.png'),
(25, 'social networking link', 'social_networking_links.png'),
(26, 'rss feeds', 'rss_feeds.png'),
(27, 'navigation', 'navigation.png'),
(28, 'job posting', 'job_posting.png'),
(29, 'question', 'question.png'),
(30, 'products', 'products.png'),
(31, 'tools', 'manufactures.png'),
(32, 'Animation', 'animation.png'),
(33, 'Timer', 'timer.png'),
(34, 'header', 'header2.png');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `user`, `password`, `type`) VALUES
(1, 'test', '/oUUG6+CxYsHBoZXf+OgYoomHUaw6pH9WB0mlShwFxbmpoFTmdjSCRq2ZZcumtpc3dqxIzYyachFlEZuVd9HmA==', 1),
(2, 'admin', 'Tl4ckpgYMtzgu8d/3VxuR8qFPKYF8QRfpl/GgJ2b3/AbOf1bH6OK7AzNeEsPvPOBpH//GAem8OeEnnneMooNcg==', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content_section`
--
ALTER TABLE `content_section`
  ADD PRIMARY KEY (`content_id`,`section_id`);

--
-- Indexes for table `data_row`
--
ALTER TABLE `data_row`
  ADD PRIMARY KEY (`id`,`content_id`,`section_id`),
  ADD KEY `content_id` (`content_id`),
  ADD KEY `section_id` (`section_id`),
  ADD KEY `fields_id_2` (`content_id`,`section_id`);

--
-- Indexes for table `fields`
--
ALTER TABLE `fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `select` (`select_id`);

--
-- Indexes for table `field_data`
--
ALTER TABLE `field_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fields_id` (`fields_id`,`row_id`),
  ADD KEY `fields_id_2` (`fields_id`),
  ADD KEY `content_id` (`row_id`);

--
-- Indexes for table `field_section`
--
ALTER TABLE `field_section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `field_type`
--
ALTER TABLE `field_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`date`,`user`);

--
-- Indexes for table `meta_data`
--
ALTER TABLE `meta_data`
  ADD PRIMARY KEY (`content_id`);

--
-- Indexes for table `section_icon`
--
ALTER TABLE `section_icon`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `fields`
--
ALTER TABLE `fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `field_data`
--
ALTER TABLE `field_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=421;
--
-- AUTO_INCREMENT for table `field_section`
--
ALTER TABLE `field_section`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `field_type`
--
ALTER TABLE `field_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `section_icon`
--
ALTER TABLE `section_icon`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
