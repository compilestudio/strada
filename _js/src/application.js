$(document).ready(function() {
	text_resize();
	//team_left();
	//project_left();
	//proj_filter();
});
$(window).resize(function(){
	text_resize();
	//team_left();
	//project_left();
});

function text_resize(){
	//Aplicamos el expand en los campos de texto
	// if($(window).width()>992){
	// 	$('.full').expander('destroy').expander({slicePoint: 1493});
	// }else{
	// 	$('.full').expander('destroy').expander({slicePoint: 993});
	// }
	
	$('.navbar-nav > li:nth-child(3) a:first').on('click',function(){
		if($(window).width()<992){
			console.log(1);
			return false;
		}
		return true;
	});
}
function team_left(){
	$('.team').removeClass("left");
	if($(window).width()>=576){
		$(".team:nth-child(1)").addClass("left");
		$(".team:nth-child(7n)").addClass("left");
	}else{
		$(".team:nth-child(1)").addClass("left");
		$(".team:nth-child(5n)").addClass("left");
	}
}
function project_left(){
	$('.project').removeClass("left");

	$('li.project').not(".hidden").each(function (i) {
		if (i % 3 == 0) $(this).addClass('left');
	});
}
function proj_filter(){
	$('.separated-name').on('click',function(){
		$('.separated-name').removeClass("active");
		$(this).addClass("active");
		var filterVal = $(this).attr("data-target");
		if(filterVal == 'all') {
		  $('ul#portfolio li.hidden').fadeIn('slow',function(){setTimeout(function(){project_left()},10);}).removeClass('hidden');
		}else {
		  $('ul#portfolio li').each(function() {
		  	var tags = $(this).attr("data-tag");
		  	tags = tags.split(",");
			if(tags.indexOf(filterVal)<0) {
				$(this).fadeOut('normal').addClass('hidden');
			} else {
				$(this).fadeIn('normal',function(){setTimeout(function(){project_left()},10);}).removeClass('hidden');
			}
		  });
		}
	});
}