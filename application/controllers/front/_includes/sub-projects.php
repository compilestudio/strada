<?php
$two = $this->uri->segment(2);
?>
<div class="submenu">
	<ul class="nav nav-tabs">
		<li role="presentation" <?php if(empty($two)) echo 'class="active"';?>>
			<a class="first" href="<?=site_url("projects");?>">
				FEATURED
			</a>
		</li>
		<li role="presentation" class="dropdown <?php if($two=='type') echo 'active';?>">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
				TYPES
					<?php if(!empty($_type_)):?>
						<span class="sub">
							<?=$_type_;?>
						</span>
					<?php endif;?>
			</a>
			<ul class="dropdown-menu">
				<?php if(!empty($types))
				foreach($types as $t):?>
					<li>
						<a href="<?=site_url('projects/type/'.$t['menu']);?>">
							<?=$t['type'];?>
						</a>
					</li>
				<?php endforeach;?>
			</ul>
		</li>
		<li role="presentation" class="dropdown <?php if($two=='theme') echo 'active';?>">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
				THEMES
					<?php if(!empty($_theme_)):?>
						<span class="sub">
							<?=$_theme_;?>
						</span>
					<?php endif;?>
			</a>
			<ul class="dropdown-menu">
				<?php if(!empty($themes))
					foreach($themes as $t):?>
						<li>
							<a href="<?=site_url('projects/theme/'.$t['menu']);?>">
								<?=$t['theme'];?>
							</a>
						</li>
					<?php endforeach;?>
			</ul>
		</li>
		<li role="presentation" class="<?php if($two=='all') echo 'active';?>">
			<a href="<?=site_url("projects/all");?>">
				ALL
			</a>
		</li>
	</ul>
</div>