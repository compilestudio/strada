<!doctype html>
<html lang="en">
<head>
	<link rel="icon" type="image/png" href="<?=base_url();?>_img/ico.ico">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="">

	<script type="application/ld+json">
	{
	  "@context": "http://schema.org",
	  "@type": "Organization",
	  "url": "<?=base_url();?>",
	  "logo": "<?=base_url();?>/_img/logo.png",
	  "sameAs": [
	    "https://www.facebook.com/",
	    "https://twitter.com/",
	    "https://www.pinterest.com//",
	    "https://www.linkedin.com/"
	  ],
	  "contactPoint": [{
	    "@type": "ContactPoint",
	    "telephone": "+1-415-383-7900",
	    "contactType": "customer service"
	  }]
	}
	</script>

	<title>
		Strada
	</title>
	<?php echo link_tag('_css/front/style.css?v=0.1');?>
	<script src="<?=base_url();?>_js/jquery.js"></script>
	<script src="<?=base_url();?>_js/app.min.js"></script>
	<script src="<?=base_url();?>_js/jquery.superslides.min.js"></script>
	<script src="<?=base_url();?>_js/bootstrap.min.js"></script>
</head>
<body>
	<header class="collapsed">
		<nav class="navbar navbar-toggleable-md navbar-light bg-faded bg-inverse">
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<a class="navbar-brand" href="<?=base_url();?>" style="color: #FFF">
				<img src="<?=base_url();?>img/logo.png" alt="Home Page">
			</a>
		  <div class="collapse navbar-collapse" id="navbarNavDropdown">
		    <ul class="navbar-nav">
		      <li class="nav-item dropdown">
		        <a class="nav-link dropdown-toggle my-nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		          Approach
		        </a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
		          <a class="dropdown-item my-nav-link" href="#">Philosophy</a>
		          <a class="dropdown-item my-nav-link" href="#">Strategy</a>
		        </div>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link my-nav-link" href="#">Team</a>
		      </li>
		      <li class="nav-item dropdown">
		        <a class="nav-link dropdown-toggle my-nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		          Projects
		        </a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
		          <a class="dropdown-item my-nav-link" href="#">Featured</a>
		          <a class="dropdown-item my-nav-link" href="#">Office</a>
		          <a class="dropdown-item my-nav-link" href="#">Residential</a>
		          <a class="dropdown-item my-nav-link" href="#">Other</a>
		        </div>
		      </li>
		      <li class="nav-item dropdown">
		        <a class="nav-link dropdown-toggle my-nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		          Projects
		        </a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
		          <a class="dropdown-item my-nav-link" href="#">Investment Management</a>
		          <a class="dropdown-item my-nav-link" href="#">Developmet</a>
		        </div>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link my-nav-link" href="#">Contact</a>
		      </li>
		    </ul>
		  </div>
		</nav>
	</header>
	<div class="container-fluid" style="padding: 0;">