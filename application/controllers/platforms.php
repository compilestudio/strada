<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Platforms extends MY_Controller {

#**************************************************************************************************
	public function investment(){
		$this->load->model("backend/content_model","content");

		// $data['cat'] = $cat;
		$data = null;
		$this->load_header_front($data);

		// $data['categories'] = $this->content->get_section_multiple(5,4);
		$plat = $this->content->get(7)->title;
		$sub = $this->content->get(8)->title;
		$data['title'] = $sub;
		$data['parent'] = $plat;
		$data['info'] = $this->content->get_section_info(8,3);

		$this->load->view('image_text',$data);
		$this->load_footer_front(null);
	}
	public function development(){
		$this->load->model("backend/content_model","content");

		$this->load_header_front(null);
		$data = null;

		$plat = $this->content->get(7)->title;
		$sub = $this->content->get(9)->title;
		$data['title'] = $sub;
		$data['parent'] = $plat;
		$data['info'] = $this->content->get_section_info(9,3);

		$this->load->view('image_text',$data);
		$this->load_footer_front(null);
	}


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
