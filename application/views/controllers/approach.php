<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Approach extends MY_Controller {

#**************************************************************************************************
	public function philosophy(){
		$this->load->model("backend/content_model","content");

		$this->load_header_front(null);
		$data = null;

		$data['info'] = $this->content->get_section_info(3,3);
		$data['page'] = $this->content->get(3);
		$data['parent'] = "Approach";
		$data['title'] = "Philosophy";

		$this->load->view('image_text',$data);
		$this->load_footer_front(null);
	}

	public function strategy(){
		$this->load->model("backend/content_model","content");

		$this->load_header_front(null);
		$data = null;

		$data['info'] = $this->content->get_section_info(6,3);
		$data['page'] = $this->content->get(3);
		$data['parent'] = "Approach";
		$data['title'] = "Strategy";

		$this->load->view('image_text',$data);
		$this->load_footer_front(null);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
