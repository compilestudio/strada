<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($id,$sub=0)
	{
		$this->load_header(":::Backend:::");
		$this->load->model("backend/content_model","content");
		$data = $this->content->content_info($id,$sub);
		$this->load->view('backend/content_view',$data);
		$this->load_footer();
	}
	public function events()
	{
		$this->load_header(":::Backend:::");
		$this->load->model("backend/content_model","content");
		$data = $this->content->content_info(9,0);

		$this->content->set_table("events");		
		$data["events"] = $this->content->list_rows("*");


		$this->load->view('backend/events',$data);
		$this->load_footer();
	}	
#******************************************************************************************************************************************************************
	#Funcion par aguardar el archivo subido en la sección de solo imágenes
	public function upload_file(){
		$this->load->helper('path');
		$tempFile = $_FILES['Filedata']['tmp_name'];
		$targetPath = set_realpath("uploads");
		$file = uniqid() .".". pathinfo($_FILES['Filedata']['name'], PATHINFO_EXTENSION);;
		
		#$file = $_FILES['Filedata']['name'];
		
		$targetFile =  str_replace('//','/',$targetPath) . $file;
		
		move_uploaded_file($tempFile,$targetFile);
		
		#insertamos en la bd
		$content_id = $this->input->post("content_id");
		$section_id = $this->input->post("section_id");
		$user_type = $this->input->post('user_type');
		
		$this->load->model("backend/content_model","content");
		
		$id_ = $this->content->image_(-1,$content_id,$section_id,"",$file,$user_type);

		echo $file.",".$id_;
	}
#******************************************************************************************************************************************************************
	#Funcion par aguardar el archivo subido en la sección de solo imágenes
	public function upload_file_field(){
		$this->load->helper('path');
		$tempFile = $_FILES['Filedata']['tmp_name'];
		$targetPath = set_realpath("uploads");
		$file = uniqid() .".". pathinfo($_FILES['Filedata']['name'], PATHINFO_EXTENSION);;
		
		#$file = $_FILES['Filedata']['name'];
		
		$targetFile =  str_replace('//','/',$targetPath) . $file;
		
		move_uploaded_file($tempFile,$targetFile);
		
		#insertamos en la bd
		$content_id = $this->input->post("content_id");
		$section_id = $this->input->post("section_id");
		$field_id = $this->input->post("field_id");
		$row_id = $this->input->post("row_id");
		$user_type = $this->input->post('user_type');
		
		$this->load->model("backend/content_model","content");
		
		$id_ = $this->content->image_field(-1,$content_id,$section_id,"",$file,$user_type,$field_id,$row_id);

		echo $file.",".$id_.",".$field_id;
	}
#******************************************************************************************************************************************************************
	#Funcion para cargar el archivo en los campos
	public function upload_file_fields(){
		$this->load->helper('path');
		$tempFile = $_FILES['Filedata']['tmp_name'];
		$targetPath = set_realpath("uploads");
		$file = uniqid() .".". pathinfo($_FILES['Filedata']['name'], PATHINFO_EXTENSION);;
		
		$file = $_FILES['Filedata']['name'];
		
		$targetFile =  str_replace('//','/',$targetPath) . $file;
		
		move_uploaded_file($tempFile,$targetFile);
		
		echo $file;
	}
#******************************************************************************************************************************************************************
	#Funcion para eliminar la imÃ¡gen	
	public function delete_image(){
		$id = $this->input->post('id');
		$this->load->model("backend/content_model","content");
		$this->content->delete_image($id);
		
	}
#******************************************************************************************************************************************************************
	#Funcion para eliminar la imagen del field	
	public function delete_image_field(){
		$id = $this->input->post('id');
		$this->load->model("backend/content_model","content");
		$this->content->delete_image_field($id);
		
	}
#******************************************************************************************************************************************************************
	#Funcion para restaurar la imagen	
	public function restore_image(){
		$id = $this->input->post('id');
		$this->load->model("backend/content_model","content");
		$this->content->restore_image($id);
		
	}
#******************************************************************************************************************************************************************
	#Funcion para restaurar la imagen	
	public function restore_image_field(){
		$id = $this->input->post('id');
		$this->load->model("backend/content_model","content");
		$this->content->restore_image_field($id);
		
	}
#******************************************************************************************************************************************************************
	#Funcion para actualizar el caption de las imÃ¡genes	
	public function update_image_caption(){
		$id = $this->input->post('id');
		$caption = $this->input->post('caption');
		$this->load->model("backend/content_model","content");
		
		$this->content->image_($id,0,0,$caption);
		
	}
#******************************************************************************************************************************************************************
	#Funcion para actualizar el caption de las imagenes del campo field	
	public function update_image_caption_field(){
		$id = $this->input->post('id');
		$caption = $this->input->post('caption');
		$this->load->model("backend/content_model","content");
		
		$user_type=$this->session->userdata('user_type');
		$this->content->image_field($id,0,0,$caption,"",$user_type);
		
	}
#******************************************************************************************************************************************************************
	#Funcion para guardar el contenido de una secciÃ³n
	public function save_section($content_id,$section_id,$row_id){
		$user_type=$this->session->userdata('user_type');
		
		$this->load->model("backend/content_model","content");
		$fields = $this->content->get_field_list($content_id,$section_id);
		
		#Si es nueva insertamos en la tabla el row
		$this->content->set_table("data_row");
		if($row_id==-1):
			
			$sql = "	SELECT max(id) next 
					FROM data_row 
					WHERE content_id= ".$content_id."
					and section_id=".$section_id;
			$query = $this->db->query($sql);			
			if ($query->num_rows() > 0):
				$row = $query->result();
				$next_id = $row[0]->next;
				$next_id++;
			else:
				$next_id=0;
			endif;
			
			$row_id = $data['id'] =$next_id;	
			$data = array(
					'content_id' => $content_id,
					'section_id' => $section_id,
					'id' => $row_id
			);
			
			if($user_type==3):
				$data['status'] = 2;
			endif;
			
			$this->content->insert($data);
		else:
			if($user_type==3):
				$data['status'] = 3;
				$this->content->update($data,$row_id);
			endif;	
		endif;
		
		
		$this->content->set_table("field_data");

		#Store coordinates
		$coords = 0;

		#insertamos los campos del row, los ponemos en el campo draft
		foreach($fields as $field):
			$value = $this->input->post($field->name);			
			$field_id = $this->input->post($field->name."_id");

			#Guardamos las coordenadas
			/*
			if($field_id==48):
				$value = $coords;
			endif;
			*/

			#quitamos si no viene el ID del campo, como cuando son separadores
			if($field_id):
				#Vemos si es mÃºltiple
				if($field->type_id==22 || $field->type_id==23):
					if($value!="")
						$value = implode(",",$value);
				endif;
				
				$where = array('row_id' => $row_id,'fields_id'=>$field_id);
				
				#Obtnemos la información previamente almacenada
				$previous = $this->content->list_rows('*',$where);
				
				
				#Eliminamos la info previamente almacenada
				$this->db->delete('field_data', $where); 
				if($field_id!=0):
					$array = array(
								'fields_id' =>$field_id,
								'row_id' => $row_id 
							);
					if($user_type==3):
						$array['content_draft'] = $value;
						$array['content_live'] = $previous[0]->content_live;
					else: 
						$array['content_live'] = $value;
					endif;
					$this->content->insert($array);
					#echo $this->db->last_query()."\n\n";
				endif;

				#Codificamos el address
				/*
				if($field_id==47):
					$coords_ = $this->lookup($value." San Francisco, CA");
					if($coords_):
						$coords = $coords_['latitude'].",".$coords_['longitude'];
					endif;
				endif;
				*/

			endif;
		endforeach;
		$return_values = $this->content->get_select_field_options_($section_id,$content_id,$row_id);
		echo $return_values;
		
	}
#Funcion para buscar el address
	function lookup($string){

		$string = str_replace (" ", "+", urlencode($string));
		$details_url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$string."&sensor=false";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $details_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = json_decode(curl_exec($ch), true);

		// If Status Code is ZERO_RESULTS, OVER_QUERY_LIMIT, REQUEST_DENIED or INVALID_REQUEST
		if ($response['status'] != 'OK') {
			return null;
		}

		print_r($response);
		$geometry = $response['results'][0]['geometry'];

		$longitude = $geometry['location']['lat'];
		$latitude = $geometry['location']['lng'];

		$array = array(
			'latitude' => $geometry['location']['lng'],
			'longitude' => $geometry['location']['lat'],
			'location_type' => $geometry['location_type'],
		);

		return $array;

	}
#******************************************************************************************************************************************************************
	#Funcion para guardar el contenido del Meta
	public function save_meta($content_id){
		$meta_title = $this->input->post('meta_page_title');
		$meta_keywords = $this->input->post('meta_keywords');
		$meta_description = $this->input->post('meta_description');
		
		$data = array(
					'meta_title' => $meta_title,
					'meta_keywords' => $meta_keywords,
					'meta_description' => $meta_description
				);
				
		$query = $this->db->get_where('meta_data', array('content_id' => $content_id));
		if ($query->num_rows() > 0):
			$this->db->where('content_id', $content_id);
			$this->db->update('meta_data', $data); 
		else:
			$data['content_id'] = $content_id;
			$this->db->insert('meta_data', $data);
		endif;
	}
#******************************************************************************************************************************************************************
	#Funcion para eliminar una seccion
	public function delete_section($content_id,$section_id,$row_id){
		
		$this->load->model("backend/content_model","content");
		
		$user_type=$this->session->userdata('user_type');
		if($user_type==3):
			$this->content->set_table("data_row");
			$info = $this->content->get($row_id);
			if($info->status!=2):
				$array = array('status'=>1);
				$this->content->update($array,$row_id);
			else:
				$this->content->set_table("content");
				#Eliminamos la informacion de la seccion
				$this->db->where('content_id', $content_id);
				$this->db->where('section_id', $section_id);
				$this->db->where('id', $row_id);
				$this->db->delete('data_row'); 
				
				#We delete the fields
				$fields = $this->content->get_field_list($content_id,$section_id);
				foreach($fields as $field):
					$this->db->where('fields_id',$field->id);
					$this->db->where('row_id',$row_id);
					$this->db->delete('field_data');

                    #We delete the files
                    $this->db->where('content_id', $content_id);
                    $this->db->where('section_id', $section_id);
                    $this->db->where('row_id',$row_id);
                    $this->db->where('field_id',$field->id);
                    $this->db->delete('file');

				endforeach;


            endif;
		else:
			$this->content->set_table("content");
			#Eliminamos la informacion de la seccion
			$this->db->where('content_id', $content_id);
			$this->db->where('section_id', $section_id);
			$this->db->where('id', $row_id);
			$this->db->delete('data_row'); 
			
			#We delete the fields
			$fields = $this->content->get_field_list($content_id,$section_id);
			foreach($fields as $field):
				$this->db->where('fields_id',$field->id);
				$this->db->where('row_id',$row_id);
				$this->db->delete('field_data');
                #We delete the files
                $this->db->where('content_id', $content_id);
                $this->db->where('section_id', $section_id);
                $this->db->where('row_id',$row_id);
                $this->db->where('field_id',$field->id);
                $this->db->delete('file');
			endforeach;


		endif;
		
		$this->content->set_table("content");
		echo $this->content->get_select_field_options_($section_id,$content_id);
	}
#******************************************************************************************************************************************************************
	#Funcion para obtener los campos en json
	public function get_fields_json($content_id,$section_id,$row=1){
		$this->load->model("backend/content_model","content");
		
		$object = $this->content->get_fields_data($content_id,$section_id,$row);
		
		header('Content-type: text/json');
		echo json_encode($object);
	}
#******************************************************************************************************************************************************************
	#Obtenemos los archivos para el field tipo file
	public function get_files(){
		$content_id = $this->input->post("content_id");
		$section_id = $this->input->post("section_id");
		$field_id = $this->input->post("field_id");
		$row_id = $this->input->post("row");
		
		$this->load->model("backend/content_model","content");
		$return = $this->content->get_files($content_id,$section_id,$field_id,$row_id);
		echo $field_id."##@@##".$return;
	}
#******************************************************************************************************************************************************************
	#Funcion para obtener los rows de los multiples
	public function get_rows_json(){
		$content_id = $this->input->get("content_id");
		$section_id = $this->input->get("section_id");
		$query = $this->input->get("query");
		$user_type=$this->session->userdata('user_type');
		if($user_type==3):
			$content_type = "draft";
		else:
			$content_type = "live";
		endif;
		
		$sql = "SELECT 	dr.id value, fd.content_".$content_type." label 
		        FROM 	data_row dr,fields f,field_data fd 
		        WHERE 	dr.content_id=f.content_id 
		        and 	dr.section_id=f.section_id 
		        and 	dr.content_id=".$content_id." 
		        and 	dr.section_id=".$section_id." 
		        and 	f.id=fd.fields_id 
		        and 	dr.id=fd.row_id 
		        and 	order_by=1
		        and 	fd.content_".$content_type." like '%".$query."%'
			  group 	by dr.id
		        order by label asc";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0):
			$object = $query->result();
		else:
			$object = NULL;
		endif;
		
		$callback = $this->input->get("callback");
		
		header('content-type: application/json; charset=utf-8');
		#echo $callback.'({"totalResultsCount":'.$query->num_rows().',"info":'.json_encode($object).')';
		#echo json_encode($object);
		echo $callback . '('.json_encode($object).')';
	}	
#******************************************************************************************************************************************************************
	#Funcion para actualizar el orden de las imÃ¡genes
	public function update_image_order(){
		$images = $this->input->post("image");
		$this->load->model("backend/content_model","content");
		$this->content->update_image_order($images);
	}	
#******************************************************************************************************************************************************************
	#Funcion para actualizar el orden de las imagenes del field
	public function update_image_order_field(){
		$images = $this->input->post("image");
		$this->load->model("backend/content_model","content");
		$this->content->update_image_order_field($images);
	}	
	
	
	
#******************************************************************************************************************************************************************	
#******************************************************************************************************************************************************************
	#Funcion par aguardar el archivo subido
	public function upload_file2(){
		$this->load->helper('path');
		$tempFile = $_FILES['Filedata']['tmp_name'];
		$targetPath = set_realpath("uploads");
		
		$this->load->library('image_lib');
		#resize image 186x181
		$config['x_axis'] = '0';
		$config['y_axis'] = '0';
		$config['image_library'] = 'imagemagick';
		$config['library_path'] = '/usr/X11R6/bin/';
		$config['source_image']	= $tempFile;
		$config['width'] = '186';
		$config['height'] = '181';
		$this->image_lib->initialize($config); 
		
		$this->image_lib->crop();
		
		$file = $_FILES['Filedata']['name'];
		
		$targetFile =  str_replace('//','/',$targetPath) . $file;
		
		move_uploaded_file($tempFile,$targetFile);
		echo $file;
	}
#******************************************************************************************************************************************************************
	#Funcion par aguardar el archivo subido
	public function upload_file3(){
		$this->load->helper('path');
		$tempFile = $_FILES['Filedata']['tmp_name'];
		$targetPath = set_realpath("uploads");
		
		$this->load->library('image_lib');
		#resize image 770
		$config['x_axis'] = '0';
		$config['y_axis'] = '0';
		$config['image_library'] = 'imagemagick';
		$config['library_path'] = '/usr/X11R6/bin/';
		$config['source_image']	= $tempFile;
		$config['width'] = '770';
		$this->image_lib->initialize($config); 
		
		$this->image_lib->crop();
		
		$file = $_FILES['Filedata']['name'];
		
		$targetFile =  str_replace('//','/',$targetPath) . $file;
		
		move_uploaded_file($tempFile,$targetFile);
		echo $file;
	}
#******************************************************************************************************************************************************************
	#Funcion para guardar la informaciÃ³n del contenido
	function content_save(){
		
		$content_id = $this->input->post("id");
					
		#Armamos la informaciÃ³n para el contenido
		$data = array(
			'title' => $this->input->post("content_title_label"),
			'father' => $this->input->post("father_id"),
			'content_order' => $this->input->post("content_order")
		);

		$this->load->model('backend/content_model','content');
		$content_id = $this->content->save_content($data,$content_id);
		if($this->input->post("id")==19):
			#address
			$address = $this->input->post("address")." San Francisco";
			$result_ = $this->decodeAddress($address);
			$result_ = explode(",",$result_);
		endif;
		
		#Obtenemos los fields del contenido
		$fields = $this->content->content_fields($content_id);
		$section = $this->input->post('section_list');
		if(!$section):
			if($fields!='')
				for($i=0;$i<count($fields);$i++):
					$field_id = $this->input->post($fields[$i]['name']."_id");
					$field_value = $this->input->post($fields[$i]['name']);
					if($this->input->post("id")==19):
						if($fields[$i]['name']=="lat"):
							$field_value = $result_[0];
						endif;
						if($fields[$i]['name']=="long"):
							$field_value = $result_[1];
						endif;
					endif;
					$field_type = $fields[$i]['type'];
					$this->content->insert_field_value($field_id,$field_value,$field_type,$content_id);
				endfor;
			redirect("backend/content/".$content_id);
		else:
			#we insert
			$data = array(
				'title' => $this->input->post("section_title"),
				'father' => $content_id,
				'content_order' => $this->input->post("section_order"),
				'parent_category' => $this->input->post("parent_category_"),
				'sub_category' => $this->input->post("sub_category_")
			);
			$section = $this->content->save_content($data,$section);
			if($fields!='')
				for($i=0;$i<count($fields);$i++):
					$field_id = $this->input->post($fields[$i]['name']."_id");
					$field_value = $this->input->post($fields[$i]['name']);
					if($this->input->post("id")==19):
						if($fields[$i]['name']=="lat"):
							$field_value = $result_[0];
						endif;
						if($fields[$i]['name']=="long"):
							$field_value = $result_[1];
						endif;
					endif;
					$field_type = $fields[$i]['type'];
					$this->content->insert_field_value($field_id,$field_value,$field_type,$section);
				endfor;
			
			#Si trae subsecciones nivel 3
			if($this->input->post('v3')==1):
				$this->content->set_table("content_category");
				$categories = $this->content->list_rows('*','father=-1 and content_id='.$content_id,'category_order asc');
				if($categories):
					$this->db->delete('category_content', array('content_id' => $section)); 
					$this->content->set_table("category_content");
					foreach($categories as $cat):
						$param = array(
									'content_id' => $section,
									'parent_category' => $cat->id,
									'sub_category' => $this->input->post('category_'.$cat->id)
								 );
						$this->content->insert($param);
					endforeach;
				endif;
			endif;
			#Si trae subsecciones nivel 4
			if($this->input->post('v4')==1):
				$categories = $this->input->post('category_select');
				if(count($categories)>0):
					$this->db->delete('category_content', array('content_id' => $section)); 
					$this->content->set_table("category_content");
					foreach($categories as $cat):
						$param = array(
									'content_id' => $section,
									'parent_category' => $cat,
									'sub_category' => 0
								 );
						$this->content->insert($param);
					endforeach;
				endif;
			endif;
				
			
			redirect("backend/content/".$content_id."/".$section);
		endif;
	}
#******************************************************************************************************************************************************************
	#Funcion para guardar la informaciÃ³n del contenido enviado por el evento aprobado
	function content_save_event(){
		
		$content_id = $this->input->post("id");
					
		#Armamos la informaciÃ³n para el contenido
		$data = array(
			'title' => $this->input->post("content_title_label"),
			'father' => $this->input->post("father_id"),
			'content_order' => $this->input->post("content_order")
		);

		$this->load->model('backend/content_model','content');
		$content_id = $this->content->save_content($data,$content_id);
		
		#Obtenemos los fields del contenido
		$fields = $this->content->content_fields($content_id);
		$section = $this->input->post('section_list');
		if(!$section):
			if($fields!='')
				for($i=0;$i<count($fields);$i++):
					$field_id = $this->input->post($fields[$i]['name']."_id");
					$field_value = $this->input->post($fields[$i]['name']);
					$field_type = $fields[$i]['type'];
					$this->content->insert_field_value($field_id,$field_value,$field_type,$content_id);
				endfor;
			redirect("backend/content/".$content_id);
		else:
			#we insert
			$data = array(
				'title' => $this->input->post("section_title"),
				'father' => $content_id,
				'content_order' => $this->input->post("section_order"),
				'parent_category' => $this->input->post("parent_category_"),
				'sub_category' => $this->input->post("sub_category_")
			);
			$section = $this->content->save_content($data,$section);
			if($fields!='')
				for($i=0;$i<count($fields);$i++):
					$field_id = $this->input->post($fields[$i]['name']."_id");
					$field_value = $this->input->post($fields[$i]['name']);
					$field_type = $fields[$i]['type'];
					$this->content->insert_field_value($field_id,$field_value,$field_type,$section);
				endfor;
			
			#Si trae subsecciones nivel 3
			if($this->input->post('v3')==1):
				$this->content->set_table("content_category");
				$categories = $this->content->list_rows('*','father=-1 and content_id='.$content_id,'category_order asc');
				if($categories):
					$this->db->delete('category_content', array('content_id' => $section)); 
					$this->content->set_table("category_content");
					foreach($categories as $cat):
						$param = array(
									'content_id' => $section,
									'parent_category' => $cat->id,
									'sub_category' => $this->input->post('category_'.$cat->id)
								 );
						$this->content->insert($param);
					endforeach;
				endif;
			endif;
			#Si trae subsecciones nivel 4
			if($this->input->post('v4')==1):
				$categories = $this->input->post('category_select');
				if(count($categories)>0):
					$this->db->delete('category_content', array('content_id' => $section)); 
					$this->content->set_table("category_content");
					foreach($categories as $cat):
						$param = array(
									'content_id' => $section,
									'parent_category' => $cat,
									'sub_category' => 0
								 );
						$this->content->insert($param);
					endforeach;
				endif;
			endif;
			
			$this->content->set_table("event");
			$array = array('b'=>1);
			$this->content->update($array,$this->input->post("event_id"));
			$this->content->set_table("content");
			
			redirect("backend/event_approval/1/");
		endif;
	}
#******************************************************************************************************************************************************************
	#Funcion para guardar la informaciÃ³n del contenido enviado por el evento aprobado
	function content_save_event2(){
		#echo "<pre>";
		#var_dump($_POST);
		#echo "</pre>";
		
		$content_id = $this->input->post("id");
					
		#Armamos la informaciÃ³n para el contenido
		$data = array(
			'title' => $this->input->post("content_title_label"),
			'father' => $this->input->post("father_id"),
			'content_order' => $this->input->post("content_order")
		);

		$this->load->model('backend/content_model','content');
		$content_id = $this->content->save_content($data,$content_id);
		$dates = $this->input->post('event_date');
		$fecha_count =0;
		foreach($dates as $d):
			#Obtenemos los fields del contenido
			$fields = $this->content->content_fields($content_id);
			$section = $this->input->post('section_list');
			if(!$section):
				if($fields!='')
					for($i=0;$i<count($fields);$i++):
						$field_id = $this->input->post($fields[$i]['name']."_id");
						$field_value = $this->input->post($fields[$i]['name']);
						$field_type = $fields[$i]['type'];
						$this->content->insert_field_value($field_id,$field_value,$field_type,$content_id);
					endfor;
				redirect("backend/content/".$content_id);
			else:
				#we insert
				$data = array(
					'title' => $this->input->post("section_title"),
					'father' => $content_id,
					'content_order' => $this->input->post("section_order"),
					'parent_category' => $this->input->post("parent_category_"),
					'sub_category' => $this->input->post("sub_category_")
				);
				$section = $this->content->save_content($data,$section);
				if($fields!='')
					for($i=0;$i<count($fields);$i++):
						#echo $fields[$i]['name']."<br>";
						if($fields[$i]['name']=="event_date"):
							$field_value = $this->input->post($fields[$i]['name']);
							$field_value = $field_value[$fecha_count];
						else:
							$field_value = $this->input->post($fields[$i]['name']);
						endif;
						#echo $field_value."<br>";
						$field_id = $this->input->post($fields[$i]['name']."_id");
						$field_type = $fields[$i]['type'];
						$this->content->insert_field_value($field_id,$field_value,$field_type,$section);
					endfor;
				
				#Si trae subsecciones nivel 3
				if($this->input->post('v3')==1):
					$this->content->set_table("content_category");
					$categories = $this->content->list_rows('*','father=-1 and content_id='.$content_id,'category_order asc');
					if($categories):
						$this->db->delete('category_content', array('content_id' => $section)); 
						$this->content->set_table("category_content");
						foreach($categories as $cat):
							$param = array(
										'content_id' => $section,
										'parent_category' => $cat->id,
										'sub_category' => $this->input->post('category_'.$cat->id)
									 );
							$this->content->insert($param);
						endforeach;
					endif;
				endif;
				#Si trae subsecciones nivel 4
				if($this->input->post('v4')==1):
					$categories = $this->input->post('category_select');
					if(count($categories)>0):
						$this->db->delete('category_content', array('content_id' => $section)); 
						$this->content->set_table("category_content");
						if($categories)
						foreach($categories as $cat):
							$param = array(
										'content_id' => $section,
										'parent_category' => $cat,
										'sub_category' => 0
									 );
							$this->content->insert($param);
						endforeach;
					endif;
				endif;
				
				$this->content->set_table("event");
				$array = array('b'=>1);
				$this->content->update($array,$this->input->post("event_id"));
				$this->content->set_table("content");
			endif;
			$fecha_count++;
		endforeach;
		redirect("backend/event_approval/create/1"); 
	}
#******************************************************************************************************************************************************************
	#Funcion para guardar la informaciÃ³n del contenido
	function event_reject($id){
		$this->load->model('backend/content_model','content');
		$this->content->set_table("event");
		$array = array('b'=>2);
		$this->content->update($array,$this->input->post("event_id"));
		$this->content->set_table("content");
		echo "1";
	}
#******************************************************************************************************************************************************************
	#Funcion para guardar la informaciÃ³n del contenido
	function delete($id){
		#Eliminamos de la tabla de contenido
		$this->load->model('backend/content_model','content');
		$data = array(
			'deleted' => 1
		);
		$this->content->update($data,$id);
		
		echo "1";
	}
#******************************************************************************************************************************************************************
	#funcion para agregar una categoria
	function add_category(){
		$id = $this->input->post("id");
		$name = $this->input->post("name");
		$order = $this->input->post("order");
		$father = $this->input->post("father");
		$content_id = $this->input->post("content_id");
		$img = $this->input->post("img");
		
		$this->load->model("backend/content_model","content");
		$this->content->set_table("content_category");
		$data = array(
					'name' => $name,
					'father'=>$father,
					'category_order' => $order,
					'content_id' => $content_id,
					'img' => $img
				);
		if($id==-1):
			$id = $this->content->insert($data);
		else:
			$this->content->update($data,$id);
		endif;
		
		echo $id;
	}
#******************************************************************************************************************************************************************
	#funcion para obtener las categorias
	function get_categories(){
		$this->lang->load('backend', $this->idioma);
		
		$father = $this->input->post("father");
		$content_id = $this->input->post("content_id");
		$op = $this->input->post("op");
		$selected_id = $this->input->post("selected_id");
		
		$this->load->model("backend/content_model","content");
		$this->content->set_table("content_category");
		
		#We get them
		$categories = $this->content->list_rows('*','father='.$father.' and content_id='.$content_id,'category_order asc');
		
		if($op==2):
			$return = "<option value='-1'>".$this->lang->line('select_subcat')."</option>";
		else:
			$return = "<option value='-1'>".$this->lang->line('create_subcat')."</option>";
		endif;
		if($categories)
		foreach($categories as $cat):
			$return .= "<option value='".$cat->id."' data-order='".$cat->category_order."'";
			if($selected_id==$cat->id)
				$return .= " selected 	";
			$return .=">".$cat->name."</option>";
		endforeach;
		
		echo $return;
	}
#******************************************************************************************************************************************************************
	#funcion para obtener las categorias
	function get_sub_pages(){
		$this->lang->load('backend', $this->idioma);
		
		$parent_cat = $this->input->post("parent_cat");
		$sub_cat = $this->input->post("sub_cat");
		$content_id = $this->input->post("content_id");
		$sub_page_id = $this->input->post("sub_page_id");
		
		$this->load->model("backend/content_model","content");
		
		#We get them
		if($sub_cat>0):
			$sub_pages = $this->content->list_rows('*','parent_category='.$parent_cat.' and sub_category='.$sub_cat.' and deleted=0 and father='.$content_id,'content_order asc');
		else:
			$sub_pages = $this->content->list_rows('*','parent_category='.$parent_cat.' and deleted=0 and father='.$content_id,'content_order asc');
		endif;
		
		$return = "<option value='-1'>".$this->lang->line('create_new')."</option>";
			
		if($sub_pages)
		foreach($sub_pages as $sub):
			$return .= "<option value='".$sub->id."'";
			if($sub_page_id==$sub->id)
				$return .= " selected ";				
			$return .=">".$sub->title."</option>";
		endforeach;
		
		echo $return;
	}
#******************************************************************************************************************************************************************
	#Funcion para eliminar una categoria
	function delete_category(){
		
		$id = $this->input->post('id');
		
		$this->db->delete('content_category', array('id' => $id)); 
		
		echo "1";
	}
#******************************************************************************************************************************************************************
	#Funcion para guardar el tÃ­tulo y el orden del
	public function save_title(){
		$this->load->model("backend/content_model","content");
		
		$data = array(
			'title' => $this->input->post('title'),
			'content_order'=> $this->input->post('order'),
			'front_display'=> $this->input->post('front_display')
		);
		$this->content->update($data,$this->input->post('id'));
	}
#************************************************************************************************************************
	#Decodificamos la direccion a lat y long
	function decodeAddress($address){
		define("MAPS_HOST", "maps.google.com");
		define("KEY", "ABQIAAAAhs_VtsoXFrrSgFgu9vIyzhROQ65eSPjAHv4W4yl7sTA8ZOdUvRTJvBTExxkJz8BVXJ1NQU7AhYZoig");
		$base_url = "http://" . MAPS_HOST . "/maps/geo?output=xml" . "&key=" . KEY;
		$address = str_replace(array("\r", "\r\n", "\n"), '', $address);
		$request_url = $base_url . "&q=" . urlencode($address);
	    $xml = simplexml_load_file($request_url) or die("url not loading");
	
	    $status = $xml->Response->Status->code;
		if (strcmp($status, "200") == 0) {
	      // Successful geocode
	      $geocode_pending = false;
	      $coordinates = $xml->Response->Placemark->Point->coordinates;
	      $coordinatesSplit = split(",", $coordinates);
	      // Format: Longitude, Latitude, Altitude
	      $lat = $coordinatesSplit[1];
	      $lng = $coordinatesSplit[0];
			return $lat.",".$lng;
	    }else{
	    	return "0,0";
	    }
	}
#************************************************************************************************************************
	#Hacemos un update del padre de los files, hack porque no funciona el parametro en el uploadify
	function update_file_father(){
		$father = $this->input->post("father");
		$id = $this->input->post("id");
		$this->load->model("backend/content_model","content");
		$this->content->set_table("file");
		$array = array(
				'row_id'=>$father	
				);
		$this->content->update($array,$id);
		
	}
#************************************************************************************************************************
	#Para hacer el preview, esta se eliminara al crear las plantillas
	function preview($id){
		$this->session->set_userdata('draft', 1);
		
		$dir = "/";
		
		switch($id):
			case 1:
				$dir = site_url("/");
				break;
			case 110:
				$dir = site_url("about");
				break;
			case 115:
				$dir = site_url("about/jobs");
				break;
			case 117:
				$dir = site_url("about/team");
				break;
			case 113:
				$dir = site_url("about/commissioners");
				break;
			case 114:
				$dir = site_url("about/fun_facts");
				break;
			case 111:
				$dir = site_url("about/history");
				break;
			case 116:
				$dir = site_url("about/faq");
				break;
			case 118:
				$dir = site_url("ourwork");
				break;
			case 136:
				$dir = site_url("ourwork/name");
				break;
			case 121:
				$dir = site_url("ourwork/timeline");
				break;
			case 119:
				$dir = site_url("whatsnew");
				break;
			case 120:
				$dir = site_url("whatsnew/press_releases");
				break;
			case 141:
				$dir = site_url("whatsnew/public_meetings");
				break;
			case 123:
				$dir = site_url("whatsnew/business_opps");
				break;
			case 124:
				$dir = site_url("whatsnew/funding_opps");
				break;
			case 138:
				$dir = site_url("whatsnew/sources");
				break;
			case 125:
				$dir = site_url("whatsnew/links");
				break;
			case 126:
				$dir = site_url("impact");
				break;
			case 127:
				$dir = site_url("impact/mesure_c");
				break;
			case 128:
				$dir = site_url("impact/mesure_j");
				break;
			case 129:
				$dir = site_url("impact/our_future");
				break;
			case 142:
				$dir = site_url("impact/fun_facts");
				break;
			case 137:
				$dir = site_url("impact/your_words");
				break;
			case 140:
				$dir = site_url("investors");
				break;
			case 130:
				$dir = site_url("investors/bond_document");
				break;
			case 131:
				$dir = site_url("investors/operating_data");
				break;
			case 132:
				$dir = site_url("investors/financial_data");
				break;
			case 133:
				$dir = site_url("get_involved");
				break;
			case 143:
				$dir = site_url("get_involved/bussiness_opps");
				break;
			case 144:
				$dir = site_url("get_involved/funding_opps");
				break;
			case 139:
				$dir = site_url("get_involved/calendar");
				break;
			case 145:
				$dir = site_url("get_involved/eblast");
				break;
			case 134:
				$dir = site_url("get_involved/contat_us");
				break;
			case 153:
				$dir = site_url("get_involved/parters_login");
				break;
			case 154:
				$dir = site_url("parters/welcome");
				break;
		endswitch;
			
		redirect($dir);
	}
#************************************************************************************************************************
	#Se envian los cambios a un aprobador
	function submit_changes($id){
		$this->load->model("backend/content_model","content");
		$array = array('pending_approval'=>1,'rejected'=>0);
		$this->content->update($array,$id);
		
		#Guardamos el log		
		$this->content->save_log($id,1);
		
		redirect("backend/content/".$id);
	}
#************************************************************************************************************************
	#Se aprueban los cambios
	function approve_changes($id){
		$this->load->model("backend/content_model","content");
		#Actualizamos el estado del content
		$this->content->update(array('pending_approval'=>0),$id);
		
		#Actualizamos los rows
		$this->db->where('content_id', $id);
		$this->db->update('data_row', array('status'=>0));
		
		#Actualizamos los datos de los campos a live
		$sql = "SELECT GROUP_CONCAT( id ) ids
			FROM 		fields
			WHERE 	content_id =".$id;
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0):
			$ids = $query->result();
			$ids = $ids[0]->ids;
			
			$sql = "update `field_data` 
				set content_live = content_draft
				WHERE fields_id in (".$ids.")";
			$this->db->query($sql);
		endif;
		
		#Eliminamos los archivos/imagenes que estan por eliminar
		$sql = "delete from `file` 
			WHERE content_id=".$id."
			and status=1";
		$this->db->query($sql);
		
		#Actualizamos el status a live de los files que no se eliminaron
		$sql = "update 	`file` 
			set 		image_order=image_order_d,
					status=0,
					caption=caption_d
			WHERE 	content_id=".$id;
		$this->db->query($sql);
		
		#Guardamos el log		
		$this->content->save_log($id,2);
		
		redirect("backend/content/".$id);
	}
#************************************************************************************************************************
	#Rechazamos el cambio
	function reject_changes(){
		$this->load->model("backend/content_model","content");
		
		$id= $this->input->post("id");
		$reason= $this->input->post("reason");
		
		$this->content->update(array('rejected'=>1,'pending_approval'=>0),$id);
		
		#Guardamos el log		
		$this->content->save_log($id,3,$reason);
	}
#************************************************************************************************************************
	#Desplegamos las diferencias editadas
	function view_diff(){
		$this->load->library('finediff');
		
		$content_id = $this->input->post("content_id");
		$section_id = $this->input->post("section_id");
		$row_id = $this->input->post("row_id");
		
		$this->load->model("backend/content_model","content");
		
		$fields = $this->content->get_fields_data($content_id,$section_id,$row_id);
		
		foreach($fields as &$f):
			if(in_array($f->type_id,array(1,2,4,5,6,8,9,10,12,20,21,24,25,27))):
				$opcodes = $this->finediff->getDiffOpcodes(strip_tags($f->content_live),strip_tags($f->content_draft));
				$f->content_draft = $this->finediff->renderDiffToHTMLFromOpcodes(strip_tags($f->content_draft), $opcodes);
			endif;
			if($f->type_id==28):
				$f->files_live = $this->content->get_files_result($content_id,$section_id,$f->id,$row_id,1);
				$f->files_draft = $this->content->get_files_result($content_id,$section_id,$f->id,$row_id,2);
			endif;
		endforeach;
		
		$data['fields']= $fields;
		
		$this->load->view('backend/diff_view',$data);
	}
#************************************************************************************************************************
	#Regresamos a la visualizacion del live
	function live(){
		$array_items = array('draft' => '');
		$this->session->unset_userdata($array_items);
		redirect("/");
	}
#************************************************************************************************************************
	public function download_csv(){
		//Guardar información
		$this->load->model("Landing_model","model");
		$this->model->set_table("contact");

		$data = $this->model->list_rows("*");

		header("Content-type: text/csv");
		header("Content-Disposition: attachment; filename=contact.csv");
		header("Pragma: no-cache");
		header("Expires: 0");

		$fp = fopen('php://output', 'w');
		fputcsv($fp, array("Name", "Email", "Zip Code", "Comments"));
		if(isset($data) && !empty($data)){
			foreach ($data as $row) {
				fputcsv($fp, array($row->name, $row->email, $row->zipcode, $row->comments));
			}
		}
		fclose($fp);
	}
#************************************************************************************************************************
    public function stories($id=0,$sub=0){
        $this->load_header(":::Backend:::");
        $this->load->model("backend/content_model","content");
        $data = $this->content->content_info($id,$sub);

        $sql = "SELECT s.id,s.name,s.email,s.city,co.county, s.story,s.type
                FROM story s,county co
                WHERE s.county=co.id
                order by s.id asc";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0):
            $data['stories'] = $query->result();
        else:
            $data['stories'] = NULL;
        endif;
        $this->load->view('backend/stories',$data);
        $this->load_footer();
    }
#************************************************************************************************************************
    public function delete_story(){
        $id = $this->input->post("id");
        $this->load->model("backend/content_model","content");
        $this->content->set_table("story");
        $this->content->delete($id);
        echo $id;
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */