<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Team extends MY_Controller {

#**************************************************************************************************
	#Display the landing page
	public function index($error=0){
		$this->load->model("backend/content_model","content");

		$this->load_header_front(null);
		$data = null;

		$all_data = $this->content->get_section_multiple(4,3);
		if(!empty($all_data))
			usort($all_data,array($this,'sortTeam'));
		// $data['images'] = $all_data;
		// $data['vcard'] = $this->content->get_files_result(4,3,35);
		$data['all_data'] = $all_data;

		$this->load->view('team',$data);
		$this->load_footer_front(null);
	}
	private function sortTeam($a,$b){
		if ($a['order'] == $b['order']) {
			return 0;
		}
		return ($a['order'] < $b['order']) ? -1 : 1;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
