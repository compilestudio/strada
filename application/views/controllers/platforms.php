<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Platforms extends MY_Controller {

#**************************************************************************************************
	public function investment(){
		$this->load->model("backend/content_model","content");

		// $data['cat'] = $cat;
		$data = null;
		$this->load_header_front($data);

		// $data['categories'] = $this->content->get_section_multiple(5,4);
		$data['title'] = 'Investment';
		$data['parent'] = 'Platforms';
		$data['info'] = $this->content->get_section_info(8,3);

		$this->load->view('image_text',$data);
		$this->load_footer_front(null);
	}
	public function development(){
		$this->load->model("backend/content_model","content");

		$this->load_header_front(null);
		$data = null;

		$data['title'] = 'Development';
		$data['parent'] = 'Platforms';
		$data['info'] = $this->content->get_section_info(8,3);

		$this->load->view('image_text',$data);
		$this->load_footer_front(null);
	}


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
