<!doctype html>
<html lang="en">
<head>
	<link rel="icon" type="image/png" href="<?=base_url();?>_img/ico.ico">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="">

	<script type="application/ld+json">
	{
	  "@context": "http://schema.org",
	  "@type": "Organization",
	  "url": "<?=base_url();?>",
	  "logo": "<?=base_url();?>/_img/logo.png",
	  "sameAs": [
	    "https://www.facebook.com/",
	    "https://twitter.com/",
	    "https://www.pinterest.com//",
	    "https://www.linkedin.com/"
	  ],
	  "contactPoint": [{
	    "@type": "ContactPoint",
	    "telephone": "+1-415-383-7900",
	    "contactType": "customer service"
	  }]
	}
	</script>

	<title>
		Strada
	</title>
	<?php echo link_tag('_css/front/style.css?v=0.1');?>
	<script src="<?=base_url();?>_js/jquery.js"></script>
	<script src="<?=base_url();?>_js/tether.min.js"></script>
	<script src="<?=base_url();?>_js/app.min.js"></script>
	<script src="<?=base_url();?>_js/jquery.superslides.min.js"></script>
	<script src="<?=base_url();?>_js/bootstrap.min.js"></script>
	<script src="<?=base_url();?>_js/jquery.expander.min.js"></script>
	<script src="<?=base_url();?>_js/imagesloaded.pkgd.min.js"></script>
	
	<script src="<?=base_url();?>_js/jquery.maximage.js"></script>
	<script src="<?=base_url();?>_js/jquery.cycle.all.js"></script>
	<script src="<?=base_url();?>node_modules/readmore-js/readmore.min.js"></script>


</head>
<body class="<?=$class." ".$method;?>">
<script>
var collapse = 1;
var start = 0;
$(document).ready(function(){
	$('header').hover(
		function(){
			collapse = 0;
		},
		function(){
			if(collapse==3){
				$('header').addClass("collapsed");
			}
		}
	);
});
</script>
	<header class="collapsed">
		<nav class="navbar navbar-inverse navbar-toggleable-md navbar-light bg-faded bg-inverse">
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<a class="navbar-brand" href="<?=base_url();?>" style="color: #FFF">
				<img src="<?=base_url();?>img/logo.png" alt="Home Page" class="logolg">
				<img src="<?=base_url();?>img/logosm.png" alt="Home Page" class="logosm">
			</a>
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav">
		    	<?php if($ap->front_display=="1"):?>
			      <li class="nav-item dropdown">
			        <a class="nav-link dropdown-toggle my-nav-link <?php if($class=='approach') echo 'active';?>" href="javascript:void(0);" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			          <?=$ap->title;?>
			        </a>
			        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
			        	<?php if($phi->front_display=="1"):?>
			          		<a class="dropdown-item my-nav-link <?php if($method=="philosophy") echo 'active';?>" href="<?=site_url('approach/philosophy');?>">
			          		<?=$phi->title;?>
			          		</a>
			          	<?php endif;?>
			          	<?php if($stra->front_display=="1"):?>
			          		<a class="dropdown-item my-nav-link <?php if($method=="strategy") echo 'active';?>" href="<?=site_url('approach/strategy')?>">
			          			<?=$stra->title;?>
			          		</a>
			          	<?php endif;?>
			        </div>
			      </li>
			    <?php endif;?>
			  <?php if($team->front_display=="1"):?>
			      <li class="nav-item">
			        <a class="nav-link my-nav-link <?php if($class=='team') echo 'active';?>" href="<?=site_url('team/')?>">
			        	<?=$team->title;?>
			        </a>
			      </li>
			  <?php endif;?>
			  <?php if($proj->front_display=="1"):?>
			      <li class="nav-item dropdown">
			        <a class="nav-link dropdown-toggle my-nav-link <?php if($class=='project') echo 'active';?>" href="<?=site_url('project/')?>" id="navbarDropdownMenuLink"  aria-haspopup="true" aria-expanded="false">
			          <?=$proj->title;?>
			        </a>
			        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
			        <?php if(!empty($categories))
			        foreach($categories as $cats):?>
				        <a class="dropdown-item my-nav-link <?php if(!empty($cat) && $cat==$cats['category_']) echo 'active';?>" href="<?=site_url('project/'.$cats['category_'])?>">
				        	<?=$cats['category_'];?>
				        </a>
				    <?php endforeach;?>
			        </div>
			      </li>
			  <?php endif;?>
			  <?php if($plat->front_display=="1"):?>
			      <li class="nav-item dropdown">
			        <a class="nav-link dropdown-toggle my-nav-link <?php if($class=='platforms') echo 'active';?>" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			          <?=$plat->title;?>
			        </a>
			        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
			          <?php if($inv->front_display=="1"):?>
			          	<a class="dropdown-item my-nav-link <?php if($method=="investment") echo 'active';?>" href="<?=site_url('platforms/investment')?>">
			          		<?=$inv->title;?>
			          	</a>
			          <?php endif;?>
			          <?php if($dev->front_display=="1"):?>
			          	<a class="dropdown-item my-nav-link <?php if($method=="development") echo 'active';?>" href="<?=site_url('platforms/development')?>">
			          		<?=$dev->title;?>
			          	</a>
			          <?php endif;?>
			        </div>
			      </li>
			  <?php endif;?>
			  <?php if($cont->front_display=="1"):?>
			      <li class="nav-item">
			        <a class="nav-link my-nav-link <?php if($class=="contact") echo 'active';?>" href="<?=site_url('contact/')?>">
			        	<?=$cont->title;?>
			        </a>
			      </li>
			  <?php endif;?>
		    </ul>
		  </div>
		</nav>
	</header>
	<div class="container-fluid">
		<div class="main-row">