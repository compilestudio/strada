<div>
	<div id="maximage">
		<?php
			foreach ($images as $image) { 
				$url = "javscript:void(0);";
				if(!empty($image['link']))
					$url = addhttp($image['link']);
			?>
				<img src="<?=base_url().'uploads/'.$image['img'];?>" alt="" width="1400" height="1050" data-href="<?=$url;?>" />
		<?php
			}
		?>
		
	</div>
	<?php if($caption['show']=="on"):?>
		<div class="caption-home">
			<?=$caption['caption'];?>
		</div>
	<?php endif;?>
	<div class="circles"></div>
</div>

<script src="<?=base_url();?>_js/jquery.maximage.js"></script>
<script src="<?=base_url();?>_js/jquery.cycle.all.js"></script>
<?php
function addhttp($url) {
	if(!empty($url)):
	    if (!preg_match("~^(?:f|ht)tps?://~i", $url)):
	        $url = "http://" . $url;
	    endif;
	else:
		$url = "javascript:void(0);";
	endif;
    return $url;
}
?>
<script type="text/javascript" charset="utf-8">
	$(function(){
		// Trigger maximage
		jQuery('#maximage').maximage({
			cycleOptions: {
				pager: $('.circles')
			}
		});
		//Links
		$('.mc-image').on('click',function(){
			window.open($(this).data("href"));
		});
		$('.circles').css("bottom",parseInt($('.caption-home').outerHeight())+15);
	});
	$(window).resize(function(){
		$('.circles').css("bottom",parseInt($('.caption-home').outerHeight())+15);
	});
</script>
