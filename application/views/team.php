<div class="team-container">
<?php
	$count = 0;
	//for($i=0;$i<2;$i++)
	foreach ($all_data as $dato):
?>
	<div class="team" data-toggle="modal" data-target=".bd-example-modal-lg-<?=$dato['row'];?>">
		<div class="contain">
			<img src="<?=base_url().'uploads/'.$dato['image']?>">
			<p class="person-name"><?=$dato['name']?></p>
		</div>
	</div>
	<div class="modal fade bd-example-modal-lg-<?=$dato['row'];?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" style="max-width: 100%;">
			<div class="modal-content modal-info">
			    <div class="modal-body">
			    	<a data-dismiss="modal" class="btn-close-modal">
			    		<img src="<?=base_url();?>img/close.png" alt="Close">
			    	</a>
        			<div class="row row-modal">
						<div class="left-modal">
							<div class="team-modal" data-toggle="modal" data-target=".bd-example-modal-lg-<?=$dato['row'];?>">
								<img src="<?=base_url().'uploads/'.$dato['innerImage']?>">
								<p class="person-name"><?=$dato['name']?></p>
							</div>
							<p style="margin-top: 5px;margin-top: 30px;"><?=$dato['position']?></p>
							<a href="mailto:<?=$dato['mail']?>" class="info-personal">
								<?=$dato['mail']?>
							</a>
							<?php if(!empty($dato['vcard'])):?>
								<p class="info-personal">
									<a class="v-card" href="<?=site_url('team/download/'.$dato['vcard'])?>">
										<strong>v-card</strong>
									</a>
								</p>
							<?php endif;?>
						</div>
						<div class="right-modal">
							<p class="info-description"><?=$dato['description']?></p>
							<div class="row">
								<?php if(!empty($dato['highlights'])):?>
									<div class="section-modal">
										<p class="title-section">Highlights</p>
										<?=$dato['highlights'];?>
									</div>
								<?php endif;?>
								<?php if(!empty($dato['projects'])):?>
									<div class="section-modal">
										<p class="title-section">Projects</p>
										<?=$dato['projects'];?>
									</div>
							<?php endif;?>
							</div>
						</div>
					</div>
  				</div>
			</div>
		</div>
	</div>
<?php
		$count++;
	endforeach;
?>
</div>

<script>
$(window).on("load",function(){
		resize();
});
$(window).resize(function(){
	resize();
});
function resize(){
	var height = 0;
	$('.team-container .team .contain').removeAttr("style");
	$.each($('.team-container .team .contain'), function( index, value ) {
	  var h = parseInt($(value).outerHeight());
	  if(h>height){
	  	height = h;
	  }
	});
	//$('.team-container .team .contain').outerHeight(height);
	var w = parseInt($(".team-container .team:first").width());
	$('.left-modal,.right-modal').removeAttr("style");
	if($(window).width()>=576){
		$('.left-modal').width(w);
		$('.right-modal').width($(document).width()-w-70);
	}
	setTimeout(function(){
		$('.team-container').removeAttr("style")
			.css('marginRight',$('.team-container .team:first').css("marginLeft"));
		//$('.navbar-brand').removeAttr("style").css('marginLeft',parseInt($('.team-container .team:first').css("marginLeft"))-15);
	},100);
}
</script>


