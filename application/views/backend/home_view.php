<div class="row_">
	<div class="left_column">
		<?=$menu;?>
	</div>
	<div class="right_column">
		<div class="headline">
			<?=$home_welcome;?>
		</div>
		<div class="description">
			<?=$home_explanation;?>
		</div>
		<div class="sub_headline">
			<?=$home_login_title;?>
		</div>
		<div class="description">
			<?=$home_login_explanation;?>
		</div>
		<form method="post" action="<?=site_url("backend/home/update_user");?>">
			<fieldset class="login_fieldset_home">
				<div class="control-group">
					<label for="user_name" class="control-label"><?=$user_login;?></label>
					<input type="text" id="user_name" name="user_name" value="<?=$user_name;?>" class="span5">
					<label for="password" class="control-label"><?=$user_pass;?></label>
					<input type="password" id="password" name="password" class="span5">
					<div class="span5 button">
						<button type="submit" class="btn btn-primary pull-right">
							<?=$update_button;?>
							<i class="icon-pencil icon-white"></i>
						</button>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>