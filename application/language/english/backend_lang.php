<?php
$lang['date_format']	= "mm-dd-yyyy";

$lang['user_login']	= "Username: ";
$lang['user_pass']	= "Password: ";
$lang['login_button']	= "Login";
$lang['login_error']	= "User and/or password incorrect";

#Home
$lang['home_welcome']	= "WELCOME";
$lang['home_explanation']	= "You can manage all content on this website by clicking on one of the main nav areas on the left or on any of the backend pages.";
$lang['home_login_title']	= "Login Information";
$lang['home_login_explanation']	= "To change the username and password of the person who is listed as the user for this backend, please select below:";
$lang['update_button']	= "Update";

#Sections
$lang['header_title']	= "Page Navigation Header";
$lang['header_description']	= "Edit the name of the page and page order here. <br>Hide or display the main page here.";
$lang['section_edit']	= "[ Edit Info]";
$lang['section_close']	= "[ Close ]";

#Header information
$lang['page_header_title']	= "Page Header";
$lang['page_order_title']	= "Page Order";
$lang['page_status_title']	= "Page Status";

#Section information
$lang['multiple_label'] = "Select ";
$lang['multiple_new'] = "Create New";
$lang['multiple_none'] = "None";

#Images
$lang['upload_label'] = "Please upload your images";
$lang['placeholder'] = "Place Holder Text";
$lang['confirm_img_delete'] = "Do you want to delete this image?";
$lang['confirm_img_restore'] = "Do you want to restore this image?";
$lang['edit_image_title'] = "Edit Image Text";
$lang['update_image_text'] = "Update Text";

#switch Information
$lang['switch_on'] = "yes";
$lang['switch_off'] = "no";


#Buttons
$lang['btn_sumbit'] = "Save";
$lang['btn_save_draft'] = "Save changes";
$lang['btn_submit_changes'] = "Submit changes for Approval";
$lang['btn_approve_changes'] = "Approve Changes";
$lang['btn_reject_changes'] = "Reject Changes";
$lang['btn_approve'] = "Approve";
$lang['btn_delete'] = "Delete Record";
$lang['btn_expand'] = "[ Expand All ]&nbsp;&nbsp;&nbsp;";
$lang['btn_collapse'] = "[ Collapse All ]";

#Notifications
$lang['success_notification'] = "Content Saved Successfully";
$lang['success_notification_delay'] = "3000";
$lang['loading_content_notification'] = "<h3>Loading your content please wait</h3>";
$lang['content_locked'] = "<h3>This content changes are pending for approval</h3>";
$lang['delete_notification'] = "Content Deleted Successfully";
$lang['delete_notification_delay'] = "3000";

#Default Menues
$lang['header_menu_legend'] = "Header";
$lang['footer_menu_legend'] = "Footers";
$lang['login_menu_legend'] = "Login";
$lang['directions_menu_legend'] = "Directions";

#Meta tags
$lang['meta_tags_title'] = "Meta Data";
$lang['meta_tags_description'] = "Edit the meta data: page title, page descriptions and keywords for this page here. <br>(This data is used in search results.)";
$lang['meta_tags_save'] = "Save Meta Data";
$lang['meta_page_title'] = "Page Title";
$lang['meta_keywords_title'] = "Keywords";
$lang['meta_description_title'] = "Description";


#reject
$lang['reject_title'] = "Please enter the reason for your rejection";
$lang['reject_btn'] = "Reject";

?>