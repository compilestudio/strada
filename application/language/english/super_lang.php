<?php
$lang['log_in_title']	= "Welcome";
$lang['user_login']	= "Username: ";
$lang['user_pass']	= "Password: ";
$lang['login_button']	= "Log-in";
$lang['login_error']	= "User and/or password incorrect";

$lang['home_welcome']	= "WELCOME SUPER ADMIN";
$lang['home_explanation']	= "You can manage all the sections that the site will contain and the user can change";
$lang['home_login_title']	= "Log in Information";
$lang['home_login_explanation']	= "You can change the access information to the super admin section below:";
$lang['update_button']	= "Update";


#Menu
$lang['add_section']	= "Add a main section";


#Content
$lang['content_description']	= "Use this page to edit the section optionas as well as it's fields";
$lang['title_label']	= "Title";
$lang['description_label']	= "Description of what to do on this section";
$lang['deleted_label']	= "Deleted?";
$lang['content_order']	= "Order";
$lang['has_subsections']	= "Has dynamic sub-content?";
$lang['front_display']	= "Display on the front end?";
$lang['update_btn']	= "Update";
$lang['delete_btn']	= "Delete";

#Fields
$lang['add_fields']	= "Add new fields";
$lang['add_fields_description']	= "Here you add the fields that this section will have, or if this content will have dynamic sub content this fields will be for each sub content.";
$lang['field_name']	= "Field name (no spaces)";
$lang['field_order']	= "Order";
$lang['field_display_text']	= "Display text";
$lang['field_type']	= "Field type";
$lang['field_section']	= "Field section";
$lang['update_field_btn']	= "Add";

#Assigned fields
$lang['assigned_fields']	= "Assigned Fields";
$lang['assigned_fields_description']	= "This is a list of the fields assigned to this content";

$lang['parent_select_id']	= "Parent for the display";
$lang['no_selection']	= "No selection";

$lang['delete_content_alert']	= "Are you sure that you want to delete this content?";
$lang['delete_field_alert']	= "Are you sure that you want to delete this field?";

$lang['new_content_for']	= "New content for: ";
$lang['new_content_top']	= "New main content ";

?>